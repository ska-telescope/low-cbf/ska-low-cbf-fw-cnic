#!/bin/bash

if [ -z $1 ]; then
    echo "Usage: $0 <version>"
    exit 1
fi

echo "Downloading version $1 for u50"

mkdir -p firmware_u50
cd firmware_u50
get_alveo_image -p cnic -v "$1" -f u50 -s gitlab -x 201920
cd ..

#!/bin/bash

if [ -z $1 ]; then
    echo "Usage: $0 <version>"
    exit 1
fi

echo "Downloading version $1 for u50lv"

mkdir -p firmware_u50lv
cd firmware_u50lv
get_alveo_image -p cnic -v "$1" -f u50lv -s gitlab -x 2
cd ..

#!/bin/bash

if [ -z $1 ]; then
    echo "Usage: $0 <version>"
    exit 1
fi

echo "Downloading version $1 for u55c"

mkdir -p firmware
cd firmware
get_alveo_image -p cnic -v "$1"  -f u55c -s gitlab
cd ..

#!/bin/bash

if [ -z $1 ]; then
    echo "Usage: $0 <version>"
    exit 1
fi

echo "Downloading version $1 for u280"

mkdir -p firmware_u280
cd firmware_u280
get_alveo_image -p cnic -v "$1" -f u280 -s gitlab -x 1
cd ..

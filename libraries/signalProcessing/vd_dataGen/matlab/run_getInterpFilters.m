% Get interpolation filters for CNIC data generation
% Generates 9-bit filter taps (i.e. limited to the range +/-255)
% with the same DC response for all the filters,
% and the maximum possible output when applied to 1 bit data of
% 2047, so the output of interpolation using these filters applied
% to 1-bit data will always be representable with 12 bits.
f1 = getInterpFilters(8,2048,1);
writeCOE = 1;

% maximum possible value that can be returned when interpolating
% 1 bit data
f1_max_abs_sum = max(sum(abs(f1),2));

% Scale factor so that the maximum possible value generated is
% +/-2047 when used to interpolate 1 bit data.
f1_scale = 2044/f1_max_abs_sum;

% Adjust the scale of individual filters so that they all have the 
% same DC response after rounding to 9-bit values.
f_scaled = zeros(2048,8);
all_scales = zeros(2048,1);
for filt = 1:2048
    this_scale = f1_scale;
    while (sum((round(this_scale * f1(filt,:)))) > floor(f1_scale))
        this_scale = this_scale * 0.9999999; 
    end
    while (sum((round(this_scale * f1(filt,:)))) < floor(f1_scale))
        this_scale = this_scale * 1.0000001;
    end
    f_scaled(filt,:) = round(this_scale * f1(filt,:));
    all_scales(filt) = this_scale;
end

disp([' max scale = ' num2str(max(all_scales)) ', min scale = ' num2str(min(all_scales))]);
DC_response = sum(f_scaled,2);
disp([' Max DC response = ' num2str(max(DC_response)) ', min DC response = ' num2str(min(DC_response))]);
max_response = sum(abs(f_scaled),2);
disp([' Max response = ' num2str(max(max_response)) ', min max response = ' num2str(min(max_response))]);

if (writeCOE)
    fid = fopen(['interp_taps.coe'],'w');
    fprintf(fid,'memory_initialization_radix = 2;\n');
    fprintf(fid,'memory_initialization_vector = ');
    for filterSel = 1:2048
        dstr = [];
        dstr(1:72) = '0';
        for tap = 1:8
            dstr(((tap-1)*12 + 1):((tap-1)*12 + 12)) = dec2binX(f_scaled(filterSel,tap),12);
        end
        fprintf(fid,['\n' dstr]);
    end
    fprintf(fid,';\n');
    fclose(fid);
end


----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 06/20/2023 09:17:08 PM
-- Module Name: vd_datagen_tb - Behavioral
-- Description: 
--   testbench for vd_datagen module 
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library vd_datagen_lib, axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;
use vd_datagen_lib.vd_datagen_reg_pkg.all;
use std.textio.all;
use IEEE.std_logic_textio.all;

entity vd_datagen_tb is
    generic(
        g_N128_SOURCE_INSTANCES : integer := 8;
        g_REGISTER_INIT_FILENAME : string := "/home/hum089/projects/ska-low-cbf-fw-cnic/libraries/signalProcessing/vd_dataGen/tests/test1_cfg.txt";
        -- special test case for axi full interface to write and read back polynomial config settings.
        --g_REGISTER_INIT_FILENAME : string := "/home/hum089/projects/ska-low-cbf-fw-cnic/libraries/signalProcessing/vd_dataGen/tests/mem_wr_rd.txt";
        g_HBM_DUMP_FILENAME_BUF0 : string := "/home/hum089/projects/ska-low-cbf-fw-cnic/libraries/signalProcessing/vd_dataGen/tests/test1_HBM_dump_buf0.txt";
        g_HBM_DUMP_FILENAME_BUF1 : string := "/home/hum089/projects/ska-low-cbf-fw-cnic/libraries/signalProcessing/vd_dataGen/tests/test1_HBM_dump_buf1.txt"
    );
--  Port ( );
end vd_datagen_tb;

architecture Behavioral of vd_datagen_tb is


    function get_axi_size(AXI_DATA_WIDTH : integer) return std_logic_vector is
    begin
        if AXI_DATA_WIDTH = 8 then
            return "000";
        elsif AXI_DATA_WIDTH = 16 then
            return "001";
        elsif AXI_DATA_WIDTH = 32 then
            return "010";
        elsif AXI_DATA_WIDTH = 64 then
            return "011";
        elsif AXI_DATA_WIDTH = 128 then
            return "100";
        elsif AXI_DATA_WIDTH = 256 then
            return "101";
        elsif AXI_DATA_WIDTH = 512 then
            return "110";    -- size of 6 indicates 64 bytes in each beat (i.e. 512 bit wide bus) -- out std_logic_vector(2 downto 0);
        elsif AXI_DATA_WIDTH = 1024 then
            return "111";
        else
            assert FALSE report "Bad AXI data width" severity failure;
            return "000";
        end if;
    end get_axi_size;

    constant M02_DATA_WIDTH : integer := 512;
    signal m02_awvalid : std_logic;
    signal m02_awready : std_logic;
    signal m02_awaddr : std_logic_vector(31 downto 0);
    signal m02_awid : std_logic_vector(0 downto 0);
    signal m02_awlen : std_logic_vector(7 downto 0);
    signal m02_awsize : std_logic_vector(2 downto 0);
    signal m02_awburst : std_logic_vector(1 downto 0);
    signal m02_awlock :  std_logic_vector(1 downto 0);
    signal m02_awcache :  std_logic_vector(3 downto 0);
    signal m02_awprot :  std_logic_vector(2 downto 0);
    signal m02_awqos :  std_logic_vector(3 downto 0);
    signal m02_awregion :  std_logic_vector(3 downto 0);
    signal m02_wvalid :  std_logic;
    signal m02_wready :  std_logic;
    signal m02_wdata :  std_logic_vector((M02_DATA_WIDTH-1) downto 0);
    signal m02_wstrb :  std_logic_vector(63 downto 0);
    signal m02_wlast :  std_logic;
    signal m02_bvalid : std_logic;
    signal m02_bready :  std_logic;
    signal m02_bresp :  std_logic_vector(1 downto 0);
    signal m02_bid :  std_logic_vector(0 downto 0);
    signal m02_arvalid :  std_logic;
    signal m02_arready :  std_logic;
    signal m02_araddr :  std_logic_vector(31 downto 0);
    signal m02_arid :  std_logic_vector(0 downto 0);
    signal m02_arlen :  std_logic_vector(7 downto 0);
    signal m02_arsize :  std_logic_vector(2 downto 0);
    signal m02_arburst : std_logic_vector(1 downto 0);
    signal m02_arlock :  std_logic_vector(1 downto 0);
    signal m02_arcache :  std_logic_vector(3 downto 0);
    signal m02_arprot :  std_logic_Vector(2 downto 0);
    signal m02_arqos :  std_logic_vector(3 downto 0);
    signal m02_arregion :  std_logic_vector(3 downto 0);
    signal m02_rvalid :  std_logic;
    signal m02_rready :  std_logic;
    signal m02_rdata :  std_logic_vector((M02_DATA_WIDTH-1) downto 0);
    signal m02_rlast :  std_logic;
    signal m02_rid :  std_logic_vector(0 downto 0);
    signal m02_rresp :  std_logic_vector(1 downto 0);

    signal writing_buf0, writing_buf1 : std_logic;
    signal start_running : std_logic;
    signal clk : std_logic := '0';
    signal reset : std_logic;
    
    signal axi_lite_mosi : t_axi4_lite_mosi;
    signal axi_lite_miso : t_axi4_lite_miso;
    
    signal axi_full_mosi : t_axi4_full_mosi;
    signal axi_full_miso : t_axi4_full_miso;
    signal axi_rst_n : std_logic := '1';
    signal poly_runs : std_logic_vector(15 downto 0);
    signal write_HBM_to_disk : std_logic := '0';
    signal hbm_dump_addr : integer;
    signal hbm_dump_filename : string(1 to g_HBM_DUMP_FILENAME_BUF0'length);
    signal reset_axi : std_logic;
    signal rdata32 : std_logic_vector(31 downto 0);
    signal test_count : std_logic_vector(3 downto 0) := "0000";

    type t_rb_fsm is (idle, set_ar, wait_ready, wait_last, update_addr, check_finished);
    signal rb_fsm : t_rb_fsm := idle;
    signal do_readback : std_logic := '0';
    signal araddr : std_logic_vector(32 downto 0) := (others => '0');
    signal arvalid : std_logic := '0';
    
begin
    
    clk <= not clk after 2 ns;

    axi_full_mosi.awsize <= get_axi_size(32);
    axi_full_mosi.awburst <= "01";   -- "01" indicates incrementing addresses for each beat in the burst. 
    axi_full_mosi.bready  <= '1';  -- Always accept acknowledgement of write transactions. -- out std_logic;
    axi_full_mosi.wstrb  <= (others => '1');  -- We always write all bytes in the bus. --  out std_logic_vector(63 downto 0);
    axi_full_mosi.arsize  <= get_axi_size(32);   -- 6 = 64 bytes per beat = 512 bit wide bus. -- out std_logic_vector(2 downto 0);
    axi_full_mosi.arburst <= "01";    -- "01" = incrementing address for each beat in the burst. -- out std_logic_vector(1 downto 0);
    
    axi_full_mosi.arlock <= '0';
    axi_full_mosi.awlock <= '0';
    axi_full_mosi.awid(0) <= '0';   -- We only use a single ID -- out std_logic_vector(0 downto 0);
    axi_full_mosi.awcache <= "0011";  -- out std_logic_vector(3 downto 0); bufferable transaction. Default in Vitis environment.
    axi_full_mosi.awprot  <= "000";   -- Has no effect in Vitis environment. -- out std_logic_vector(2 downto 0);
    axi_full_mosi.awqos   <= "0000";  -- Has no effect in vitis environment, -- out std_logic_vector(3 downto 0);
    axi_full_mosi.awregion <= "0000"; -- Has no effect in Vitis environment. -- out std_logic_vector(3 downto 0);
    axi_full_mosi.arid(0) <= '0';     -- ID are not used. -- out std_logic_vector(0 downto 0);
    axi_full_mosi.arcache <= "0011";  -- out std_logic_vector(3 downto 0); bufferable transaction. Default in Vitis environment.
    axi_full_mosi.arprot  <= "000";   -- Has no effect in vitis environment; out std_logic_Vector(2 downto 0);
    axi_full_mosi.arqos    <= "0000"; -- Has no effect in vitis environment; out std_logic_vector(3 downto 0);
    axi_full_mosi.arregion <= "0000"; -- Has no effect in vitis environment; out std_logic_vector(3 downto 0);
    

--  TYPE t_axi4_full_miso IS RECORD  -- Master In Slave Out
--    -- write_address channel
--    awready : std_logic;                                       -- write address ready
--    -- write data channel
--    wready  : std_logic;                                       -- write ready
--    -- write response channel
--    bid     : std_logic_vector(c_axi4_full_id_w-1 downto 0);   -- write response id
--    bresp   : std_logic_vector(c_axi4_full_resp_w-1 downto 0); -- write response
--    bvalid  : std_logic;                                       -- write response valid
--    buser   : std_logic_vector(c_axi4_full_auser_w-1 DOWNTO 0);
--    -- read address channel
--    arready : std_logic;                                       -- read address ready
--    -- read data channel
--    rid     : std_logic_vector(c_axi4_full_id_w-1 downto 0);   -- read id
--    rdata   : std_logic_vector(c_axi4_full_data_w-1 downto 0); -- read data
--    rresp   : std_logic_vector(c_axi4_full_resp_w-1 downto 0); -- read response
--    rvalid  : std_logic;                                       -- read valid
--    ruser   : std_logic_vector(c_axi4_full_auser_w-1 DOWNTO 0);
--    rlast   : std_logic;
--  END RECORD;    
    
    process
        file RegCmdfile: TEXT;
        variable RegLine_in : Line;
        variable regData, regAddr : std_logic_vector(31 downto 0);
        variable RegGood : boolean;
        variable c : character;
    begin
        axi_rst_n <= '1';
        reset <= '0';
        start_running <= '0';
        write_HBM_to_disk <= '0';
        do_readback <= '0';

        axi_lite_mosi.awaddr <= (others => '0');
        axi_lite_mosi.awprot <= "000";
        axi_lite_mosi.awvalid <= '0';
        axi_lite_mosi.wdata <= (others => '0');
        axi_lite_mosi.wstrb <= "1111";
        axi_lite_mosi.wvalid <= '0';
        axi_lite_mosi.bready <= '0';
        axi_lite_mosi.araddr <= (others => '0');
        axi_lite_mosi.arprot <= "000";
        axi_lite_mosi.arvalid <= '0';
        axi_lite_mosi.rready <= '0';
        
        axi_full_mosi.awaddr <= (others => '0');
        axi_full_mosi.awvalid <= '0';
        -- Always write bursts of 64 bytes = 16 x 4-byte words = 1 polynomial specification
        axi_full_mosi.awlen <= "00001111";
        axi_full_mosi.wvalid <= '0';
        axi_full_mosi.wdata <= (others => '0');
        axi_full_mosi.wlast <= '0';
        
        wait until rising_edge(clk);
        wait for 100 ns;
        wait until rising_edge(clk);
        axi_rst_n <= '0';
        wait until rising_edge(clk);
        axi_rst_n <= '1';
        wait until rising_edge(clk);
        reset <= '1';
        wait until rising_edge(clk);
        reset <= '0';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
                
        axi_lite_transaction(clk, axi_lite_miso, axi_lite_mosi, 
            c_vd_datagen_reg_enable_vd_address.base_address + c_vd_datagen_reg_enable_vd_address.address,
            true, x"00000003");
        
        wait until rising_edge(clk);
        wait for 100 ns;
        wait until rising_edge(clk);
        -- write configuration data to memory over the axi full interface.
        FILE_OPEN(RegCmdfile, g_REGISTER_INIT_FILENAME, READ_MODE);
        
        while (not endfile(RegCmdfile)) loop
            -- Get data for 1 source (64 bytes = 16 x 4byte words)
            readline(regCmdFile, regLine_in);
            -- drop "[" character that indicates the address
            read(regLine_in,c,regGood);
            -- get the address
            hread(regLine_in,regAddr,regGood);
            axi_full_mosi.awaddr(31 downto 0) <= regAddr;
            axi_full_mosi.awvalid <= '1';
            wait until (rising_edge(clk) and axi_full_miso.awready='1');
            wait for 1 ps;
            axi_full_mosi.awvalid <= '0';
            for i in 0 to 15 loop
                readline(regCmdFile, regLine_in);
                hread(regLine_in,regData,regGood);
                axi_full_mosi.wvalid <= '1';
                axi_full_mosi.wdata(31 downto 0) <= regData;
                if i=15 then
                    axi_full_mosi.wlast <= '1';
                else
                    axi_full_mosi.wlast <= '0';
                end if;
                wait until (rising_edge(clk) and axi_full_miso.wready = '1');
                wait for 1 ps;
            end loop;
            axi_full_mosi.wvalid <= '0';
            wait until rising_edge(clk);
            wait until rising_edge(clk);
        end loop;
        wait until rising_edge(clk);
        wait for 100 ns;
        -- read back some data 
        wait until rising_edge(clk);
        do_readback <= '1';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        do_readback <= '0';
        --------------------------------------------------------------
        -- First HBM Buffer 
        wait until rising_edge(clk);
        start_running <= '1';
        wait until rising_edge(clk);
        start_running <= '0';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        -- 128 poly runs = 128*16 samples = 2048 samples = 1 SPS packets worth of data.
        wait until (writing_buf0 = '0');
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        write_HBM_to_disk <= '1';
        hbm_dump_addr <= 0;
        hbm_dump_filename <= g_HBM_DUMP_FILENAME_BUF0;
        wait until rising_edge(clk);
        write_HBM_to_disk <= '0';
        ---------------------------------------------------------------
        -- Second HBM Buffer
        wait until rising_edge(clk);
        start_running <= '1';
        wait until rising_edge(clk);
        start_running <= '0';
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        -- 128 poly runs = 128*16 samples = 2048 samples = 1 SPS packets worth of data.
        wait until (writing_buf1 = '0');
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        write_HBM_to_disk <= '1';
        hbm_dump_addr <= 536870912; -- second buffer is at 512 MBytes.
        hbm_dump_filename <= g_HBM_DUMP_FILENAME_BUF1;
        wait until rising_edge(clk);
        write_HBM_to_disk <= '0';
        ---------------------------------------------------------------
        
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then
            test_count <= std_logic_vector(unsigned(test_count) +1);

            axi_full_mosi.arlen <= "00001111";
            
            case rb_fsm is
                when idle =>
                    if do_readback = '1' then
                        rb_fsm <= set_ar;
                        araddr <= std_logic_vector(to_unsigned(256,33));
                        arvalid <= '0';
                    else
                        araddr <= (others => '0');
                        arvalid <= '0';
                    end if;
                
                when set_ar =>
                    arvalid <= '1';
                    rb_fsm <= wait_ready;
                    
                when wait_ready =>
                    if axi_full_miso.arready = '1' then
                        arvalid <= '0';
                        rb_fsm <= wait_last;
                    end if;
                
                when wait_last =>
                    if axi_full_miso.rlast = '1' and axi_full_miso.rvalid = '1' then
                        rb_fsm <= update_addr;
                    end if;
                
                when update_addr =>
                    araddr <= araddr(31 downto 0) & '0';
                    rb_fsm <= check_finished;
                    
                when check_finished => 
                    if araddr(20) = '1' then
                        rb_fsm <= idle;
                    else
                        rb_fsm <= set_ar;
                    end if;
                
                when others =>
                    rb_fsm <= idle;
                
            end case;
        end if;
    end process;
    
    axi_full_mosi.araddr <= araddr;
    axi_full_mosi.arvalid <= arvalid;
    -- axi bram controller doesn't seem to do the right thing when rready is low,
    -- don't know why, but ila shows rready never goes low, so skip testing it for now.
    axi_full_mosi.rready <= '1'; --test_count(0);
    axi_full_mosi.arlen <= "00001111";
    
    rdata32 <= axi_full_miso.rdata(31 downto 0);

    reset_axi <= not axi_rst_n;
    
    DUT : entity vd_datagen_lib.dataGen_top
    generic map (
        -- Number of data streams to generate as a multiple of 128
        -- Min = 1, max = 8, use less to reduce build times.
        g_N128_SOURCE_INSTANCES => g_N128_SOURCE_INSTANCES, -- integer range 1 to 8 := 8 
        -- Number of blocks of 128 SPS samples to generate per run
        -- Each block of 128 SPS samples = 512 bytes of data in the HBM
        -- Maximum is 1024 => (1024 blocks)*(512 bytes/block) = 512 kBytes 
        -- (= 131072 SPS samples / stream)
        g_128_SAMPLE_BLOCKS => 4 --  integer range 1 to 1024 := 1024
    ) Port map (
        clk => clk, --  in std_logic;
        reset_axi => reset_axi, -- in std_logic;
        reset => reset, --  in std_logic;
        -- ARGs full interface
        -- Addresses two ultraRAMs for storage of delay polynomials 
        -- other signal generation controls
        i_vdgen_full_axi_mosi => axi_full_mosi, -- in  t_axi4_full_mosi;
        o_vdgen_full_axi_miso => axi_full_miso, -- out t_axi4_full_miso;
        -- ARGs lite interface
        -- Registers for top level control and monitoring.
        i_vdgen_lite_axi_mosi => axi_lite_mosi, -- in t_axi4_lite_mosi; 
        o_vdgen_lite_axi_miso => axi_lite_miso, -- out t_axi4_lite_miso;
        ----------------------------------------------------
        -- Control signals to the rest of CNIC
        -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
        o_writing_buf0 => writing_buf0, --  out std_logic;
        -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
        o_writing_buf1 => writing_buf1, --  out std_logic;
        -- Where we are up to in generating data for the HBM buffer.
        -- Each poly run is 16 output samples, total of 8192 per HBM buffer 
        -- 8192*16= 131072 samples = 512kBytes/stream @ 4 bytes/sample
        o_poly_runs => poly_runs, -- out (15:0);
        -- Pulse to start writing data (to whichever buffer we are currently up to)
        i_start => start_running,  --  in std_logic;
        ----------------------------------------------------
        -- HBM interface, Uses 1 Gbyte of HBM. 
        -- aw bus
        m01_axi_awvalid  => m02_awvalid, -- out std_logic;
        m01_axi_awready  => m02_awready, -- in std_logic;
        m01_axi_awaddr   => m02_awaddr,  -- out std_logic_vector(31 downto 0);
        m01_axi_awlen    => m02_awlen,   -- out std_logic_vector(7 downto 0);
        -- w bus - write data
        m01_axi_wvalid   => m02_wvalid,  -- out std_logic;
        m01_axi_wready   => m02_wready,  -- in std_logic;
        m01_axi_wdata    => m02_wdata,   -- out std_logic_vector(511 downto 0);
        m01_axi_wlast    => m02_wlast,   -- out std_logic;
        -- b bus - write response
        m01_axi_bvalid   => m02_bvalid,  -- in std_logic;
        m01_axi_bresp    => m02_bresp    -- in std_logic_vector(1 downto 0)
    );
    
    
    -- unused axi ports
    -- Read port is unused.
    m02_araddr <= (others => '0');
    m02_arlen <= (others => '0');
    m02_arvalid <= '0';
    m02_rready <= '1';
    
    -- Other unused axi ports
    m02_awsize <= get_axi_size(M02_DATA_WIDTH);
    m02_awburst <= "01";   -- "01" indicates incrementing addresses for each beat in the burst. 
    m02_bready  <= '1';  -- Always accept acknowledgement of write transactions. -- out std_logic;
    m02_wstrb  <= (others => '1');  -- We always write all bytes in the bus. --  out std_logic_vector(63 downto 0);
    m02_arsize  <= get_axi_size(M02_DATA_WIDTH);   -- 6 = 64 bytes per beat = 512 bit wide bus. -- out std_logic_vector(2 downto 0);
    m02_arburst <= "01";    -- "01" = incrementing address for each beat in the burst. -- out std_logic_vector(1 downto 0);
    
    m02_arlock <= "00";
    m02_awlock <= "00";
    m02_awid(0) <= '0';   -- We only use a single ID -- out std_logic_vector(0 downto 0);
    m02_awcache <= "0011";  -- out std_logic_vector(3 downto 0); bufferable transaction. Default in Vitis environment.
    m02_awprot  <= "000";   -- Has no effect in Vitis environment. -- out std_logic_vector(2 downto 0);
    m02_awqos   <= "0000";  -- Has no effect in vitis environment, -- out std_logic_vector(3 downto 0);
    m02_awregion <= "0000"; -- Has no effect in Vitis environment. -- out std_logic_vector(3 downto 0);
    m02_arid(0) <= '0';     -- ID are not used. -- out std_logic_vector(0 downto 0);
    m02_arcache <= "0011";  -- out std_logic_vector(3 downto 0); bufferable transaction. Default in Vitis environment.
    m02_arprot  <= "000";   -- Has no effect in vitis environment; out std_logic_Vector(2 downto 0);
    m02_arqos    <= "0000"; -- Has no effect in vitis environment; out std_logic_vector(3 downto 0);
    m02_arregion <= "0000"; -- Has no effect in vitis environment; out std_logic_vector(3 downto 0);
    
    -- m02 HBM interface : 2nd stage corner turn between filterbanks and beamformer, 2x256 MBytes.
    HBMm02 : entity vd_datagen_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 32, -- : integer := 32;   -- Byte address width. This also defines the amount of data. Use the correct width for the HBM memory block, e.g. 28 bits for 256 MBytes.
        AXI_ID_WIDTH => 1, -- integer := 1;
        AXI_DATA_WIDTH => 512, -- integer := 256;  -- Must be a multiple of 32 bits.
        READ_QUEUE_SIZE => 16, --  integer := 16;
        MIN_LAG => 60,  -- integer := 80   
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 43526, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 97, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 97 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    ) Port map (
        i_clk        => clk,
        i_rst_n      => axi_rst_n,
        axi_awaddr   => m02_awaddr(31 downto 0),
        axi_awid     => m02_awid, -- in std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_awlen    => m02_awlen,
        axi_awsize   => m02_awsize,
        axi_awburst  => m02_awburst,
        axi_awlock   => m02_awlock,
        axi_awcache  => m02_awcache,
        axi_awprot   => m02_awprot,
        axi_awqos    => m02_awqos, -- in(3:0)
        axi_awregion => m02_awregion, -- in(3:0)
        axi_awvalid  => m02_awvalid,
        axi_awready  => m02_awready,
        axi_wdata    => m02_wdata,
        axi_wstrb    => m02_wstrb,
        axi_wlast    => m02_wlast,
        axi_wvalid   => m02_wvalid,
        axi_wready   => m02_wready,
        axi_bresp    => m02_bresp,
        axi_bvalid   => m02_bvalid,
        axi_bready   => m02_bready,
        axi_bid      => m02_bid, -- out std_logic_vector(AXI_ID_WIDTH - 1 downto 0);
        axi_araddr   => m02_araddr(31 downto 0),
        axi_arlen    => m02_arlen,
        axi_arsize   => m02_arsize,
        axi_arburst  => m02_arburst,
        axi_arlock   => m02_arlock,
        axi_arcache  => m02_arcache,
        axi_arprot   => m02_arprot,
        axi_arvalid  => m02_arvalid,
        axi_arready  => m02_arready,
        axi_arqos    => m02_arqos,
        axi_arid     => m02_arid,
        axi_arregion => m02_arregion,
        axi_rdata    => m02_rdata,
        axi_rresp    => m02_rresp,
        axi_rlast    => m02_rlast,
        axi_rvalid   => m02_rvalid,
        axi_rready   => m02_rready,
        i_write_to_disk => write_HBM_to_disk, -- in std_logic;
        i_write_to_disk_addr => hbm_dump_addr, -- address to start the memory dump at.
        -- 4 Mbytes = first 8 data streams
        i_write_to_disk_size => 4194304, -- size in bytes
        i_fname => hbm_dump_filename -- : in string
    );


end Behavioral;

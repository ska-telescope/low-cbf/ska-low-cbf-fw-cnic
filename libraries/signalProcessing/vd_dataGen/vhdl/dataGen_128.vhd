----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 06/04/2023 11:20:16 PM
-- Module Name: dataGen_Filter - Behavioral
-- Description: 
--  
--                                                                                                                      256 bits in
--                                                                                                                           |
--           |---------------------------------------------------------------------------------------------------------------+--------|
--           | #dataGen_128# : Generate data for 128 SPS streams                                                             |        |
--           |                                                                                                               |        |
--  config->-+-#dataGen_config_mem0#                                 Interpolation_taps_ROM                                  |        |
--  data     |   | |                                                         |                                               |        |
--           |   | |                          |--------------------------------------------------------|                     |        |
--           |   | |                          | **dataGen_singleSource0**    |                         |                     |        |
--           |   | |                          | (generates 256 sky sources)  |                         |                     |        |
--           |   | ->datagen_poly_eval0----->-|--buffer->data_source0->interp filter-> phase-> scale---|---+->round->16 sample buffer |
--           |   |   (evaluates 512       |   |          data_source1        |      (complex  (real*   |   |                 |        |
--           |   |    polynomials)        |   |               |              |       mult)    complex) |   |                \|/       |
--           |   |                        |   |          state_memory        |                         |   |                 |        |
--           |   |                        |   |--------------------------------------------------------|   |                 |        |
--           |   |                        |                                  |                             |                 |        |
--           |   |                        |   |--------------------------------------------------------|   |                 |        |
--           |   |                        |->-| **dataGen_singleSource1**    |                         |->-|                 |        |
--           |   |                            |--------------------------------------------------------|   |                 |        |
--           | #dataGen_config_mem1#                                         |                             |                 |        |
--           |   | |                                                         |                             |                 |        |
--           |   | |                          |--------------------------------------------------------|   |                 |        |
--           |   | ->datagen_poly_eval1----->-| **dataGen_singleSource2**    |                         |->-|                 |        |
--           |   |                        |   |--------------------------------------------------------|   |                 |        |
--           |   |                        |                                  |                             |                 |        | 
--           |   |                        |   |--------------------------------------------------------|   |                 |        |
--           |   |                        |->-| **dataGen_singleSource3**                              |->-|                 |        |
--           |   |                            |--------------------------------------------------------|                     |        |
--           |   |                                                                                                           |        |
--           |---+-----------------------------------------------------------------------------------------------------------+--------|
--               |                                                                                                           |
--            config data out                                                                                          256 bits out
--
--  Resource use estimate:
--   BRAM : 2 datagen_poly_eval (1 x 2 instances)
--          4 sin/cos lookup    (0.5 x 8 instances)
--          4  state memory     (1 x 4 instances)
--          4  buffer           (1 x 4 instances)
--          16 Interpolation taps
--        = 30 
--   DSP :  256 interp filter  (32 x 8 instances)
--          24  sin/cos lookup (3 x 8 instances)
--          16  phase          (4 x 4 instances)
--          8   scale          (2 x 4 instances)
--          20  FP64 poly eval ((3 + 7) * 2 instances) 
--        = 196
--
--  ------------------------------------------------------------------------------
--  dataGen_config_mem addressing
--   Each instance of dataGen_config_mem is 2 ultraRAMs:
--    - poly_eval read side 8192 deep x 64 bits wide
--    - args side : 16384 deep x 32 bits wide
--      two instances per module, so 32768 deep x 32 bits wide
--   
--   The input address caters for up to 8 instances of dataGen_128, so 
--   the args address is 32768 * 8 = 262144 deep => 18 bit address.
--
--   Each entry in dataGen_config_mem is 64 bytes (8x8byte words when reading)
--    - 4 consecutive entries (4x64 = 256 bytes = 64 args addresses) define one polarisation for one stream 
--    - 8 consecutive entries (8*64 = 512 bytes = 128 args addresses) define one stream.
--   
--   Config entries assigned to each ultraRAM buffer :
--      datagen_config_mem0 : 0, 1, 4, 5, 8, 9, ...
--      datagen_config_mem1 : 2, 3, 6, 7, 10, 11, ...
--   This arrangement means that datagen_singleSource0 and datagen_singleSource1 are fed from datagen_poly_eval0 
--   and datagen_singleSource2 and datagen_singleSource3 are fed from datagen_poly_eval1,
--   so that the four data source generation modules are for the same output stream.
--   Every second stream processed by the "dataGen_singleSource" is for pol0, then pol1.
--   So, mapping the ARGs 32-bit wide data, 18 bit address bus to the ultraRAM 64 bit wide data, 13 bit address bus:
--
--     i_args_addr(1:0)   : Unused, byte address within a 4-byte word.
--     i_args_addr(2)     -> conversion from 4-byte to 8-byte bus    ; select byte write enables either x0F or xF0
--     i_args_addr(5:3)   -> dataGen_config_mem addr(2:0)            ; which of 8 x 64bit words for a single source description
--     i_args_addr(6)     -> dataGen_config_mem addr(3)              ; two consecutive sources go to consecutive locations
--     i_args_addr(7)     -> selects dataGen_config_mem0 or mem1     ; so that sources 0,1, 4,5, 8,9 etc go to mem0
--     i_args_addr(15:8)  -> dataGen_config_mem addr(11:4)
--     i_args_addr(18:16) -> enable writes to this module if = g_DATAGEN_128_INSTANCE
--     i_args_addr(19)    -> dataGen_config_mem addr(12)             ; select which buffer to use
--     
----------------------------------------------------------------------------------
library IEEE, vd_datagen_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library common_lib;
USE common_lib.common_pkg.ALL;

entity dataGen_128 is
    generic (
        -- up to 8 instances of this module, 
        g_DATAGEN_128_INSTANCE : integer range 0 to 7 := 0
    );
    port (
        clk : in std_logic;
        reset : in std_logic;
        ------------------------------------------------------------
        -- Reading/Writing from/to the configuration memory 
        i_args_wr_data : in std_logic_vector(31 downto 0);
        i_args_addr    : in std_logic_vector(19 downto 0);  -- byte address
        i_args_wren    : in std_logic;
        i_args_rd_data : in std_logic_vector(31 downto 0);
        i_args_rd_data_valid : in std_logic;
        -- Pipelined version of the control signals, to go to the next dataGen_128 module.
        -- If the read address is for a memory in this module, 
        -- then put i_host_return_data on o_host_return_data, 
        -- otherwise put data read from inside this module. 
        o_args_wr_data : out std_logic_vector(31 downto 0);
        o_args_addr    : out std_logic_vector(19 downto 0);
        o_args_wren    : out std_logic;
        o_args_rd_data : out std_logic_vector(31 downto 0);
        o_args_rd_data_valid : out std_logic;
        ------------------------------------------------------------
        -- control - Evaluate polynomials enerate a burst of 16 samples for all data streams
        i_poly_eval_start    : in std_logic; -- Evaluate all polynomials and then increment time by g_TIME_STEP
        -- which of the polynomial buffers to use, only read on i_poly_eval_start
        i_poly_eval_buf      : in std_logic;
        -- Double precision time in ns, read on i_poly_eval_start
        i_poly_eval_time_ns : in std_logic_vector(63 downto 0); 
        o_poly_eval_running  : out std_logic; -- status; if '1', module is busy.
        -- trigger readout and processing of the delay data to generate samples.
        i_read_delays : in std_logic;
        i_read_buffer : in std_logic; -- Which of the 2 buffers in the dataGen_singleSource modules to read from
        o_read_delay_running : out std_logic;
        -- At the lowest level there are two pipelines for processing sources,
        -- synchronise at the top level to ensure output data goes to the HBM buffer 
        -- module at the same time. 
        o_all_source_rdy : out std_logic_vector(1 downto 0);
        i_all_source_rdy : in std_logic_vector(1 downto 0);
        --
        -------------------------------------------------------------
        -- Data output
        -- data in from the previous dataGen_128 instance
        i_data_samples : in std_logic_vector(255 downto 0);
        i_data_stream  : in std_logic_vector(10 downto 0);
        i_data_block   : in std_logic;
        i_data_valid   : in std_logic;
        -- Pipelined output; either from the i_data signals or generated internally 
        o_data_samples : out std_logic_Vector(255 downto 0);
        o_data_stream  : out std_logic_vector(10 downto 0);
        o_data_block   : out std_logic;
        o_data_valid   : out std_logic
    );
    -- prevent optimisation across module boundaries.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of dataGen_128 : entity is "yes";
end dataGen_128;

architecture Behavioral of dataGen_128 is

    -- Interpolation filter coefficients
    -- 16 x 16bit values.
    -- 3 clock read latency
    component datagen_interp_rom16
    port (
        clka  : in std_logic;
        addra : in std_logic_vector(10 downto 0);
        douta : out std_logic_vector(255 downto 0));
    end component;
    
    signal args_bram_addr : std_logic_vector(15 downto 0);
    signal args_bram_wren : std_logic_vector(1 downto 0);
    signal args_rd_mem0_del2, args_rd_mem0_del1, args_rd_mem0 : std_logic;
    signal args_rd_mem1_del2, args_rd_mem1_del1, args_rd_mem1 : std_logic;
    signal args_bram_rddata : t_slv_32_arr(1 downto 0);
    signal config_mem_rd_addr : t_slv_13_arr(1 downto 0);
    signal config_mem_rd_data : t_slv_64_arr(1 downto 0);
    signal poly_eval_start : std_logic; -- evaluate all polynomials and then increment time by g_TIME_STEP
    signal poly_eval_buf :  std_logic; -- which of the polynomial buffers to use, only read on i_start
    signal poly_eval_rst_time : std_logic; -- set time used in the polynomial back to zero, only read on i_start
    
    signal src_sample_offset : t_slv_11_arr(1 downto 0);  -- Number of 1080ns samples to delay by
    signal src_interp_filter : t_slv_11_arr(1 downto 0);  -- Interpolation filter selection = fraction of a 1080 ns sample
    signal src_phase_offset : t_slv_24_arr(1 downto 0); -- Phase correction
    signal src_config  : t_slv_32_arr(1 downto 0);
    signal src_startup : std_logic_vector(1 downto 0); -- high if first run after i_rst, use to initialise number generators.
    signal src_count : t_slv_9_arr(1 downto 0);        -- which polynomial this is for
    signal src_valid, src_last : std_logic_vector(1 downto 0);
    signal src_frames : t_slv_32_arr(1 downto 0);
    
    signal data_re, data_im : t_slv_40_arr(3 downto 0);
    signal src_count_out : t_slv_8_arr(3 downto 0);
    signal data_re_sum01, data_im_sum01, data_re_sum23, data_im_sum23, data_re_40bit, data_im_40bit : std_logic_vector(39 downto 0);
    
    signal saturate_re, saturate_im, re_round_up, im_round_up : std_logic;
    signal data_re_int_8bit, data_im_int_8bit : std_logic_vector(7 downto 0);
    signal data_re_final, data_im_final : std_logic_vector(7 downto 0);
    signal poly_eval_running : std_logic_vector(1 downto 0);
    signal dvec, dvec_hold : std_logic_vector(255 downto 0);
    signal rst : std_logic;
    signal filter_select : t_slv_11_arr(3 downto 0);
    signal interp_addr : std_logic_vector(10 downto 0);
    signal interp_addr_valid : std_logic;
    signal filter_arbiter_count : std_logic_vector(1 downto 0) := "00";
    signal interp_dout : t_slv_16_arr(31 downto 0);
    type t_f_taps is array(3 downto 0) of t_slv_16_arr(31 downto 0);
    signal filterTaps : t_f_taps;
    signal filterValid : std_logic_vector(3 downto 0);
    signal interp_addr_valid_del1, interp_addr_valid_del2, interp_addr_valid_del3 : std_logic;
    signal filter_arbiter_count_del0, filter_arbiter_count_del1, filter_arbiter_count_del2, filter_arbiter_count_del3 : std_logic_vector(1 downto 0);
    signal filter_select_valid : std_logic_vector(3 downto 0);
    signal source_rdy : t_slv_2_arr(3 downto 0);
    signal data_valid, data_block : std_logic_vector(3 downto 0);
    signal data_valid_del1, data_valid_del2, data_valid_del3, data_valid_del4, data_valid_del5 : std_logic;
    signal read_delay_busy : std_logic_vector(3 downto 0);
    
    signal filter_select_valid_hold : std_logic_vector(3 downto 0);
    signal filter_select_hold : t_slv_11_arr(3 downto 0);
    signal filterDest : std_logic_vector(3 downto 0);
    signal interp_addr_dest, interp_addr_dest_del1, interp_addr_dest_del2, interp_addr_dest_del3 : std_logic;
    signal filter_select_dest : std_logic_vector(3 downto 0);
    --signal current_stream : std_logic_vector(10 downto 0);
    signal read_start_del : std_logic_vector(15 downto 0);
    signal poly_eval_time_ns : std_logic_vector(63 downto 0);
    signal data_block_del1,  data_block_del2,  data_block_del3,  data_block_del4,  data_block_del5 : std_logic;
    signal src_count_out_del1, src_count_out_del2, src_count_out_del3, src_count_out_del4, src_count_out_del5 : std_logic_vector(7 downto 0);
    
    signal interp_addr_valid_del5, interp_addr_valid_del4 : std_logic;
    signal filter_arbiter_count_del5, filter_arbiter_count_del4 : std_logic_vector(1 downto 0);
    signal interp_addr_dest_del5, interp_addr_dest_del4 : std_logic; 
    
begin

    ----------------------------------------------------------------------------------------------
    -- Control
    
    process(clk)
    begin
        if rising_edge(clk) then
            -- Evaluate all polynomials and then increment time by g_TIME_STEP
            poly_eval_start <= i_poly_eval_start; 
            -- which of the polynomial buffers to use, only read on i_start
            poly_eval_buf <= i_poly_eval_buf;
            poly_eval_time_ns <= i_poly_eval_time_ns;
            
            o_poly_eval_running <= poly_eval_running(0) or poly_eval_running(1);
            
            rst <= reset;
        end if;
    end process;

    ----------------------------------------------------------------------------------------------
    -- Configuration Memory for the Polynomial coefficients
    -- byte address
    args_bram_addr <=  i_args_addr(19) & i_args_addr(15 downto 8) & i_args_addr(6 downto 2) & "00";
    args_bram_wren(0) <= 
        '1' when i_args_wren = '1' and i_args_addr(7) = '0' and (unsigned(i_args_addr(18 downto 16)) = g_DATAGEN_128_INSTANCE) 
        else '0';
    args_bram_wren(1) <= 
        '1' when i_args_wren = '1' and i_args_addr(7) = '1' and (unsigned(i_args_addr(18 downto 16)) = g_DATAGEN_128_INSTANCE) 
        else '0';    
    
    process(clk)
    begin
        if rising_edge(clk) then
            if ((unsigned(i_args_addr(18 downto 16)) = g_DATAGEN_128_INSTANCE) and (i_args_addr(7) = '0')) then
                args_rd_mem0 <= '1';
            else
                args_rd_mem0 <= '0';
            end if;
            if ((unsigned(i_args_addr(18 downto 16)) = g_DATAGEN_128_INSTANCE) and (i_args_addr(7) = '1')) then
                args_rd_mem1 <= '1';
            else
                args_rd_mem1 <= '0';
            end if;
            
            args_rd_mem0_del1 <= args_rd_mem0;
            args_rd_mem1_del1 <= args_rd_mem1;
            
            args_rd_mem0_del2 <= args_rd_mem0_del1;
            args_rd_mem1_del2 <= args_rd_mem1_del1;
            
            o_args_addr <= i_args_addr;
            o_args_wren <= i_args_wren;
            o_args_wr_data <= i_args_wr_data;

            if i_args_rd_data_valid = '1' then
                o_args_rd_data <= i_args_rd_data;
                o_args_rd_Data_valid <= '1';
            elsif args_rd_mem0_del2 = '1' then
                o_args_rd_data <= args_bram_rddata(0);
                o_args_rd_data_valid <= '1';
            elsif args_rd_mem1_del2 = '1' then
                o_args_rd_data <= args_bram_rddata(1);
                o_args_rd_data_valid <= '1';
            else
                o_args_rd_data <= (others => '0');
                o_args_rd_data_valid <= '0';
            end if;
        end if;
    end process;
    
    ------------------------------------------------------------------------
    -- Interpolation filter coefficients
    -- Single ROM is shared between 4 destinations.
    -- Typically all 4 destinations will request at the same time.
    -- Requests are serviced round-robin.
    
    process(clk)
    begin
        if rising_edge(clk) then
            for i in 0 to 3 loop
                if filter_select_valid(i) = '1' then
                    filter_select_valid_hold(i) <= '1';
                    filter_select_hold(i) <= filter_select(i);
                elsif (unsigned(filter_arbiter_count) = i) then
                    filter_select_valid_hold(i) <= '0';
                end if;
            end loop;
            
            filter_arbiter_count <= std_logic_vector(unsigned(filter_arbiter_count) + 1);
            case filter_arbiter_count is
                when "00" => 
                    interp_addr <= filter_select_hold(0);
                    interp_addr_valid <= filter_select_valid_hold(0);
                    interp_addr_dest <= filter_select_dest(0);
                when "01" => 
                    interp_addr <= filter_select_hold(1);
                    interp_addr_valid <= filter_select_valid_hold(1);
                    interp_addr_dest <= filter_select_dest(1);
                when "10" => 
                    interp_addr <= filter_select_hold(2);
                    interp_addr_valid <= filter_select_valid_hold(2);
                    interp_addr_dest <= filter_select_dest(2);
                when others => 
                    interp_addr <= filter_select_hold(3);
                    interp_addr_valid <= filter_select_valid_hold(3);
                    interp_addr_dest <= filter_select_dest(3);
            end case;
            filter_arbiter_count_del0 <= filter_arbiter_count;

            -- 3 cycle latency for the interpolation ROM
            interp_addr_valid_del1 <= interp_addr_valid;
            filter_arbiter_count_del1 <= filter_arbiter_count_del0;
            interp_addr_dest_del1 <= interp_addr_dest;
            
            interp_addr_valid_del2 <= interp_addr_valid_del1;
            filter_arbiter_count_del2 <= filter_arbiter_count_del1;
            interp_addr_dest_del2 <= interp_addr_dest_del1;
            
            interp_addr_valid_del3 <= interp_addr_valid_del2;
            filter_arbiter_count_del3 <= filter_arbiter_count_del2;
            interp_addr_dest_del3 <= interp_addr_dest_del2;
            
            interp_addr_valid_del4 <= interp_addr_valid_del3;
            filter_arbiter_count_del4 <= filter_arbiter_count_del3;
            interp_addr_dest_del4 <= interp_addr_dest_del3;
            
            interp_addr_valid_del5 <= interp_addr_valid_del4;
            filter_arbiter_count_del5 <= filter_arbiter_count_del4;
            interp_addr_dest_del5 <= interp_addr_dest_del4;
            
            case filter_arbiter_count_del5 is
                when "00" => 
                    filterTaps(0) <= interp_dout;
                    filterValid(0) <= interp_addr_valid_del5;
                    filterDest(0) <= interp_addr_dest_del5;
                    filterValid(3 downto 1) <= "000";
                when "01" =>
                    filterTaps(1) <= interp_dout;
                    filterValid(1) <= interp_addr_valid_del5;
                    filterDest(1) <= interp_addr_dest_del5;
                    filterValid(3 downto 2) <= "00";
                    filterValid(0) <= '0';
                when "10" =>
                    filterTaps(2) <= interp_dout;
                    filterValid(2) <= interp_addr_valid_del5;
                    filterDest(2) <= interp_addr_dest_del5;
                    filterValid(3) <= '0';
                    filterValid(1 downto 0) <= "00";
                when others =>
                    filterTaps(3) <= interp_dout;
                    filterValid(3) <= interp_addr_valid_del5;
                    filterDest(3) <= interp_addr_dest_del5;
                    filterValid(2 downto 0) <= "000";
            end case;
            
        end if;
    end process;
    
    --interpRomi : datagen_interp_rom16
    --port map (
    --    clka  => clk,
    --    addra => interp_addr,
    --    douta => interp_dout
    --);
    
    interpRomi : entity vd_datagen_lib.rom32_wrapper
    port map (
        clk  => clk,
        -- Interpolation fraction is i_addr/2048
        -- 0 = left-most point, filter output is [0 0 ... 0 1 0 ... 0]
        -- 1024 = mid point filter.
        -- 2047 = right-most filter.
        i_addr => interp_addr,
        -- 5 cycle latency
        o_data => interp_dout  -- out t_slv_16_arr(31 downto 0)
    );
    
    
    poly_gen : for p in 0 to 1 generate
    
        vd_memi : entity vd_datagen_lib.datagen_config_mem
        port map ( 
            i_clk                   => clk,
            i_rst                   => rst,
            --
            i_host_data             => i_args_wr_data,
            i_host_addr             => args_bram_addr,
            i_host_wren             => args_bram_wren(p),
            o_host_return_data      => args_bram_rddata(p),
            -------------------------------------------------
            i_rd_addr               => config_mem_rd_addr(p),  -- in (12:0)
            o_rd_data               => config_mem_rd_data(p)  -- out (63:0)
        );
        
        poly_evali : entity vd_datagen_lib.datagen_poly_eval
        port map (
            clk  => clk,
            i_rst => rst,
            -- Control
            i_start    => poly_eval_start,      -- in std_logic; Evaluate all polynomials and then increment time by g_TIME_STEP
            i_coef_buf => poly_eval_buf,        -- in std_logic; Which of the polynomial buffers to use, only read on i_start
            i_time_ns  => poly_eval_time_ns,    -- in (63:0); Time to evaluate the polynomials at, nanoseconds, double precision floating point. 
            o_running  => poly_eval_running(p), -- out std_logic; Status; if '1', module is busy.
            -- read the config memory with 
            o_rd_addr  => config_mem_rd_addr(p), -- out (12:0);
            i_rd_data  => config_mem_rd_data(p), -- in (63:0); 3 clock latency.
            -----------------------------------------------------------------------
            -- Output to the interpolation modules
            -- total bits = 11 + 11 + 15 + 32 = 69
            o_sample_offset => src_sample_offset(p), -- out (10:0); -- Number of 1080ns samples to delay by
            o_interp_filter => src_interp_filter(p), -- out (10:0); -- Interpolation filter selection = fraction of a 1080 ns sample
            o_phase_offset  => src_phase_offset(p),  -- out (23:0); -- Phase correction
            -- Config info read from the config memory address 7:
            --  bits 14:0 = random number generator seed OR phase step for a sinusoid
            --  bits 15   = '0' to select pseudo-random numbers, '1' to select sinusoid
            --  bits 31:16 = Scale factor for the data
            o_config  => src_config(p),  -- out (31:0);
            o_startup => src_startup(p), -- out std_logic; High if first run after i_rst, use to initialise number generators.
            o_count   => src_count(p),   -- out (8:0); Which polynomial this is for ?
            o_valid   => src_valid(p),   -- out std_logic;
            o_last    => src_last(p),    -- out std_logic; Outputs valid for the last polynomial.
            --
            o_frame_count => src_frames(p) -- out (31:0)
        );
    
        singleSource_gen : for i in 0 to 1 generate
            singlesourci : entity vd_datagen_lib.dataGen_singleSource
            generic map(
                -- Either process even-indexed delays or odd-indexed delays
                -- as determined by i_count(0)
                g_ODD_DELAYS => i -- integer range 0 to 1   -- 0 for even indexed delays, 1 for odd indexed delays
            ) port map(
                clk  => clk, --  in std_logic;
                i_rst => rst,
                --------------------------------------------------
                -- Delay data
                i_sample_offset => src_sample_offset(p), -- in (10:0);
                i_interp_filter => src_interp_filter(p), -- in (10:0); Interpolation filter selection
                i_phase_offset  => src_phase_offset(p),  -- in (23:0); Phase correction
                i_config        => src_config(p),        -- in (31:0); config info, 14:0 = seed or sinusoid phase step, 15 = select sinusoid, 31:16 = scale
                i_startup       => src_startup(p),       -- in std_logic;
                i_count         => src_count(p),         -- in (8:0); Which polynomial this is for
                i_frame         => src_frames(p),        -- in std_logic; -- Which half of the double buffer to write this burst of delay data to.
                i_valid         => src_valid(p),         -- in std_logic;
                i_last          => src_last(p),          -- in std_logic;
                --o_space_available => poly_space_available(p*2+i), -- out std_logic; There is space available for a new set of polynomial data
                --------------------------------------------------
                -- Request a set of interpolation taps
                -- Pulse high on filterValid, after 4 to 7 clocks i_filterValid will respond
                -- with the requested filter taps.
                o_filter     => filter_select(p*2+i), -- out std_logic_vector(10 downto 0);
                o_filterDest => filter_select_dest(p*2+i), -- out std_logic; 1 bit of meta data to indicate which datasource pipeline this filter is for.
                o_filterValid => filter_select_valid(p*2+i), -- out std_logic;
                -- 16 x 16 bit filter taps
                i_filterTaps => filterTaps(p*2+i), -- in t_slv_16_arr(31 downto 0);
                i_filterDest => filterDest(p*2+i), -- in std_logic;
                i_filterValid => filterValid(p*2+i), -- in std_logic;
                --------------------------------------------------
                -- Synchronisation with other data sources.
                -- Trigger reading of the delay buffer
                i_read_delays => i_read_delays, -- in std_logic;
                i_read_buffer => i_read_buffer, -- in std_logic; Which of the 2 buffers to read from
                o_busy => read_delay_busy(p*2+i), -- out std_logic;                
                
                -- When data source has generated preload samples and is ready to start generating
                -- data samples, o_ready goes high, and we wait for i_ready to go high
                -- before proceeding to generate data samples.
                o_ready => source_rdy(p*2+i), -- out (1:0);
                i_ready => i_all_source_rdy,  -- in (1:0);
                --------------------------------------------------
                -- data output
                -- 40+40 bit complex
                o_data_re => data_re(p*2 + i),     -- out (39:0);
                o_data_im => data_im(p*2 + i),     -- out (39:0);
                o_src_count => src_count_out(p*2 + i), -- out (7:0);
                o_block   => data_block(p*2 + i),  -- out std_logic;
                o_valid   => data_valid(p*2 + i)   -- out std_logic; -- data valid when both o_valid and i_rdy are high
            );
        end generate;
    end generate;
    
    process(clk)
    begin
        if rising_edge(clk) then
        
            for i in 0 to 1 loop
                if source_rdy(0)(i) = '1' and source_rdy(1)(i) = '1' and source_rdy(2)(i) = '1' and source_rdy(3)(i) = '1' then
                    o_all_source_rdy(i) <= '1';
                else
                    o_all_source_rdy(i) <= '0';
                end if;
            end loop;
            
            if read_delay_busy = "0000" then
                o_read_delay_running <= '0';
            else
                o_read_delay_running <= '1';
            end if;
        end if;
    end process;
    
    ---------------------------------------------------------------
    -- Add up the four sky sources to get a single polarisation
    process(clk)
        variable data_re_int_v, data_im_int_v : std_logic_vector(17 downto 0);
        variable data_re_frac_v, data_im_frac_v : std_logic_vector(21 downto 0);
        variable re_tie_v, im_tie_v, re_round_up_v, im_round_up_v : std_logic;
    begin
        if rising_edge(clk) then
            data_re_sum01 <= std_logic_vector(unsigned(data_re(0)) + unsigned(data_re(1)));
            data_im_sum01 <= std_logic_vector(unsigned(data_im(0)) + unsigned(data_im(1)));
            data_re_sum23 <= std_logic_vector(unsigned(data_re(2)) + unsigned(data_re(3)));
            data_im_sum23 <= std_logic_vector(unsigned(data_im(2)) + unsigned(data_im(3)));
            src_count_out_del1 <= src_count_out(0); -- all src_count_out should be the same, just use 0.
            data_valid_del1 <= data_valid(0); -- all 4 data_valid signals should go high/low at the same time, so just use the first one.
            data_block_del1 <= data_block(0); -- likewise for data_block
            
            data_re_40bit <= std_logic_vector(unsigned(data_re_sum01) + unsigned(data_re_sum23));
            data_im_40bit <= std_logic_vector(unsigned(data_im_sum01) + unsigned(data_im_sum23));
            src_count_out_del2 <= src_count_out_del1;
            data_valid_del2 <= data_valid_del1;
            data_block_del2 <= data_block_del1;
            
            -- saturate : 40 bits => 8 bits
            -- Drop low 22 bits. Details regarding scaling and amplitude are in 
            -- the comments in the header of dataGen_filter.vhd
            data_re_int_v := data_re_40bit(39 downto 22);
            data_re_frac_v := data_re_40bit(21 downto 0);
            data_im_int_v := data_im_40bit(39 downto 22);
            data_im_frac_v := data_im_40bit(21 downto 0);
            
            if data_re_frac_v = "1000000000000000000000" then
                re_tie_v := '1';
            else
                re_tie_v := '0';
            end if;
            
            if data_im_frac_v = "1000000000000000000000" then
                im_tie_v := '1';
            else
                im_tie_v := '0';
            end if;
            
            if ((re_tie_v = '1' and data_re_int_v(0) = '1') or (re_tie_v = '0' and data_re_frac_v(21) = '1')) then
                -- tie and odd, or > 0.5
                re_round_up_v := '1';
            else
                re_round_up_v := '0';
            end if;
            
            if ((im_tie_v = '1' and data_im_int_v(0) = '1') or (im_tie_v = '0' and data_im_frac_v(21) = '1')) then
                -- tie and odd, or > 0.5
                im_round_up_v := '1';
            else
                im_round_up_v := '0';
            end if;
            
            if ((signed(data_re_int_v) > 127) or ((signed(data_re_int_v) = 127) and (re_round_up_v = '1')) or 
                (signed(data_re_int_v) < -128) or (signed(data_re_int_v) = -128 and (re_round_up_v = '0'))) then
                saturate_re <= '1';
            else
                saturate_re <= '0';
            end if;
            
            if ((signed(data_im_int_v) > 127) or ((signed(data_im_int_v) = 127) and (im_round_up_v = '1')) or 
                (signed(data_im_int_v) < -128) or (signed(data_im_int_v) = -128 and (im_round_up_v = '0'))) then
                saturate_im <= '1';
            else
                saturate_im <= '0';
            end if;            
            re_round_up <= re_round_up_v;
            im_round_up <= im_round_up_v;
            data_re_int_8bit <= data_re_int_v(7 downto 0);
            data_im_int_8bit <= data_im_int_v(7 downto 0);
            data_valid_del3 <= data_valid_del2;
            data_block_del3 <= data_block_del2;
            src_count_out_del3 <= src_count_out_del2;
            
            --
            if saturate_re = '1' or saturate_im = '1' then
                data_re_final <= "10000000"; -- -128 to mark as RFI
                data_im_final <= "10000000";
            else
                if re_round_up = '1' then
                    data_re_final <= std_logic_vector(unsigned(data_re_int_8bit) + 1);
                else
                    data_re_final <= data_re_int_8bit;
                end if;
                if im_round_up = '1' then
                    data_im_final <= std_logic_vector(unsigned(data_im_int_8bit) + 1);
                else
                    data_im_final <= data_im_int_8bit;
                end if;
            end if;
            data_valid_del4 <= data_valid_del3;
            data_block_del4 <= data_block_del3;
            src_count_out_del4 <= src_count_out_del3;
            
            if (data_valid_del4 = '1') then
                dvec(255 downto 240) <= data_im_final & data_re_final;
                dvec(239 downto 0) <= dvec(255 downto 16);
            end if;
            data_valid_del5 <= data_valid_del4;
            data_block_del5 <= data_block_del4;
            src_count_out_del5 <= src_count_out_del4;
            
            if (data_valid_del5 = '1' and data_valid_del4 = '0') then
                -- falling edge of data_valid, dvec holds the final value
                -- i.e. all 16 samples generated in this burst.
                -- All the different dataGen_128 modules load o_data_samples simultaneously,
                -- then data is shifted out through the daisy chain in subsequent clock cycles.
                o_data_samples <= dvec;
                o_data_stream <= std_logic_vector(to_unsigned(g_DATAGEN_128_INSTANCE,3)) & src_count_out_del5;
                o_data_block <= data_block_del5;
                o_data_valid <= '1';
            else
                o_data_samples <= i_data_samples;
                o_data_stream <= i_data_stream;
                o_data_block <= i_data_block;
                o_data_valid <= i_data_valid;
            end if;
            
        end if;
    end process;
    
end Behavioral;

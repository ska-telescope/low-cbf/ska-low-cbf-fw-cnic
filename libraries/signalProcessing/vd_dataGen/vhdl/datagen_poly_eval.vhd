----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey
-- 
-- Create Date: 04 june 2023 01:11:00 AM
-- Module Name: datagen_poly_eval - Behavioral
-- Description: 
--  Evaluate Delay polynomials.
--  Each polynomial is defined by 6 double precision values that are read from the
--  memory (o_rd_addr, i_rd_data).
--  A seventh value defines the sky frequency in cycles per ns = GHz = (freq in Hz) * 1e-9.
--  This module evaluates 512 polynomials.
--     delay = c0 + c1*x + c2*x^2 + c3*x^3 + c4*x^4 + c5*x^5
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library vd_datagen_lib;
use IEEE.NUMERIC_STD.ALL;
library common_lib;
USE common_lib.common_pkg.ALL;

entity datagen_poly_eval is
    generic(
        -- Amount of time to increment the argument of the polynomial by,
        -- in nanoseconds, as a double precision value
        --g_TIME_STEP : std_logic_vector := x"40D3880000000000" -- = 20000 = 5ns per sample, 4000 samples
        g_TIME_STEP : std_logic_vector := x"40D0E00000000000" -- = 17280 = 1080 ns per sample, 16 samples 
    );
    port(
        clk : in std_logic;
        -- First output after a reset will reset the data generation
        i_rst : in std_logic;
        -- Control
        i_start    : in std_logic; -- Evaluate all polynomials
        i_coef_buf : in std_logic; -- which of the polynomial buffers to use, only read on i_start
        i_time_ns  : in std_logic_vector(63 downto 0); -- Time to evaluate the polynomials at, nanoseconds, double precision floating point. 
        o_running  : out std_logic; -- status; if '1', module is busy.
        -- read the config memory (to get polynomial coefficients)
        o_rd_addr  : out std_logic_vector(12 downto 0);
        i_rd_data  : in std_logic_vector(63 downto 0);  -- 3 clock latency.
        -----------------------------------------------------------------------
        -- Output to the interpolation modules
        -- total bits = 11 + 11 + 16 + 32 = 70
        o_sample_offset : out std_logic_vector(10 downto 0); -- Number of 1080ns samples to delay by
        o_interp_filter : out std_logic_vector(10 downto 0); -- Interpolation filter selection = fraction of a 1080 ns sample
        o_phase_offset  : out std_logic_vector(23 downto 0); -- Phase correction
        -- Config info read from the config memory address 7:
        --  bits 14:0 = random number generator seed OR phase step for a sinusoid
        --  bits 15   = '0' to select pseudo-random numbers, '1' to select sinusoid
        --  bits 31:16 = Scale factor for the data
        o_config : out std_logic_vector(31 downto 0);
        o_startup : out std_logic;  -- high if first run after i_rst, use to initialise number generators.
        o_count  : out std_logic_vector(8 downto 0);  -- which polynomial this is for
        o_valid  : out std_logic;
        o_last   : out std_logic; -- Outputs valid for the last polynomial
        -----------------------------------------------------------------------
        -- monitoring
        o_frame_count : out std_logic_vector(31 downto 0) -- count of the number of i_start since rst.
    );
    -- prevent optimisation across module boundaries.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of datagen_poly_eval : entity is "yes";
end datagen_poly_eval;

architecture Behavioral of datagen_poly_eval is

    -- 1e-9
    constant c_fp64_1eMinus9 : std_logic_vector(63 downto 0) := x"3E112E0BE826D695";
    -- Need to multiply time in ns by the period in ns to get number of samples,
    -- divide by 1080 = multiply by 1/1080
    constant c_fp64_rate : std_logic_vector(63 downto 0) := x"3F4E573AC901E574"; -- = 1/1080
    
    
    COMPONENT fp64_add
    PORT (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    END COMPONENT;
    
    COMPONENT fp64_mult
    PORT (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid : IN STD_LOGIC;
        s_axis_b_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    END COMPONENT;
    
    COMPONENT fp64_to_int
    PORT (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    END COMPONENT;
    
    --signal interpolator_taps : std_logic_vector(95 downto 0);
    --signal interpolator_addr : std_logic_vector(10 downto 0);
    
    signal poly_term : std_logic_vector(63 downto 0);
    signal time_ns : std_logic_vector(63 downto 0);
    signal time_s : std_logic_vector(63 downto 0);
    signal running : std_logic := '0';
    signal start_del1, start_del2 : std_logic := '0';
    signal phase_del : t_slv_4_arr(32 downto 0);
    signal phase_count_del : t_slv_11_arr(32 downto 0);
    signal short_phase : std_logic;
    signal coef_buf : std_logic;
    
    signal fp64_add_valid_in, fp64_mult_valid_in, fp64_to_int_valid_in : std_logic := '0';
    signal fp64_add_din0, fp64_add_din1, fp64_add_dout : std_logic_vector(63 downto 0);
    signal fp64_mult_din0, fp64_mult_din1, fp64_mult_dout : std_logic_vector(63 downto 0);
    signal fp64_add_valid_out, fp64_mult_valid_out, fp64_to_int_valid_out : std_logic := '0';
    signal fp64_to_int_din, fp64_to_int_dout : std_logic_vector(63 downto 0);
    signal cur_val_rdAddr : std_logic_vector(8 downto 0); 
    signal cur_val_rdData : std_logic_vector(63 downto 0);
    signal cur_val_wrAddr : std_logic_vector(8 downto 0);
    signal cur_val_wrEn : std_logic := '0';
    signal cur_val_wrData : std_logic_vector(63 downto 0);
    --signal interpolator_addr_valid, interpolator_addr_valid_del1, interpolator_addr_valid_del2 : std_logic;
    signal offset_del1, offset_del2 : std_logic_vector(19 downto 0);
    --signal rd_data_del : t_slv_32_arr(22 downto 0);
    signal frame_count : std_logic_vector(31 downto 0) := x"00000000";
    signal startup : std_logic := '0';
    signal long_phase : std_logic := '0';
    signal phase_offset_24bit : std_logic_vector(23 downto 0) := x"000000";
    signal phase_offset_frac : std_logic := '0';
    signal sample_offset_int : std_logic_vector(10 downto 0);
    signal interp_filter_int : std_logic_vector(10 downto 0);
    
begin
    
    process(clk)
    begin
        if rising_edge(clk) then
            -- Polynomial evaluation steps :
            -- Actual number of clocks used is a few more than listed here, to allow
            -- time for the previous operation to complete.
            -- Phase 0: 512 clocks
            --   Read c0, write to state_bram
            -- Phase 1: 16 clocks
            --   multiply time_ns by 1e-9 to get time_s, also store in poly_term
            -- Phase 2: 512 clocks
            --   Read c1, multiply by poly_term, add to state_bram
            --   (state_bram now hold c0 + c1*x)
            -- Phase 3: 16 clocks
            --   multiply: time_s x poly_term to get poly_term = x^2
            -- Phase 4: 512 clocks
            --   read c2, multiply by poly_term to get c2 * x^2, add to state_bram
            --   (state_bram now holds c0 + c1*x + c2*x^2)
            -- Phase 5: 16 clocks
            --   multiply: time_s x poly_term to get poly_term = x^3
            -- phase 6: 512 clocks
            --   read c3, multiply by poly_term to get c3 * x^3, add to state_bram
            -- phase 7: 16 clocks
            --   multiply: time_s x poly_term to get poly_term = x^4
            -- phase 8: 512 clocks
            --   read c4, multiply by poly_term to get c4 * x^4, add to state_bram
            -- phase 9: 16 clocks
            --   multiply: time_s x poly_term to get poly_term = x^5
            -- phase 10: 512 clocks
            --   read c5, multiply by poly_term to get c5 * x^5, add to state_bram
            -- phase 11: 16 clocks
            --   Do nothing.
            -- phase 12: 1024 clocks
            --   Read state_bram to get delay, 
            --     - Read c6 (= sky frequency in GHz), multiply by delay,
            --       Convert to an integer, fractional bits are the phase correction
            --     - Read c7 (= control info : random seed, sinusoid step, scale)
            --       multiply delay by (1/1080e-9) to get number of 1080ns samples, 
            --       convert to an integer
            --   send:
            --       - c7 control info
            --       - sample offset 
            --       - interpolation filter (=fractional offset)
            --       - phase correction
            --   
            
            start_del1 <= i_start;
            start_del2 <= start_del1;
            o_running <= running;
            if i_start = '1' and start_del1 = '0' then -- rising edge of i_start, run processing
                time_ns <= i_time_ns;
                phase_del(0) <= "0000";
                phase_count_del(0) <= "00000000000";
                coef_buf <= i_coef_buf;
                running <= '1';
            elsif (running = '1') then
                if ((unsigned(phase_del(0)) = 12) and (unsigned(phase_count_del(0)) > 1070)) then
                    running <= '0';
                    phase_del(0) <= "0000";
                    phase_count_del(0) <= "00000000000";
                elsif (short_phase = '1') and (unsigned(phase_count_del(0)) >= 15) then
                    phase_count_del(0) <= "00000000000";
                    phase_del(0) <= std_logic_vector(unsigned(phase_del(0)) + 1);
                elsif (long_phase = '1') and (unsigned(phase_count_del(0)) > 549) then 
                    phase_count_del(0) <= "00000000000";
                    phase_del(0) <= std_logic_vector(unsigned(phase_del(0)) + 1);
                else
                    phase_count_del(0) <= std_logic_vector(unsigned(phase_count_del(0)) + 1);
                end if;
            end if;
            
            if ((unsigned(phase_del(0)) = 1) or (unsigned(phase_del(0)) = 3) or (unsigned(phase_del(0)) = 5) or 
                (unsigned(phase_del(0)) = 7) or (unsigned(phase_del(0)) = 9) or (unsigned(phase_del(0)) = 11)) then
                -- Just doing one fp64 operation, so this is a short phase, i.e. <16 clocks
                short_phase <= '1';
            else
                short_phase <= '0';
            end if;
            
            if ((unsigned(phase_del(0)) = 0) or (unsigned(phase_del(0)) = 2) or (unsigned(phase_del(0)) = 4) or 
                (unsigned(phase_del(0)) = 6) or (unsigned(phase_del(0)) = 8) or (unsigned(phase_del(0)) = 10)) then
                -- Processing all the polynomials, so long phase, i.e. 512 clocks
                long_phase <= '1';
            else
                long_phase <= '0';
            end if;
            
            phase_del(32 downto 1) <= phase_del(31 downto 0);
            phase_count_del(32 downto 1) <= phase_count_del(31 downto 0);
            
            -- Reading from the coefficient memory
            -- Memory has 8 entries per polynomial 
            --  = 6 coefficients + 1 control info + 1 unused.
            o_rd_addr(12) <= coef_buf;
            case phase_del(0) is
                when "0000" =>  -- read c0
                    o_rd_addr(2 downto 0) <= "000";
                    o_rd_addr(11 downto 3) <= phase_count_del(0)(8 downto 0);
                when "0010" =>  -- read c1
                    o_rd_addr(2 downto 0) <= "001";
                    o_rd_addr(11 downto 3) <= phase_count_del(0)(8 downto 0);
                when "0100" =>  -- read c2
                    o_rd_addr(2 downto 0) <= "010";
                    o_rd_addr(11 downto 3) <= phase_count_del(0)(8 downto 0);
                when "0110" =>  -- read c3
                    o_rd_addr(2 downto 0) <= "011";
                    o_rd_addr(11 downto 3) <= phase_count_del(0)(8 downto 0);
                when "1000" =>  -- read c4
                    o_rd_addr(2 downto 0) <= "100";
                    o_rd_addr(11 downto 3) <= phase_count_del(0)(8 downto 0);
                when "1010" =>  -- read c5
                    o_rd_addr(2 downto 0) <= "101";
                    o_rd_addr(11 downto 3) <= phase_count_del(0)(8 downto 0);
                when "1100" =>  -- read c6 and read control info
                    if phase_count_del(0)(0) = '0' then
                        -- Read c6 ( = Sky frequency in GHz)
                        o_rd_addr(2 downto 0) <= "110";
                        -- Every second clock processes a new output, so use (9 downto 1)
                        o_rd_addr(11 downto 3) <= phase_count_del(0)(9 downto 1);
                    else
                        -- Read control info (=seed, sinusoid step, scale factor)
                        o_rd_addr(2 downto 0) <= "111";
                        -- Delay Read address to align with the data output for phase 12.
                        -- In phase 12, we do 
                        --   Read c6  : 4+1 clocks
                        --   multiply : 12+1 clocks
                        --   convert to integer : 6 clocks
                        --   +1 (output every second clock)
                        --   Less 4 for read latency = 4+1+12+1+6+1-4 = 21
                        o_rd_addr(11 downto 3) <= phase_count_del(21)(9 downto 1);
                    end if;
                when others =>  -- Unused.
                    o_rd_addr(2 downto 0) <= "111";
                    o_rd_addr(11 downto 3) <= phase_count_del(0)(8 downto 0);
            end case;
            
            -- Reading + writing from/to the current value BRAM
            if ((phase_del(13) = "0010") or (phase_del(13) = "0100") or (phase_del(13) = "0110") or (phase_del(13) = "1000") or (phase_del(13) = "1010")) then
                -- Phase 2,4,6,8,10 : Read c1 to c5, multiply by poly_term, add to state_bram
                -- Delay 4 to c1, then 1 (fp64_mult_din0), then 12 (multiplier) - 3 (cur_val memory latency, cur_val_rdData needs to align with the multiplier output)
                -- Delay = 4+1+12-3 = 14 
                -- ==> Use phase_del(13) so cur_val_rdAddr is valid on del(14) 
                cur_val_rdAddr <= phase_count_del(13)(8 downto 0);
            else
                -- phase 12 : read c6 and c7 takes two cycles, so we only need the cur_val (=time offset in ns) every second clock.
                --  
                cur_val_rdAddr <= phase_count_del(0)(9 downto 1);
            end if;
            
            if (phase_del(4) = "0000" and (unsigned(phase_count_del(4)) < 512)) then
                -- phase 0, write c0 to current value BRAM
                -- 4 clock latency
                cur_val_wrAddr <= phase_count_del(4)(8 downto 0);
                cur_val_wrData <= i_rd_data;
                cur_val_wrEn <= '1';
            elsif (((phase_del(32) = "0010") or (phase_del(32) = "0100") or (phase_del(32) = "0110") or
                    (phase_del(32) = "1000") or (phase_del(32) = "1010")) and 
                   (unsigned(phase_count_del(32)) < 512)) then
                -- Phase 2,4,6,8,10 : Read c1 to c5, multiply by poly_term, add to state_bram
                -- Delay 4 to c1, then 1 (fp64_mult_din0), then 12 (multiplier) then 1 (fp64_add_din) then 14 (adder)
                -- Delay = 4+1+12+1+14 = 32 
                cur_val_wrAddr <= phase_count_del(32)(8 downto 0);
                cur_val_wrEn <= '1';
                cur_val_wrData <= fp64_add_dout;
            else
                cur_val_wrEn <= '0';
            end if;
            
            -- Input to the double precision multiplier
            if (phase_del(0) = "0001" and unsigned(phase_count_del(0)) = 0) then
                -- multiply time_ns by 1e-9 to get time_s
                fp64_mult_valid_in <= '1';
                fp64_mult_din0 <= time_ns;
                fp64_mult_din1 <= c_fp64_1eMinus9;
            elsif ((phase_del(4) = "0010" or phase_del(4) = "0100" or phase_del(4) = "0110" or phase_del(4) = "1000" or phase_del(4) = "1010") and 
                   (unsigned(phase_count_del(4)) < 512)) then
                --Phase 2: 512 clocks
                --   Read c1, multiply by poly_term
                --   delayed by 4 : o_rd_addr, mem latency, mem latency, mem latency
                -- Also phase 4 : c2 * poly_term
                --      phase 6 : c3 * poly_term
                --      phase 8 : c4 * poly_term
                --      phase 10 : c5 * poly_term
                fp64_mult_valid_in <= '1';
                fp64_mult_din0 <= poly_term;
                fp64_mult_din1 <= i_rd_data;
            elsif (((phase_del(0) = "0011") or (phase_del(0) = "0101") or (phase_del(0) = "0111") or (phase_del(0) = "1001")) and 
                   (unsigned(phase_count_del(0)) = 0)) then
                -- Phase 3: 16 clocks
                --   multiply: time_s x poly_term to get poly_term = x^2
                -- Also phase 5 : time_s x poly_term to get poly_term = x^3
                -- Also phase 7 : time_s x poly_term to get poly_term = x^4
                -- Also phase 9 : time_s x poly_term to get poly_term = x^5
                fp64_mult_valid_in <= '1';
                fp64_mult_din0 <= poly_term;
                fp64_mult_din1 <= time_s;
            elsif (phase_del(4) = "1100") and (unsigned(phase_count_del(4)) < 1024) then
                -- phase 12: 1024 clocks
                --   even counts : Delay * sky_frequency (=c6) [to get phase correction]
                --   odd  counts : Delay * (1/1080)            [to get integer and fractional sample offset]
                --    delayed by 4 clocks : cur_val_rdAddr, mem latency, mem latency, mem latency
                fp64_mult_valid_in <= '1';
                fp64_mult_din0 <= cur_val_rdData;
                if (phase_count_del(4)(0) = '0') then
                    fp64_mult_din1 <= i_rd_data;  -- c6 = sky frequency
                else
                    fp64_mult_din1 <= c_fp64_rate; -- = 1/1080
                end if;
            else
                fp64_mult_valid_in <= '0';
                fp64_mult_din0 <= (others => '0');
                fp64_mult_din1 <= (others => '0');
            end if;
            
            if phase_del(0) = "0001" and fp64_mult_valid_out = '1' then
                time_s <= fp64_mult_dout;
            end if;
            
            if ((phase_del(0) = "0001" or phase_del(0) = "0011" or phase_del(0) = "0101" or 
                 phase_del(0) = "0111" or phase_del(0) = "1001") and fp64_mult_valid_out = '1') then
                poly_term <= fp64_mult_dout;
            end if;
            
            -- Input to the double precision adder
            if ((phase_del(17) = "0010" or phase_del(17) = "0100" or phase_del(17) = "0110" or
                 phase_del(17) = "1000" or phase_del(17) = "1010")
                and (unsigned(phase_count_del(17)) < 512)) then
                -- Phase 2: 512 clocks
                --   Read c1, multiply by poly_term, add to state_bram
                -- 17 clock latency : 4 to get input to the multiplier + 1 register + 12 for latency of the multiplier
                -- Also phase 4  : read c2, multiply by poly_term to get c2 * x^2, add to state_bram
                -- Also phase 6  : read c3, multiply by poly_term to get c3 * x^3, add to state_bram
                -- Also phase 8  : read c4, multiply by poly_term to get c4 * x^4, add to state_bram
                -- Also phase 10 : read c5, multiply by poly_term to get c5 * x^5, add to state_bram
                fp64_add_valid_in <= '1';
                fp64_add_din0 <= fp64_mult_dout;
                fp64_add_din1 <= cur_val_rdData;
            else
                fp64_add_valid_in <= '0';
                fp64_add_din0 <= (others => '0');
                fp64_add_din1 <= (others => '0');
            end if;
            
            -- Conversion to integer
            if ((phase_del(17) = "1100") and (unsigned(phase_count_del(17)) < 1024)) then
                -- phase 12: 1024 clocks
                -- Alternates between :
                --   - delay * c6 (sky frequency GHz) = phase correction
                --   - delay * (1/1080)               = delay in samples
                -- latency 4 (to multiplier input) + 1 (fp64_mult_din) + 12 (multiplier)
                fp64_to_int_valid_in <= '1';
            else
                fp64_to_int_valid_in <= '0';
            end if;
            fp64_to_int_din <= fp64_mult_dout; -- fp64_to_int is only used in phase 12
            
            -- Module Outputs
            -- latency 4 (to multiplier input) + 1 (fp64_mult_din) + 12 (multiplier) + 1 (fp64_to_int_din) + 6 (int conversion)
            --  = 4+1+12+1+6 = 24
            phase_offset_24bit <= fp64_to_int_dout(31 downto 8);
            phase_offset_frac <= fp64_to_int_dout(7);
            -- Replaced 16 bit phase with rounded 24-bit phase.
            --if ((phase_del(24) = "1100") and (phase_count_del(24)(0) = '0')) then
            --    o_phase_offset <= fp64_to_int_dout(31 downto 16);
            --end if;
            if ((phase_del(25) = "1100") and (phase_count_del(25)(0) = '0')) then
                if phase_offset_frac = '0' then
                    o_phase_offset <= phase_offset_24bit;
                else
                    o_phase_offset <= std_logic_vector(unsigned(phase_offset_24bit) + 1);
                end if;
            
                -- Replace with rounded version.
                --o_sample_offset <= fp64_to_int_dout(42 downto 32);
                --o_interp_filter <= fp64_to_int_dout(31 downto 21);
                
                if fp64_to_int_dout(31 downto 20) = "111111111111" then
                    o_sample_offset <= std_logic_vector(unsigned(sample_offset_int) + 1);
                    o_interp_filter <= "00000000000";
                else
                    o_sample_offset <= sample_offset_int;
                    if fp64_to_int_dout(20) = '0' then
                        o_interp_filter <= interp_filter_int;
                    else
                        o_interp_filter <= std_logic_vector(unsigned(interp_filter_int) + 1);
                    end if;
                end if;
                
                o_count <= phase_count_del(25)(9 downto 1);
                if (unsigned(phase_count_del(25)) < 1024) then
                    o_valid <= '1';
                else
                    o_valid <= '0';
                end if;
                if phase_count_del(25)(9 downto 1) = "111111111" then
                    o_last <= '1';
                else
                    o_last <= '0';
                end if;
            else
                o_valid <= '0';
                o_last <= '0';
            end if;
            
            --rd_data_del(0) <= i_rd_data(31 downto 0);  -- rd_data_del(0) aligns with phase_del(6)
            --rd_data_del(22 downto 1) <= rd_data_del(21 downto 0);
            o_config <= i_rd_data(31 downto 0);
            
            if i_rst = '1' then
                startup <= '1';
            elsif ((phase_del(25) = "1100") and (unsigned(phase_count_del(25)) > 1024)) then
                startup <= '0';
            end if;
            o_startup <= startup;

            if i_rst = '1' then
                frame_count <= (others => '0');
            elsif ((phase_del(25) = "1100") and (unsigned(phase_count_del(25)) = 1024)) then
                frame_count <= std_logic_vector(unsigned(frame_count) + 1);
            end if;
            
        end if;
    end process;
    
    sample_offset_int <= fp64_to_int_dout(42 downto 32);
    interp_filter_int <= fp64_to_int_dout(31 downto 21);
    
    o_frame_count <= frame_count;
    
    -- Double precision floating point adder, 14 clock latency
    fp64_addi : fp64_add
    PORT map (
        aclk => clk, -- 
        s_axis_a_tvalid => fp64_add_valid_in, -- IN STD_LOGIC;
        s_axis_a_tdata  => fp64_add_din0, -- IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        s_axis_b_tvalid => fp64_add_valid_in, -- IN STD_LOGIC;
        s_axis_b_tdata  => fp64_add_din1, -- IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid => fp64_add_valid_out, --  OUT STD_LOGIC;
        m_axis_result_tdata  => fp64_add_dout  -- OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    );
    
    -- double precision floating point multiplier, 12 clock latency
    fp64_multi : fp64_mult
    PORT MAP (
        aclk => clk,
        s_axis_a_tvalid => fp64_mult_valid_in,
        s_axis_a_tdata => fp64_mult_din0,
        s_axis_b_tvalid => fp64_mult_valid_in,
        s_axis_b_tdata => fp64_mult_din1,
        m_axis_result_tvalid => fp64_mult_valid_out,
        m_axis_result_tdata => fp64_mult_dout
    );
    
    -- Double precision to 32.32 int, 6 clock latency
    fp64_to_inti : fp64_to_int
    PORT MAP (
        aclk => clk,
        s_axis_a_tvalid => fp64_to_int_valid_in,
        s_axis_a_tdata => fp64_to_int_din,
        m_axis_result_tvalid => fp64_to_int_valid_out,
        m_axis_result_tdata => fp64_to_int_dout
    );
    
    -- Stores the current value of the polynomials as they are being evaluated.
    -- so after phase 0, all the c0, then after phase 1 (c0 + c1*x), then (c0 + c1*x + c2*x^2) etc.
    cur_vali : entity vd_datagen_lib.poly_cur_val_bram
    port map (
        clk  => clk,
        -- Write side
        wrAddr => cur_val_wrAddr, -- in(8:0)
        wrEn   => cur_val_wrEn,   -- in std_logic;
        wrData => cur_val_wrData, -- in(63:0)
        -- Read side, 3 clock latency
        rdAddr => cur_val_rdAddr, -- in(8:0)
        rdData => cur_val_rdData  -- out(63:0)
    );
    
    
end Behavioral;

----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 08/05/2023
-- Design Name: 
-- Module Name: vd_axi_bram_wrapper
--  
-- Description: 
--      Created to deal with verbose creation of mappings when compiling in Vitis
----------------------------------------------------------------------------------

library IEEE, spead_lib, signal_processing_common;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;


entity vd_datagen_axi_bram_wrapper is
    Port ( 
        i_clk : in std_logic;
        i_rst : in std_logic;

        i_clk_vd        : in std_logic;
        i_clk_vd_reset  : in std_logic;
        -------------------------------------------------------
        -- Block ram interface
        o_bram_wrEn    : out std_logic;
        -- 2^19 = 1 MByte, transactions use 4-byte words so low 2 bits are always "00"
        o_bram_addr    : out std_logic_vector(19 downto 0); 
        o_bram_wrdata  : out std_logic_vector(31 downto 0);
        -- 11 clock read latency required (from o_bram_addr)
        i_bram_rddata  : in std_logic_vector(31 downto 0);
        ------------------------------------------------------
        -- AXI full interface
        i_vd_full_axi_mosi : in  t_axi4_full_mosi;
        o_vd_full_axi_miso : out t_axi4_full_miso
    );
end vd_datagen_axi_bram_wrapper;

architecture Behavioral of vd_datagen_axi_bram_wrapper is

    COMPONENT axi_bram_ctrl_vd_datagen
    PORT (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC;
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC;
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        bram_rst_a : OUT STD_LOGIC;
        bram_clk_a : OUT STD_LOGIC;
        bram_en_a : OUT STD_LOGIC;
        bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
        bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0));
    END COMPONENT;
    
    COMPONENT axi_clock_converter_addr20b
        Port ( 
          s_axi_aclk : in STD_LOGIC;
          s_axi_aresetn : in STD_LOGIC;
          s_axi_awaddr : in STD_LOGIC_VECTOR ( 19 downto 0 );
          s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
          s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
          s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
          s_axi_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
          s_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
          s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
          s_axi_awregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
          s_axi_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
          s_axi_awvalid : in STD_LOGIC;
          s_axi_awready : out STD_LOGIC;
          s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
          s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
          s_axi_wlast : in STD_LOGIC;
          s_axi_wvalid : in STD_LOGIC;
          s_axi_wready : out STD_LOGIC;
          s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
          s_axi_bvalid : out STD_LOGIC;
          s_axi_bready : in STD_LOGIC;
          s_axi_araddr : in STD_LOGIC_VECTOR ( 19 downto 0 );
          s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
          s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
          s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
          s_axi_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
          s_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
          s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
          s_axi_arregion : in STD_LOGIC_VECTOR ( 3 downto 0 );
          s_axi_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
          s_axi_arvalid : in STD_LOGIC;
          s_axi_arready : out STD_LOGIC;
          s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
          s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
          s_axi_rlast : out STD_LOGIC;
          s_axi_rvalid : out STD_LOGIC;
          s_axi_rready : in STD_LOGIC;
          m_axi_aclk : in STD_LOGIC;
          m_axi_aresetn : in STD_LOGIC;
          m_axi_awaddr : out STD_LOGIC_VECTOR ( 19 downto 0 );
          m_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
          m_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
          m_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
          m_axi_awlock : out STD_LOGIC_VECTOR ( 0 to 0 );
          m_axi_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
          m_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
          m_axi_awregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
          m_axi_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
          m_axi_awvalid : out STD_LOGIC;
          m_axi_awready : in STD_LOGIC;
          m_axi_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
          m_axi_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
          m_axi_wlast : out STD_LOGIC;
          m_axi_wvalid : out STD_LOGIC;
          m_axi_wready : in STD_LOGIC;
          m_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
          m_axi_bvalid : in STD_LOGIC;
          m_axi_bready : out STD_LOGIC;
          m_axi_araddr : out STD_LOGIC_VECTOR ( 19 downto 0 );
          m_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
          m_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
          m_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
          m_axi_arlock : out STD_LOGIC_VECTOR ( 0 to 0 );
          m_axi_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
          m_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
          m_axi_arregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
          m_axi_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
          m_axi_arvalid : out STD_LOGIC;
          m_axi_arready : in STD_LOGIC;
          m_axi_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
          m_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
          m_axi_rlast : in STD_LOGIC;
          m_axi_rvalid : in STD_LOGIC;
          m_axi_rready : out STD_LOGIC
        );
    END COMPONENT;

    signal clk                      : std_logic;
    signal reset                    : std_logic;
    signal reset_n                  : std_logic;

    signal clk_vd                   : std_logic;
    signal reset_vd                 : std_logic;
    signal reset_vd_n               : std_logic;

    signal bram_en : std_logic;
    signal bram_we_byte : std_logic_vector(3 downto 0);

    signal axi_mosi : t_axi4_full_mosi;
    signal axi_miso : t_axi4_full_miso;    
    signal awlock_slv : std_logic_vector(0 downto 0);
    signal arlock_slv : std_logic_vector(0 downto 0);

    signal axi_mosi_arlock : std_logic_vector(0 downto 0);
    signal axi_mosi_awlock : std_logic_vector(0 downto 0);

    signal ARGs_rstn_in_clk_vd_domain : std_logic;


begin

    clk         <= i_clk;
    reset       <= i_rst;
    reset_n     <= NOT i_rst;

    clk_vd      <= i_clk_vd;
    reset_vd    <= i_clk_vd_reset;
    reset_vd_n  <= NOT i_clk_vd_reset;

    --------------------------------------------------------------
    sync_packet_registers_sig : entity signal_processing_common.sync
    Generic Map (
        USE_XPM     => true,
        WIDTH       => 1
    )
    Port Map ( 
        Clock_a                 => clk,
        Clock_b                 => clk_vd,
        
        data_in(0)              => reset_n,

        data_out(0)             => ARGs_rstn_in_clk_vd_domain
    );
    --------------------------------------------------------------


    awlock_slv(0)   <= i_vd_full_axi_mosi.awlock;
    arlock_slv(0)   <= i_vd_full_axi_mosi.arlock;

    KRNL_VD_AXI_clock_convert : axi_clock_converter_addr20b
    port map (
        s_axi_aclk      => clk,
        s_axi_aresetn   => reset_n,
        s_axi_awaddr    => i_vd_full_axi_mosi.awaddr(19 downto 0),
        s_axi_awlen     => i_vd_full_axi_mosi.awlen,
        s_axi_awsize    => i_vd_full_axi_mosi.awsize,
        s_axi_awburst   => i_vd_full_axi_mosi.awburst,
        s_axi_awlock    => awlock_slv,
        s_axi_awcache   => i_vd_full_axi_mosi.awcache,
        s_axi_awprot    => i_vd_full_axi_mosi.awprot,
        s_axi_awregion  => (others => '0'),
        s_axi_awqos     => (others => '0'),
        s_axi_awvalid   => i_vd_full_axi_mosi.awvalid,
        s_axi_awready   => o_vd_full_axi_miso.awready,        

        s_axi_wdata     => i_vd_full_axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => i_vd_full_axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => i_vd_full_axi_mosi.wlast,
        s_axi_wvalid    => i_vd_full_axi_mosi.wvalid,
        s_axi_wready    => o_vd_full_axi_miso.wready,

        s_axi_bresp     => o_vd_full_axi_miso.bresp,
        s_axi_bvalid    => o_vd_full_axi_miso.bvalid,
        s_axi_bready    => i_vd_full_axi_mosi.bready ,

        s_axi_araddr    => i_vd_full_axi_mosi.araddr(19 downto 0),
        s_axi_arlen     => i_vd_full_axi_mosi.arlen,
        s_axi_arsize    => i_vd_full_axi_mosi.arsize,
        s_axi_arburst   => i_vd_full_axi_mosi.arburst,
        s_axi_arlock    => arlock_slv,
        s_axi_arcache   => i_vd_full_axi_mosi.arcache,
        s_axi_arprot    => i_vd_full_axi_mosi.arprot,
        s_axi_arregion  => "0000",
        s_axi_arqos     => "0000",
        s_axi_arvalid   => i_vd_full_axi_mosi.arvalid,
        s_axi_arready   => o_vd_full_axi_miso.arready,
        
        s_axi_rdata     => o_vd_full_axi_miso.rdata(31 downto 0),
        s_axi_rresp     => o_vd_full_axi_miso.rresp,
        s_axi_rlast     => o_vd_full_axi_miso.rlast,
        s_axi_rvalid    => o_vd_full_axi_miso.rvalid,
        s_axi_rready    => i_vd_full_axi_mosi.rready,
        -- master interface

        m_axi_aclk      => clk_vd,
        m_axi_aresetn   => ARGs_rstn_in_clk_vd_domain,
        m_axi_awaddr    => axi_mosi.awaddr(19 downto 0),
        m_axi_awlen     => axi_mosi.awlen,
        m_axi_awsize    => axi_mosi.awsize,
        m_axi_awburst   => axi_mosi.awburst,
        m_axi_awlock    => axi_mosi_awlock,
        m_axi_awcache   => axi_mosi.awcache,
        m_axi_awprot    => axi_mosi.awprot,
        m_axi_awregion  => axi_mosi.awregion,
        m_axi_awqos     => axi_mosi.awqos,
        m_axi_awvalid   => axi_mosi.awvalid,
        m_axi_awready   => axi_miso.awready,
        m_axi_wdata     => axi_mosi.wdata(31 downto 0),
        m_axi_wstrb     => axi_mosi.wstrb(3 downto 0),
        m_axi_wlast     => axi_mosi.wlast,
        m_axi_wvalid    => axi_mosi.wvalid,
        m_axi_wready    => axi_miso.wready,
        m_axi_bresp     => axi_miso.bresp,
        m_axi_bvalid    => axi_miso.bvalid,
        m_axi_bready    => axi_mosi.bready,
        m_axi_araddr    => axi_mosi.araddr(19 downto 0),
        m_axi_arlen     => axi_mosi.arlen,
        m_axi_arsize    => axi_mosi.arsize,
        m_axi_arburst   => axi_mosi.arburst,
        m_axi_arlock    => axi_mosi_arlock,
        m_axi_arcache   => axi_mosi.arcache,
        m_axi_arprot    => axi_mosi.arprot,
        m_axi_arregion  => axi_mosi.arregion,
        m_axi_arqos     => axi_mosi.arqos,
        m_axi_arvalid   => axi_mosi.arvalid,
        m_axi_arready   => axi_miso.arready,
        m_axi_rdata     => axi_miso.rdata(31 downto 0),
        m_axi_rresp     => axi_miso.rresp,
        m_axi_rlast     => axi_miso.rlast,
        m_axi_rvalid    => axi_miso.rvalid,
        m_axi_rready    => axi_mosi.rready
    );

    axi_mosi.awlock     <= axi_mosi_awlock(0);
    axi_mosi.arlock     <= axi_mosi_arlock(0);


    datagen_memspace : axi_bram_ctrl_vd_datagen
    PORT MAP (
        s_axi_aclk      => clk_vd,
        s_axi_aresetn   => ARGs_rstn_in_clk_vd_domain, -- in std_logic;
        s_axi_awaddr    => axi_mosi.awaddr(19 downto 0),
        s_axi_awlen     => axi_mosi.awlen,
        s_axi_awsize    => axi_mosi.awsize,
        s_axi_awburst   => axi_mosi.awburst,
        s_axi_awlock    => axi_mosi.awlock ,
        s_axi_awcache   => axi_mosi.awcache,
        s_axi_awprot    => axi_mosi.awprot,
        s_axi_awvalid   => axi_mosi.awvalid,
        s_axi_awready   => axi_miso.awready,
        s_axi_wdata     => axi_mosi.wdata(31 downto 0),
        s_axi_wstrb     => axi_mosi.wstrb(3 downto 0),
        s_axi_wlast     => axi_mosi.wlast,
        s_axi_wvalid    => axi_mosi.wvalid,
        s_axi_wready    => axi_miso.wready,
        s_axi_bresp     => axi_miso.bresp,
        s_axi_bvalid    => axi_miso.bvalid,
        s_axi_bready    => axi_mosi.bready ,
        s_axi_araddr    => axi_mosi.araddr(19 downto 0),
        s_axi_arlen     => axi_mosi.arlen,
        s_axi_arsize    => axi_mosi.arsize,
        s_axi_arburst   => axi_mosi.arburst,
        s_axi_arlock    => axi_mosi.arlock ,
        s_axi_arcache   => axi_mosi.arcache,
        s_axi_arprot    => axi_mosi.arprot,
        s_axi_arvalid   => axi_mosi.arvalid,
        s_axi_arready   => axi_miso.arready,
        s_axi_rdata     => axi_miso.rdata(31 downto 0),
        s_axi_rresp     => axi_miso.rresp,
        s_axi_rlast     => axi_miso.rlast,
        s_axi_rvalid    => axi_miso.rvalid,
        s_axi_rready    => axi_mosi.rready,
    
        bram_rst_a      => open,
        bram_clk_a      => open,
        bram_en_a       => bram_en,      -- out std_logic;
        bram_we_a       => bram_we_byte, -- out (3:0);
        bram_addr_a     => o_bram_addr,    -- out (19:0);
        bram_wrdata_a   => o_bram_wrdata,  -- out (31:0);
        bram_rddata_a   => i_bram_rddata   -- in (31:0);
    );

    -- transactions are always 32 bits wide, so only need to check 1 bit of the byte enable.
    o_bram_wrEn <= bram_en and bram_we_byte(0);

end Behavioral;

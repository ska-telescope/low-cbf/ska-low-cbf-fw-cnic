----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 06/04/2023 11:20:16 PM
-- Module Name: dataGen_dataSource - Behavioral
-- Description: 
--  Generates data, delays it and filters it
--  Data generation uses the xorshift64 algorithm :
--    uint64_t xorshift64(struct xorshift64_state *state){
--       uint64_t x = state->a;
--       x ^= x << 13;
--       x ^= x >> 7;
--       x ^= x << 17;
--	     return state->a = x;
--    }
-- 
--  8 groups of 8 bits are summed from the 64 bit state, to generate a 12 bit 
--  random value with roughly Gaussian statistics. 
--
-- Data Flow :
--  (1) i_load captures all the input data
--  (2) Put phase_correction into the sin_cos lookup to get the complex 18+18 bit 
--      phase correction
--  (3) generate preload samples
--      Number of preload samples to generate :
--       33 + i_required_coarse_delay - i_current_coarse_delay
--      * At the start of signal generation, i.e. when i_first_frame = '1',
--        the required_coarse_delay could be any value up to 2047, 
--        while the current_coarse_delay will be zero. 
--        - Data generation must pause after the preload phase,
--          to wait for other data sources to finish their preload phase. 
--      * Thereafter, i.e. when i_first_frame = '0', differences between 
--        i_required_coarse_delay and i_current_coarse_delay should be 0, 1, or -1
--        (but this module does not assume, it just sets o_ready and 
--         waits for i_ready before generating data samples)
--        
--  (4) Generate data samples : 
--      Then 16 samples of data are generated.
--      one cycle of o_data_valid = '0'
--      then another 16 samples of data are generated.
--
--  Number of clocks between i_load going high : 
--   see below :
--    Minimum = 1 + 1 + 32 + 1 + 16+1+16 + 1 = 69 clock cycles.
--    Typical = 1 + 1 + 33 + 1 + 16+1+16 + 1 = 70 clock cycles.
--    Maximum normal operation = 1 + 1 + 34 + 1 + 16 + 1 + 16+ 1 = 70 clock cycles.
--    Maximum at startup = 1 + 1 + 2047 + 1 + 16 + 1 + 16 + 1 = 2084 clock cycles.
--
--  Note: Each time i_load runs it generates 32 samples
--        This module has to generate 128 different streams
--        Real time available = (32 samples) * (1080ns sample period) = 34560 ns
--        Processing time required = (70 clocks) * (128 streams) * (3.333 ns/clk) = 29866 ns
--
--                       ctrl_fsm:
--    i_load = '1'         idle
--                         get_phase
--                         run_preload   \      Minimum 32 clock cycles
--                         ...            |---  Up to 18 in normal operation
--                         run_preload   /      Up to 2047 at startup
--                         wait_ready      --- Typically 1 clock in this state
--                         run_data1      \
--                         ...            |--- Always 16 clocks cycles
--                         run_data1      /
--                         run_gap      -- 1 clock
--                         run_data2   \
--                         ...          |-- 16 clocks
--                         run_data2   /
--                         idle
--
-- 
--  -----------------------------------------------------------------------------
--  For i_required_coarse_delay = i_current_coarse_delay:
--
--                                                                   idle clocks (waiting for i_ready)                     idle
--                                                                                 |                                        |
--                                 o_preload_valid (33 samples)                    |       o_data_valid (16 samples)        |     o_data_valid (16 samples)
--                     /--------------------------------------------------------\/---\/----------------------------------\/---\/---------------------------\
-- samples generated:  1  2  3  4  5  6  7  8  9  10  11  12  13  14  ..  32  33   -  34  35  36  37  ...              49       50   ...                 65
--                                                                            |
--                                                                    state that generated 
--                                                                    sample 33 is saved
--
--  
--  -----------------------------------------------------------------------------
--  For i_required_coarse_delay = i_current_coarse_delay + 1:
--
--                                                                      idle clocks (waiting for i_ready)
--                                                                                     |                                      idle
--                                 o_preload_valid (18 samples)                        |       o_data_valid (16 samples)        |     o_data_valid (16 samples)
--                     /------------------------------------------------------------\/---\/----------------------------------\/---\/----------------------------\
-- samples generated:  1  2  3  4  5  6  7  8  9  10  11  12  13  14 ...  32  33  34   -   35  36 ...                      50       51 52          ...        66
--                                                                                |
--                                                                       state that generated 
--                                                                        sample 18 is saved
--
--  -----------------------------------------------------------------------------
--  For i_required_coarse_delay = i_current_coarse_delay - 1:
--
--                                                                  idle clocks (waiting for i_ready)               idle
--                                 o_preload_valid (18 samples)                  |       o_data_valid (16 samples)    |    o_data_valid (16 samples)
--                     /----------------------------------------------------\/-------\/----------------------------\/---\/----------------------------\
-- samples generated:  1  2  3  4  5  6  7  8  9  10  11  12  ... 30  31  32   -  -   33  34 35 ...              48       49 50    ...              64
--                                                                        |
--                                                               state that generated 
--                                                                sample 16 is saved
--  
--  ------------------------------------------------------------------------------
--  On Startup : e.g. i_required_coarse_delay = 30, i_current_coarse_delay = 0
-- samples_generated: 65 + 30 - 0 = 95
-- state that generated sample 95-32 = 63 is saved 
--
----------------------------------------------------------------------------------
library IEEE, vd_datagen_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library common_lib;
USE common_lib.common_pkg.ALL;

entity dataGen_dataSource is
    Port(
        clk : in std_logic;
        ------------------------------------------------------------------
        -- Input state :
        --  - 1 bit - Load
        --  - 64-bit PN state (real part)
        --  - 64-bit PN state (imaginary part)
        --     * On startup, put the seed into the states.
        --     * If using a sinusoid, bits 15:0 of real hold the current sinusoid phase
        --  - 15 bit phase step
        --  - 1 bit use sinusoid
        --  - 16 bit phase correction
        --  - 16 bit scale
        --  - 12 bit required sample offset
        --     * Number of samples to offset by to implement the coarse delay
        --  - 12 bit current sample offset
        --
        i_load         : in std_logic;
        i_PN_re_state  : in std_logic_vector(63 downto 0);
        i_PN_im_state  : in std_logic_vector(63 downto 0);
        i_phase_step   : in std_logic_vector(14 downto 0);
        i_use_sinusoid : in std_logic;
        i_phase_correction : in std_logic_vector(23 downto 0);
        i_scale        : in std_logic_vector(15 downto 0);
        i_src_count    : in std_logic_vector(7 downto 0);
        i_required_coarse_delay : in std_logic_vector(11 downto 0);
        i_current_coarse_delay : in std_logic_vector(11 downto 0);
        o_idle : out std_logic;
        -- signal to indicate to that the other data source shouldn't be started,
        -- because this datasource has a while to go on generating preload samples.
        o_lots_of_preload_to_go : out std_logic;
        -------------------------------------------------------------------
        -- Output state for storage :
        --  64 bit PN state (real part)
        --     * Low 16 bits will contain the sinusoid phase if i_use_sinusoid was set
        --  64 bit PN state (imaginary part)
        --  12 bit current sample offset
        o_PN_re_state : out std_logic_vector(63 downto 0);
        o_PN_im_state : out std_logic_vector(63 downto 0);
        o_coarse_delay : out std_logic_vector(11 downto 0);
        o_state_valid : out std_logic;  -- o_state_valid pulses high, but state info is valid for at least 4 clocks.
        -------------------------------------------------------------------
        -- data output        
        o_data_re       : out std_logic_vector(15 downto 0);
        o_data_im       : out std_logic_vector(15 downto 0);
        o_preload_valid : out std_logic;
        o_data_valid    : out std_logic;
        o_data_block    : out std_logic;
        -- scale and phase associated with the data output
        o_phase_correction  : out std_logic_vector(35 downto 0);
        o_scale             : out std_logic_vector(15 downto 0);
        o_src_count         : out std_logic_vector(7 downto 0);
        o_scale_phase_valid : out std_logic;
        -- o_ready goes high when this module is ready to drive o_data_valid high.
        -- (actually several clocks prior, due to pipeline stages)
        o_ready : out std_logic;
        -- internal fsm only moves on to generating output data (o_data_valid = '1')
        -- when i_ready is high.
        i_ready : in std_logic
    );
    -- prevent optimisation across module boundaries.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of dataGen_dataSource : entity is "yes";
end dataGen_dataSource;

architecture Behavioral of dataGen_dataSource is

    -- 
    component sincos_lut
    port (
        aclk : in std_logic;
        s_axis_phase_tvalid : in std_logic;
        -- 24 bit phase
        s_axis_phase_tdata  : in std_logic_vector(23 downto 0);
        m_axis_data_tvalid  : out std_logic;
        -- cosine in bits 17:0, sine in bits 41:24
        -- 2.16 format, so full scale is +/-65536
        m_axis_data_tdata   : out std_logic_vector(47 downto 0));
    end component;
    
    type t_ctrl_fsm is (idle, get_phase, run_preload, run_preload_last_sample, wait_ready, run_data1, run_data_pause, run_data2);
    signal ctrl_fsm, ctrl_fsm_del1, ctrl_fsm_del2, ctrl_fsm_del3, ctrl_fsm_del4, ctrl_fsm_del5, ctrl_fsm_del6, ctrl_fsm_del7, ctrl_fsm_del8 : t_ctrl_fsm := idle;

    attribute fsm_encoding : string;
    attribute fsm_encoding of ctrl_fsm : signal is "gray";
    attribute fsm_encoding of ctrl_fsm_del1 : signal is "gray";
    attribute fsm_encoding of ctrl_fsm_del2 : signal is "gray";
    attribute fsm_encoding of ctrl_fsm_del3 : signal is "gray";
    attribute fsm_encoding of ctrl_fsm_del4 : signal is "gray";
    attribute fsm_encoding of ctrl_fsm_del5 : signal is "gray";
    attribute fsm_encoding of ctrl_fsm_del6 : signal is "gray";
    attribute fsm_encoding of ctrl_fsm_del7 : signal is "gray";
    attribute fsm_encoding of ctrl_fsm_del8 : signal is "gray";

    signal preload_samples_remaining : std_logic_vector(11 downto 0);
    signal data_samples_remaining : std_logic_vector(5 downto 0);
    signal use_sinusoid_del : std_logic_vector(7 downto 0);
    signal PN_init_state, PN_cur_state : t_slv_64_arr(1 downto 0);
    signal cur_phase, phase_step, phase_hold : std_logic_vector(15 downto 0);
    signal phase_correction : std_logic_vector(23 downto 0);
    signal scale : std_logic_vector(15 downto 0);
    signal PN_load, PN_run : std_logic;
    signal sincos_re, sincos_im : std_logic_vector(17 downto 0);
    signal PN_dout : t_slv_12_arr(1 downto 0);
    signal PN_valid : std_logic_vector(1 downto 0);
    signal sincos : std_logic_vector(47 downto 0);
    signal phase_in : std_logic_vector(23 downto 0);
    signal src_count : std_logic_vector(7 downto 0);

    signal preload_valid    : std_logic;
    signal data_valid    : std_logic;

   attribute max_fanout : integer;
   attribute max_fanout of preload_valid : signal is 256;
   attribute max_fanout of data_valid : signal is 256;

begin
    
    o_idle <= '1' when ctrl_fsm = idle else '0';

    o_preload_valid <= preload_valid;
    o_data_valid    <= data_valid;
    
    process(clk)
    begin
        if rising_edge(clk) then
        
            if ((ctrl_fsm = run_preload and (unsigned(preload_samples_remaining) < 4)) or
                (ctrl_fsm = run_preload_last_sample) or
                (ctrl_fsm = wait_ready)) then
                -- drive ready high a few clocks before we get to the wait_ready state,
                -- because of pipeline delays combining all the ready signal from all the 
                -- different dataSource modules, so in most cases we will only be in
                -- the wait_ready state for 1 clock cycle.
                o_ready <= '1';
            else
                o_ready <= '0';
            end if;
        
            if i_load = '1' then
                use_sinusoid_del(0) <= i_use_sinusoid;
                PN_init_state(0) <= i_PN_re_state; -- in (63:0);
                PN_init_state(1) <= i_PN_im_state; -- in (63:0);
                cur_phase <= i_PN_re_state(15 downto 0);
                -- i_phase_step * 2, so it has a range -32768 to 32768, to match 16-bit input to the sincos lookup
                phase_step <= i_phase_step & '0';       -- in (14:0);
                phase_correction <= i_phase_correction; -- in (23:0);
                scale <= i_scale;                       -- in (15:0);
                src_count <= i_src_count;
                preload_samples_remaining <= std_logic_vector(33 + unsigned(i_required_coarse_delay) - unsigned(i_current_coarse_delay));
                -- current coarse delay always matches the required delay after running the preload phase.
                o_coarse_delay <= i_required_coarse_delay;
                ctrl_fsm <= get_phase;
                data_samples_remaining <= "100000"; -- Generate 32 data samples
            else
                case ctrl_fsm is
                    when idle =>
                        -- wait for i_load = '1'
                        ctrl_fsm <= idle;
                        
                    when get_phase =>
                        -- convert phase_correction into complex value using sin cos lookup
                        ctrl_fsm <= run_preload;
                        
                    when run_preload =>
                        cur_phase <= std_logic_vector(unsigned(cur_phase) + unsigned(phase_step));
                        preload_samples_remaining <= std_logic_vector(unsigned(preload_samples_remaining) - 1);
                        if (unsigned(preload_samples_remaining) = 2) then
                            -- Generating the second last preload sample now.
                            ctrl_fsm <= run_preload_last_sample;
                        end if;
                    
                    when run_preload_last_sample =>
                        cur_phase <= std_logic_vector(unsigned(cur_phase) + unsigned(phase_step));
                        phase_hold <= cur_phase;
                        preload_samples_remaining <= std_logic_vector(unsigned(preload_samples_remaining) - 1);
                        ctrl_fsm <= wait_ready;
                    
                    when wait_ready =>
                        if i_ready = '1' then
                            ctrl_fsm <= run_data1;
                        end if;
                    
                    when run_data1 =>
                        cur_phase <= std_logic_vector(unsigned(cur_phase) + unsigned(phase_step));
                        data_samples_remaining <= std_logic_vector(unsigned(data_samples_remaining) - 1);
                        if (unsigned(data_samples_remaining) = 17) then
                            ctrl_fsm <= run_data_pause;
                        end if;
                    
                    when run_data_pause =>
                        ctrl_fsm <= run_data2;
                    
                    when run_data2 =>
                        cur_phase <= std_logic_vector(unsigned(cur_phase) + unsigned(phase_step));
                        data_samples_remaining <= std_logic_vector(unsigned(data_samples_remaining) - 1);
                        if (unsigned(data_samples_remaining) = 1) then
                            ctrl_fsm <= idle;
                        end if;
                    
                    when others =>
                        ctrl_fsm <= idle;
                    
                end case;
            end if;
            
            if (ctrl_fsm = run_preload and (unsigned(preload_samples_remaining) > 3)) then
                o_lots_of_preload_to_go <= '1';
            else
                o_lots_of_preload_to_go <= '0';
            end if;
            
            ctrl_fsm_del1 <= ctrl_fsm;
            ctrl_fsm_del2 <= ctrl_fsm_del1;
            ctrl_fsm_del3 <= ctrl_fsm_del2;
            ctrl_fsm_del4 <= ctrl_fsm_del3;
            ctrl_fsm_del5 <= ctrl_fsm_del4;
            ctrl_fsm_del6 <= ctrl_fsm_del5;
            ctrl_fsm_del7 <= ctrl_fsm_del6;
            ctrl_fsm_del8 <= ctrl_fsm_del7;
            
            use_sinusoid_del(7 downto 1) <= use_sinusoid_del(6 downto 0);
            
            -- control signals for the PN code are delayed by 7-2 = 5 clocks
            -- because PN generator has 2 clock latency but sincos_lut has 7 clock latency
            if (ctrl_fsm_del5 = get_phase) then
                PN_load <= '1';
            else
                PN_load <= '0';
            end if;
            
            if ((ctrl_fsm_del5 = run_preload) or (ctrl_fsm_del5 = run_preload_last_sample) or (ctrl_fsm_del5 = run_data1) or (ctrl_fsm_del5 = run_data2)) then
                PN_run <= '1';
            else
                PN_run <= '0';
            end if;
            
            -- State information to be stored for the next time
            -- this stream is processed.
            if ctrl_fsm_del6 = run_preload_last_sample then
                if use_sinusoid_del(6) = '1' then
                    o_PN_im_state <= (others => '0'); -- out (63:0);
                    o_PN_re_state(63 downto 16) <= (others => '0');
                    o_PN_re_state(15 downto 0) <= phase_hold;
                else
                    o_PN_re_state <= PN_cur_state(0); -- out (63:0);
                    o_PN_im_state <= PN_cur_state(1); -- out (63:0);
                end if;
                o_state_valid <= '1';
            else
                o_state_valid <= '0';
            end if;
            
            if ctrl_fsm = get_phase then
                phase_in <= phase_correction;
            else
                phase_in <= cur_phase & "00000000";
            end if;
            
            if use_sinusoid_del(7) = '1' then
                o_data_re <= sincos_re(17 downto 2);
                o_data_im <= sincos_im(17 downto 2);
            else
                o_data_re <= PN_dout(0)(11) & PN_dout(0)(11) & PN_dout(0)(11) & PN_dout(0)(11) & PN_dout(0);
                o_data_im <= PN_dout(1)(11) & PN_dout(1)(11) & PN_dout(1)(11) & PN_dout(1)(11) & PN_dout(1);
            end if;
            
            if ((ctrl_fsm_del8 = run_preload) or (ctrl_fsm_del8 = run_preload_last_sample)) then
                preload_valid <= '1';
            else
                preload_valid <= '0';
            end if;
            
            if (ctrl_fsm_del8 = run_data1) or (ctrl_fsm_del8 = run_data2) then
                data_valid <= '1';
            else
                data_valid <= '0';
            end if;
            
            if (ctrl_fsm_del8 = run_data2) then
                o_data_block <= '1';
            else
                o_data_block <= '0';
            end if;
            
            -- del1 => phase_in is valid, 7 clock latency, so del8 =>  sincos_re is valid.
            if (ctrl_fsm_del8 = get_phase) then
                o_phase_correction <= sincos_im & sincos_re;
                o_scale <= scale;
                o_src_count <= src_count;
                o_scale_phase_valid <= '1';
            else
                o_scale_phase_valid <= '0';
            end if;
            
        end if;
    end process;
 
    PNgeni : for i in 0 to 1 generate
        PN64i : entity vd_dataGen_lib.dataGen_PN64
        Port map(
            clk => clk, --  in std_logic;
            ------------------------------------------------------------------
            -- i_load should occur prior to i_run
            i_load     => PN_load,          -- in std_logic;
            i_PN_state => PN_init_state(i), -- in std_logic_vector(63 downto 0);
            i_run => PN_run, -- in std_logic;
            -- Valid data on o_dout, o_valid 2 clocks after i_run
            o_dout => PN_dout(i),      -- out std_logic_vector(11 downto 0);
            o_PN_state => PN_cur_state(i), -- out std_logic_vector(63 downto 0);
            o_valid  => PN_valid(i)    -- out std_logic
        );        
    end generate;
    
    -- 7 clock latency.
    sincosi : sincos_lut
    port map (
        aclk => clk,
        s_axis_phase_tvalid => '1',
        -- 16 bit phase
        s_axis_phase_tdata => phase_in,
        m_axis_data_tvalid => open,
        -- cosine in bits 17:0, sine in bits 41:24
        m_axis_data_tdata => sincos -- out (47:0)
    );
    sincos_re <= sincos(17 downto 0);
    sincos_im <= sincos(41 downto 24);
    
    
end Behavioral;
----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 06/04/2023 11:20:16 PM
-- Module Name: dataGen_singleSource - Behavioral
-- Description: 
--   Generates data for 256 different sky sources, in bursts of 32 samples per source.
--   Each burst of 32 is broken into two bursts of 16, with one clock cycle idle in between.
--
--             |--------------------------------------------------------------------------|
--             | **dataGen_singleSource**                                                 |
--             |                                                                          |
-- delay  -->--+- double buffer ---> data_source0 ---> interp filter --> phase --> scale -+--> 40+40 bit complex samples out
-- and source  |                 |   data_source1 -/       |                              |
-- parameters  |                 |      |                  |                              |
--             |                 |   state memory          |                              |
--             |-----------------+-------------------------+------------------------------|
--                               |                         | (256 bit wide, 16 taps)
--                               |-->- interpolation -->---| 
--                                    filter taps ROM
--
-- Double Buffer
--   Holds data for (2 [double buffer])x(256) = 512 sets of delay parameters, 
--   which come from the evaluation of 256 polynomials.
--   This module is connected directly to the polynomial evaluation, which 
--   generates 512 sets of parameters, indexed by "i_count".
--   Half of these (=256) are captured and processed in this module, as determined by
--   the generic g_ODD_DELAYS.
--
-- State memory
--   Holds the phase for sinusoid generation, or 2x64-bit state (re + im) for the random number generator.
--   i.e. 512 x 64 bit values
--
-- Amplitudes : 
--   For Sinusoids :
--    Sinusoid output is 2.14 format, so as 16 bit values it runs from -16384 to 16384
--    standard deviation is 16384/sqrt(2) = 11585
--
--   For Pseudo-random noise :
--    Output is the sum of 8 x 8bit values from the 64-bit PN generator.
--    To avoid bias, the sum is done as x0 - x1 + x2 - x3 + x4 - x5 + x6 - x7
--    So the maximum possible value is 4*127 + 4*128 = 1020, and minimum possible is -1020
--    Standard deviation is about 209
--    Check in Matlab with 100 million random values : 
--    >> t1 = floor(rand(100000000,8) * 256) - 128;
--    >> t2 = t1(:,1) - t1(:,2) + t1(:,3) - t1(:,4) + t1(:,5) - t1(:,6) + t1(:,7) - t1(:,8);
--    >> std(t2) (ans = 2.090308578851794e+02)
--    >> max(t2) (ans = 939)
--    >> min(t2) (ans = -931)
--
----------------------------------------------------------------------------------
library IEEE, xpm, vd_datagen_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use xpm.vcomponents.all;
library common_lib;
USE common_lib.common_pkg.ALL;

entity dataGen_singleSource is
    generic(
        -- Either process even-indexed delays or odd-indexed delays
        -- as determined by i_count(0)
        g_ODD_DELAYS : integer range 0 to 1   -- 0 for even indexed delays, 1 for odd indexed delays
    );
    port(
        clk : in std_logic;
        i_rst : in std_logic;
        --------------------------------------------------
        -- Delay data
        i_sample_offset : in std_logic_vector(10 downto 0);
        i_interp_filter : in std_logic_vector(10 downto 0); -- interpolation filter selection
        i_phase_offset  : in std_logic_vector(23 downto 0); -- phase correction
        i_config  : in std_logic_vector(31 downto 0); -- config info, 14:0 = seed or sinusoid phase step, 15 = select sinusoid, 31:16 = scale
        i_startup : in std_logic;
        i_count   : in std_logic_vector(8 downto 0);  -- which polynomial this is for
        -- Count of the number of 16-sample frames this data is for; 
        -- lsb determines which half of the double buffer to write this burst of delay data to
        i_frame   : in std_logic_vector(31 downto 0);
        i_valid   : in std_logic;
        i_last    : in std_logic; -- Data from the last polynomial
        --------------------------------------------------
        -- Request a set of interpolation taps
        -- Pulse high on filterValid, after 4 to 7 clocks i_filterValid will respond
        -- with the requested filter taps.
        o_filter : out std_logic_vector(10 downto 0);
        o_filterDest : out std_logic; -- 1 bit of meta data to indicate which datasource pipeline this filter is for.
        o_filterValid : out std_logic;
        -- 32 x 16 bit filter taps
        i_filterTaps  : in t_slv_16_arr(31 downto 0);
        i_filterDest  : in std_logic;
        i_filterValid : in std_logic;
        --------------------------------------------------
        -- Synchronisation with other data sources.
        -- Trigger reading of the delay buffer
        i_read_delays : in std_logic;
        i_read_buffer : in std_logic; -- which of the 2 buffers to read from
        o_busy : out std_logic;
        
        -- When data source has generated preload samples and is ready to start generating
        -- data samples, o_ready goes high, and we wait for i_ready to go high
        -- before proceeding to generate data samples.
        o_ready : out std_logic_vector(1 downto 0);
        i_ready : in std_logic_vector(1 downto 0);
        --------------------------------------------------
        -- data output
        -- 40+40 bit complex
        o_data_re   : out std_logic_vector(39 downto 0);
        o_data_im   : out std_logic_vector(39 downto 0);
        o_src_count : out std_logic_vector(7 downto 0); -- which polynomial this data came from.
        o_block     : out std_logic;
        o_valid     : out std_logic  -- data valid when both o_valid and i_rdy are high
    );
    -- prevent optimisation across module boundaries.
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of dataGen_singleSource : entity is "yes";
    
end dataGen_singleSource;

architecture Behavioral of dataGen_singleSource is

    -- complex sinusoid lookup table
    -- 14 bit input for the phase
    -- Output (31:16) = sin, (15:0) = cosine
    -- 6 cycle latency from s_axis_phase_tdata to m_axi_data_tdata
    component sinCos_lut
    port (
        aclk : IN STD_LOGIC;
        s_axis_phase_tvalid : IN STD_LOGIC;
        s_axis_phase_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        m_axis_data_tvalid : OUT STD_LOGIC;
        m_axis_data_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
    end component;
    
    signal delays_wrdata : std_logic_vector(79 downto 0);
    signal delays_wrAddr : std_logic_vector(8 downto 0);
    signal delays_wrEn_slv : std_logic_vector(0 downto 0);
    signal buf0_used, buf1_used : std_logic := '0';
    signal buf0_frame, buf1_frame : std_logic_vector(31 downto 0);
    
    signal src_re, src_im : t_slv_16_arr(1 downto 0);
    signal src_preload : std_logic_vector(1 downto 0);
    signal src_valid : std_logic_vector(1 downto 0);
    signal src_phase : t_slv_36_arr(1 downto 0);
    signal src_scale : t_slv_16_arr(1 downto 0);
    signal src_count : t_slv_8_arr(1 downto 0);
    signal src_scale_phase_valid : std_logic_vector(1 downto 0);
                
    type t_rd_fsm is (idle, wait_delays1, wait_delays2, wait_delays3, wait_delays4, wait_ready_dataSource0, wait_ready_dataSource1, update_addr);
    signal rd_fsm, rd_fsm_del1, rd_fsm_del2 : t_rd_fsm;
    signal PN_re_state, PN_im_state : std_logic_Vector(63 downto 0);
    signal rd_delays_phase_step : std_logic_vector(14 downto 0);
    signal rd_delays_use_sinusoid : std_logic;
    signal rd_delays_phase_correction : std_logic_vector(23 downto 0);
    signal rd_Delays_scale : std_logic_vector(15 downto 0);
    signal rd_delays_required_coarse_delay, rd_delays_current_coarse_delay : std_logic_vector(11 downto 0);
    signal read_delays_del, read_buffer_del : std_logic;
    signal delays_rdAddr, state_rdAddr : std_logic_vector(8 downto 0);
    signal delays_rdData : std_logic_vector(79 downto 0);
    signal state_rdData : std_logic_vector(71 downto 0);
    signal dataSource_idle, load_delays : std_logic_vector(1 downto 0);
    signal first_run, startup_del : std_logic;
    
    signal save_PN_re_state, save_PN_im_state : t_slv_64_arr(1 downto 0);
    signal save_PN_coarse_delay : t_slv_12_arr(1 downto 0);
    signal save_state_valid : std_logic_vector(1 downto 0);
    type t_wr_state_fsm is (idle, write_state0_addr0, write_state0_addr1, write_state1_addr0, write_state1_addr1);
    signal wr_state_fsm : t_wr_state_fsm := idle;
    signal recent_delay_addr, save_addr : t_slv_8_arr(1 downto 0);
    signal state_wrData : std_logic_vector(71 downto 0);
    signal state_wrEn_slv : std_logic_vector(0 downto 0);
    signal state_wrAddr : std_logic_vector(8 downto 0);
    signal save_valid : std_logic_vector(1 downto 0);
    signal save_PN_re, save_PN_im : t_slv_64_arr(1 downto 0);
    signal save_coarse_delay : t_slv_12_arr(1 downto 0);
    signal write_state : std_logic_vector(1 downto 0);
    signal rd_delays_interp_filter_del1, rd_delays_interp_filter : std_logic_vector(10 downto 0);    
    signal fir_taps : t_slv_16_Arr(31 downto 0);
    signal fir_valid : std_logic_vector(1 downto 0);
    signal lots_of_preload_to_go : std_logic_vector(1 downto 0);
    signal src_block : std_logic_vector(1 downto 0);
    
begin

    ---------------------------------------------------------------------
    -- Double buffer for delay information
    --  Holds : 
    --    * bits 31:0  = config 
    --                    14:0 = seed or sinusoid phase step
    --                    15   = select sinusoid
    --                    31:16 = scale
    --    * bits 42:32  = sample_offset (11 bits)
    --    * bits 53:43 = interp_filter (11 bits)
    --    * bits 69:54 = phase_offset (16 bits)
    --
    process(clk)
    begin
        if rising_edge(clk) then
            delays_wrdata(31 downto 0) <= i_config;
            delays_wrdata(42 downto 32) <= i_sample_offset;
            delays_wrdata(53 downto 43) <= i_interp_filter;
            delays_wrdata(77 downto 54) <= i_phase_offset;
            delays_wrdata(78) <= i_startup;
            delays_wrdata(79) <= '0';
            --delays_wrdata(69 downto 54) <= i_phase_offset;
            --delays_wrdata(70) <= i_startup;
            --delays_wrdata(71) <= '0';
            delays_wrAddr(8 downto 0) <= i_frame(0) & i_count(8 downto 1);
            if (g_ODD_DELAYS = 0) then
                -- This module is processing even-indexed delays
                if i_valid = '1' and i_count(0) = '0' then
                    delays_wren_slv(0) <= '1';
                else
                    delays_wren_slv(0) <= '0';
                end if;
            else
                -- This module is processing odd-indexed delays
                if i_valid = '1' and i_count(0) = '1' then
                    delays_wren_slv(0) <= '1';
                else
                    delays_wren_slv(0) <= '0';
                end if;
            end if;

            if (rd_fsm = idle) then
                o_busy <= '0';
            else
                o_busy <= '1';
            end if;
            
            read_delays_del <= i_read_delays;
            read_buffer_del <= i_read_buffer;
            startup_del <= i_startup;
            
            load_delays <= "00";
            if i_rst = '1' then
                rd_fsm <= idle;
            else
                case rd_fsm is
                    when idle =>
                        if read_delays_del = '1' then
                            rd_fsm <= wait_delays1;
                            -- Get delay and state information
                            delays_rdAddr <= read_buffer_del & "00000000";
                            state_rdAddr <= "000000000";
                        end if;
                        
                    when wait_delays1 =>
                        state_rdAddr <= std_logic_vector(unsigned(state_rdAddr) + 1);
                        rd_fsm <= wait_delays2;
                        
                    when wait_delays2 =>
                        rd_fsm <= wait_delays3;
                        
                    when wait_delays3 =>
                        -- delays data is valid, and the first word of state data is valid.
                        rd_delays_phase_step <= delays_rdData(14 downto 0);
                        rd_delays_use_sinusoid <= delays_rdData(15);
                        rd_delays_scale <= delays_rdData(31 downto 16);
                        rd_delays_required_coarse_delay <= '0' & delays_rdData(42 downto 32);
                        rd_delays_interp_filter <= delays_rdData(53 downto 43);
                        
                        rd_delays_phase_correction <= delays_rdData(77 downto 54);
                        first_run <= delays_rdData(78);
                        
                        --rd_delays_phase_correction <= delays_rdData(69 downto 54);
                        --first_run <= delays_rdData(70);
                        if delays_rdData(78) = '1' then
                            rd_delays_current_coarse_delay <= (others => '0');
                            if delays_rdData(15) = '1' then
                                -- use sinusoid, Initialise phase to 0.
                                PN_re_state(63 downto 0) <= (others => '0');
                            else
                                -- Initialise the random number generator with the provided seed.
                                PN_re_state(15 downto 0) <= '0' & delays_rdData(14 downto 0);
                                PN_re_state(47 downto 16) <= x"00ffff00";
                                PN_re_state(63 downto 48) <= '0' & delays_rdData(14 downto 0);
                                PN_im_state(15 downto 0) <= '0' & delays_rdData(14 downto 0);
                                PN_im_state(47 downto 16) <= x"ff0000ff";
                                PN_im_state(63 downto 48) <= '0' & delays_rdData(14 downto 0);
                            end if;
                        else
                            PN_re_state <= state_rdData(63 downto 0);
                            rd_delays_current_coarse_delay(7 downto 0) <= state_rdData(71 downto 64);
                        end if;
                        rd_fsm <= wait_delays4;
                        
                    when wait_delays4 =>
                        if first_run = '0' then
                            -- If first run, then PN_im_state was initialised in the previous state (wait_delays3)
                            PN_im_state <= state_rdData(63 downto 0);
                            rd_delays_current_coarse_delay(11 downto 8) <= state_rdData(67 downto 64);
                        end if;
                        if delays_rdAddr(0) = '0' then
                            rd_fsm <= wait_ready_dataSource0;
                        else
                            rd_fsm <= wait_ready_dataSource1;
                        end if;
                        
                    when wait_ready_dataSource0 =>
                        -- fetched data for the first instance of dataGen_dataSource,
                        -- wait until it is idle and then send it.
                        if dataSource_idle(0) = '1' and lots_of_preload_to_go(1) = '0' then
                            load_delays(0) <= '1';
                            rd_fsm <= update_addr;
                        end if;
                        
                    when wait_ready_dataSource1 =>
                        if dataSource_idle(1) = '1' and lots_of_preload_to_go(0) = '0' then
                            load_delays(1) <= '1';
                            rd_fsm <= update_addr;
                        end if;
                        
                    when update_addr =>
                        delays_rdAddr <= std_logic_vector(unsigned(delays_rdAddr) + 1);
                        -- 2 addresses per state, but state_rdAddr is also updated in the wait_delays1 state.
                        state_rdAddr <= std_logic_vector(unsigned(state_rdAddr) + 1);
                        if delays_rdAddr(7 downto 0) = "11111111" then
                            rd_fsm <= idle;
                        else
                            rd_fsm <= wait_delays1;
                        end if;
                    
                    when others =>
                        rd_fsm <= idle;
                    
                end case;
            end if;
            
            rd_fsm_del1 <= rd_fsm;
            rd_fsm_del2 <= rd_fsm_del1;
            rd_delays_interp_filter_del1 <= rd_delays_interp_filter;
            
            -- Get the filter coefficients
            if rd_fsm_del2 = wait_ready_dataSource0 and rd_fsm_del1 = update_addr then
                o_filter <= rd_delays_interp_filter_del1;
                o_filterDest <= '0';
                o_filterValid <= '1';
            elsif rd_fsm_del2 = wait_ready_dataSource1 and rd_fsm_Del1 = update_addr then
                o_filter <= rd_delays_interp_filter_del1;
                o_filterDest <= '1';
                o_filterValid <= '1';
            else
                o_filterValid <= '0';
            end if;
            
            fir_taps <= i_filterTaps;
            if i_filterValid = '1' and i_filterDest = '0' then
                fir_valid(0) <= '1';
            else
                fir_valid(0) <= '0';
            end if;
            if i_filterValid = '1' and i_filterDest = '1' then
                fir_valid(1) <= '1';
            else
                fir_valid(1) <= '0';
            end if;
            
            -- Save state back to the state memory.
            for i in 0 to 1 loop
                if load_delays(i) = '1' then
                    -- Capture the address to write the state information to 
                    -- for each of the dataSource modules.
                    -- This happens when we load new information into the datasource module
                    recent_delay_addr(i) <= delays_rdAddr(7 downto 0);
                end if;
                if save_state_valid(i) = '1' then
                    -- some time after new info has been loaded in the dataSource module,
                    -- it will generate the save_state_valid(i) signal, but before it finishes
                    -- processing that stream, so recent_delay_addr(i) will still be valid.
                    save_PN_re(i) <= save_PN_re_state(i);
                    save_PN_im(i) <= save_PN_im_state(i);
                    save_coarse_delay(i) <= save_PN_coarse_delay(i);
                    save_addr(i) <= recent_delay_addr(i);
                    save_valid(i) <= '1';
                elsif write_state(i) = '1' then
                    save_valid(i) <= '0';
                end if;
            end loop;
            
            case wr_state_fsm is
                when idle =>
                    if save_valid(0) = '1' then
                        wr_state_fsm <= write_state0_addr0;
                    elsif save_valid(1) = '1' then
                        wr_state_fsm <= write_state1_addr0;
                    end if;
                    state_wrEn_slv(0) <= '0';
                    state_wrAddr <= (others => '0');
                    write_state <= "00";
                    
                when write_state0_addr0 =>
                    wr_state_fsm <= write_state0_addr1;
                    -- 9 bit address, two writes per state.
                    state_wrAddr <= save_addr(0) & '0';
                    state_wrData(63 downto 0) <= save_PN_re(0);
                    state_wrData(71 downto 64) <= save_coarse_delay(0)(7 downto 0);
                    state_wrEn_slv(0) <= '1';
                    write_state <= "01";
                    
                when write_state0_addr1 =>
                    wr_state_fsm <= idle;
                    state_wrAddr <= save_addr(0) & '1';
                    state_wrData(63 downto 0) <= save_PN_im(0);
                    state_wrData(67 downto 64) <= save_coarse_delay(0)(11 downto 8);
                    state_wrData(71 downto 68) <= "0000";
                    state_wrEn_slv(0) <= '1';
                    write_state <= "00";
                    
                when write_state1_addr0 =>
                    wr_state_fsm <= write_state1_addr1;
                    state_wrAddr <= save_addr(1) & '0';
                    state_wrData(63 downto 0) <= save_PN_re(1);
                    state_wrData(71 downto 64) <= save_coarse_delay(1)(7 downto 0);
                    state_wrEn_slv(0) <= '1';
                    write_state <= "10";
                    
                when write_state1_addr1 =>
                    wr_state_fsm <= idle;
                    state_wrAddr <= save_addr(1) & '1';
                    state_wrData(63 downto 0) <= save_PN_im(1);
                    state_wrData(67 downto 64) <= save_coarse_delay(1)(11 downto 8);
                    state_wrData(71 downto 68) <= "0000";
                    state_wrEn_slv(0) <= '1';
                    write_state <= "00";
                
                when others =>
                    wr_state_fsm <= idle;
                    state_wrEn_slv(0) <= '0';
                    write_state <= "00";
            end case;
            
        end if;
    end process;
    
    -- 
    -- Xilinx Parameterized Macro, version 2022.2
    delay_bufferi : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 9,               -- DECIMAL
        ADDR_WIDTH_B => 9,               -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 80,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "false",   -- String
        MEMORY_PRIMITIVE => "block",      -- String
        MEMORY_SIZE => 40960,            -- DECIMAL 80*512 = 40960 
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 80,         -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 1,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 80,        -- DECIMAL
        WRITE_MODE_B => "no_change",     -- String
        WRITE_PROTECT => 1               -- DECIMAL
    ) port map (
        clkb  => clk,
        addrb => delays_rdAddr,  -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        doutb => delays_rdData,  -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        --
        clka  => clk,      
        addra => delays_wrAddr,      -- ADDR_WIDTH_A-bit input: Address for port A write operations
        dina  => delays_wrData,      -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations            
        wea   => delays_wrEn_slv,
        --
        ena => '1',
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',
        rstb => '0',
        sleep => '0',
        dbiterrb => open,
        sbiterrb => open
    );

    ------------------------------------------------------
    -- State memory
    -- Holds the state for 256 sky sources
    --  72 bits wide x 512 deep
    --  Two words per state
    --  Word 0 : 63:0  = real PN state
    --           71:64 = coarse delay (7:0)
    --  Word 1 : 63:0 = imaginary PN state
    --           67:64 = coarse delay (11:8)
    --           71:68 unused
    state_memoryi : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 9,               -- DECIMAL
        ADDR_WIDTH_B => 9,               -- DECIMAL
        AUTO_SLEEP_TIME => 0,            -- DECIMAL
        BYTE_WRITE_WIDTH_A => 72,        -- DECIMAL
        CASCADE_HEIGHT => 0,             -- DECIMAL
        CLOCKING_MODE => "common_clock", -- String
        ECC_MODE => "no_ecc",            -- String
        MEMORY_INIT_FILE => "none",      -- String
        MEMORY_INIT_PARAM => "0",        -- String
        MEMORY_OPTIMIZATION => "false",   -- String
        MEMORY_PRIMITIVE => "block",      -- String
        MEMORY_SIZE => 36864,            -- DECIMAL 72*512 = 36864 
        MESSAGE_CONTROL => 0,            -- DECIMAL
        READ_DATA_WIDTH_B => 72,         -- DECIMAL
        READ_LATENCY_B => 2,             -- DECIMAL
        READ_RESET_VALUE_B => "0",       -- String
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 1,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 72,        -- DECIMAL
        WRITE_MODE_B => "no_change",     -- String
        WRITE_PROTECT => 1               -- DECIMAL
    ) port map (
        clkb  => clk,
        addrb => state_rdAddr,  -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        doutb => state_rdData,  -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        --
        clka  => clk,      
        addra => state_wrAddr,      -- ADDR_WIDTH_A-bit input: Address for port A write operations
        dina  => state_wrData,      -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations            
        wea   => state_wrEn_slv,
        --
        ena => '1',
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',
        rstb => '0',
        sleep => '0',
        dbiterrb => open,
        sbiterrb => open
    );
    
    -- Read delays, current state, and interpolation filter.
    process(clk)
    begin
        if rising_edge(clk) then
            
        end if;
    end process;
    
    -- data sources
    dsrc_geni : for i in 0 to 1 generate
        dsourcei : entity vd_datagen_lib.dataGen_dataSource
        port map (
            clk => clk, --  in std_logic;
            ------------------------------------------------------------------
            -- Input state :
            --  - 1 bit - Load
            --  - 64-bit PN state (real part)
            --  - 64-bit PN state (imaginary part)
            --     * On startup, put the seed into the states.
            --     * If using a sinusoid, bits 15:0 of real hold the current sinusoid phase
            --  - 15 bit phase step
            --  - 1 bit use sinusoid
            --  - 16 bit phase correction
            --  - 16 bit scale
            --  - 12 bit required sample offset
            --     * Number of samples to offset by to implement the coarse delay
            --  - 12 bit current sample offset
            --
            i_load => load_delays(i),                         -- in std_logic;
            i_PN_re_state => PN_re_state,                     -- in (63:0);
            i_PN_im_state => PN_im_state,                     -- in (63:0);
            i_phase_step  => rd_delays_phase_step,            -- in (14:0);
            i_use_sinusoid => rd_delays_use_sinusoid,         -- in std_logic;
            i_phase_correction => rd_delays_phase_correction, -- in (15:0);
            i_scale => rd_delays_scale,                       -- in (15:0);
            i_src_count => delays_rdAddr(7 downto 0),         -- in (7:0);
            i_required_coarse_delay => rd_delays_required_coarse_delay, -- in (11:0);
            i_current_coarse_delay => rd_delays_current_coarse_delay,   -- in (11:0);
            o_idle => dataSource_idle(i), -- out std_logic;
            -- signal to indicate to that the other data source shouldn't be started,
            -- because this datasource has a while to go on generating preload samples.
            o_lots_of_preload_to_go => lots_of_preload_to_go(i), -- out std_logic;
            -------------------------------------------------------------------
            -- Output state for storage :
            --  64 bit PN state (real part)
            --     * Low 16 bits will contain the sinusoid phase if i_use_sinusoid was set
            --  64 bit PN state (imaginary part)
            --  12 bit current sample offset
            o_PN_re_state => save_PN_re_state(i),      -- out (63:0);
            o_PN_im_state => save_PN_im_state(i),      -- out (63:0);
            o_coarse_delay => save_PN_coarse_delay(i), -- out (11:0);
            o_state_valid => save_state_valid(i),      -- out std_logic;
            -------------------------------------------------------------------
            -- Data output
            o_data_re => src_re(i), -- out (15:0);
            o_data_im => src_im(i), -- out (15:0);
            o_preload_valid => src_preload(i), -- out std_logic;
            o_data_valid    => src_valid(i),   -- out std_logic;
            o_data_block    => src_block(i),   -- out std_logic;
            o_phase_correction => src_phase(i), -- out (35:0);
            o_scale => src_scale(i),     -- out (15:0);
            o_src_count => src_count(i), -- out (7:0); index of the polynomial this came from
            o_scale_phase_valid => src_scale_phase_valid(i), -- out std_logic;
            -- o_ready goes high when this module is ready to drive o_data_valid high.
            -- (actually several clocks prior, due to pipeline stages)
            o_ready => o_ready(i), --  out std_logic;
            -- Internal fsm only moves on to generating output data (o_data_valid = '1') when i_ready is high.
            i_ready => i_ready(i) -- in std_logic
        );
        
    end generate;
    
    -- interpolation filter, phase correction, scaling
    filteri : entity vd_datagen_lib.dataGen_filter
    port map(
        clk  => clk, --  in std_logic;
        ------------------------------------------------------------------------
        -- First data source data input
        i_re_src0 => src_re(0), -- in (15:0);
        i_im_src0 => src_im(0), -- in (15:0);
        i_preload_src0 => src_preload(0), -- in std_logic; Preload inputs do not generate output
        -- 18 + 18 bit phase correction
        -- Phase and scale used are loaded prior to i_valid_src0, to be used for the outputs generated by i_valid_src0 (o_re, o_im).
        i_phase_src0 => src_phase(0), -- in (35:0);
        i_phase_src0_valid => src_scale_phase_valid(0), -- in std_logic;
        -- 16 bit scale factor to apply
        i_scale_src0 => src_scale(0), -- in (15:0);
        i_src_count0 => src_count(0), -- in (7:0);
        i_scale_src0_valid => src_scale_phase_valid(0), -- in std_logic;
        -- valid input cycles generate o_valid output cycles after some latency
        -- data output includes a contribution from i_re_src0, i_im_src0 in the cycle where i_valid_src0 = '1'
        i_valid_src0 => src_valid(0), -- in std_logic;
        i_block_src0 => src_block(0),
        --------------------------------------------------------------------------
        -- 2nd data source
        i_re_src1 => src_re(1), -- in (15:0);
        i_im_src1 => src_im(1), -- in (15:0);
        i_preload_src1 => src_preload(1), -- in std_logic;
        i_phase_src1 => src_phase(1), -- in (35:0);
        i_phase_src1_valid => src_scale_phase_valid(1), -- in std_logic;
        -- 16 bit scale factor to apply
        i_scale_src1 => src_scale(1), -- in (15:0);
        i_src_count1 => src_count(1), -- in (7:0);
        i_scale_src1_valid => src_scale_phase_valid(1), -- in std_logic;
        i_valid_src1 => src_valid(1), --  in std_logic;  -- Only one of valid_src0 and valid_src1 should be high at a time.
        i_block_src1 => src_block(1),
        ------------------------------------------------------------------------
        -- FIR taps (16 taps x 16 bits each)
        i_FIR_taps => fir_taps, --  in t_slv_16_arr(31 downto 0);
        i_FIR_valid_src0 => fir_valid(0),   -- in std_logic;
        i_FIR_valid_src1 => fir_valid(1),   -- in std_logic;
        ------------------------------------------------------------------------
        -- Data output
        o_re => o_data_re,   -- out (39:0);
        o_im => o_data_im,   -- out (39:0);
        o_src_count => o_src_count, -- out (7:0);
        o_block => o_block,  -- out std_logic;
        o_valid => o_valid   -- out std_logic
    );
    
end Behavioral;
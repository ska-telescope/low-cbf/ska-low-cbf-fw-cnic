----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- 
-- Dependencies: 
-- 
-- 
----------------------------------------------------------------------------------


library IEEE,technology_lib, PSR_Packetiser_lib, signal_processing_common, HBM_PktController_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use PSR_Packetiser_lib.ethernet_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;

entity tb_hbm_pkt_controller is
--  Port ( );
end tb_hbm_pkt_controller;

architecture Behavioral of tb_hbm_pkt_controller is

    signal clock_100 : std_logic := '0';    -- 3.33ns
    signal clock_300 : std_logic := '0';    -- 3.33ns
    signal clock_400 : std_logic := '0';    -- 2.50ns
    signal clock_322 : std_logic := '0';    -- 3.11ns
    
    signal testCount_400        : integer   := 0;
    signal testCount_300        : integer   := 0;
    signal testCount_322        : integer   := 0;
    
    signal clock_400_rst        : std_logic := '1';
    signal clock_300_rst        : std_logic := '1';
    signal clock_322_rst        : std_logic := '1';
    
    signal power_up_rst_clock_400   : std_logic_vector(31 downto 0) := c_ones_dword;
    signal power_up_rst_clock_300   : std_logic_vector(31 downto 0) := c_ones_dword;
    signal power_up_rst_clock_322   : std_logic_vector(31 downto 0) := c_ones_dword;
    
    signal loop_generator           : integer := 0;
    signal loops                    : integer := 0;
    
    signal streaming_data           : std_logic_vector(511 downto 0);
    signal streaming_data_wren      : std_logic;
    
    signal streaming_data_LBUS      : std_logic_vector(511 downto 0);
    signal streaming_data_S_AXI     : std_logic_vector(511 downto 0);
    
    signal streaming_data_header    : std_logic_vector(511 downto 0);
    
    signal rx_packet_size           : std_logic_vector(13 downto 0) := "00" & x"000";   -- MODULO 64!!
    signal rx_enable_capture        : std_logic := '0';
    
    signal lfaa_bank1_addr          : std_logic_vector(31 downto 0);
    signal rx_soft_reset            : std_logic := '1';
    signal rx_packets_to_capture    : std_logic_vector(31 downto 0);
    
    signal m01_axi_wvalid           : std_logic;
    signal m01_axi_wready           : std_logic;
    signal valid_tracker            : integer := 0;
    

    signal tx_packet_size           : std_logic_vector(13 downto 0);

    signal start_tx                          : std_logic;
    signal reset_tx                          : std_logic;
    signal loop_tx                           : std_logic; 
    signal expected_total_number_of_4k_axi   : std_logic_vector(31 downto 0);
    signal expected_number_beats_per_burst   : std_logic_vector(12 downto 0);
    signal expected_beats_per_packet         : std_logic_vector(31 downto 0);
    signal expected_packets_per_burst        : std_logic_vector(31 downto 0);
    signal expected_total_number_of_bursts   : std_logic_vector(31 downto 0);
    signal expected_number_of_loops          : std_logic_vector(31 downto 0);
    signal time_between_bursts_ns            : std_logic_vector(31 downto 0);
    
    signal readaddr                          : std_logic_vector(31 downto 0); 
    signal update_readaddr                   : std_logic;


    begin
    
    clock_100 <= not clock_100 after 10.00 ns;
    clock_300 <= not clock_300 after 3.33 ns;
    clock_322 <= not clock_322 after 3.11 ns;
    clock_400 <= not clock_400 after 2.50 ns;

    -------------------------------------------------------------------------------------------------------------
    -- powerup resets for SIM
    test_runner_proc_clk300: process(clock_300)
    begin
        if rising_edge(clock_300) then
            -- power up reset logic
            if power_up_rst_clock_300(31) = '1' then
                power_up_rst_clock_300(31 downto 0) <= power_up_rst_clock_300(30 downto 0) & '0';
                clock_300_rst   <= '1';
                testCount_300   <= 0;
            else
                clock_300_rst   <= '0';
                testCount_300   <= testCount_300 + 1;
            end if;
        end if;
    end process;
    
    test_runner_proc_clk400: process(clock_400)
    begin
        if rising_edge(clock_400) then
            -- power up reset logic
            if power_up_rst_clock_400(31) = '1' then
                power_up_rst_clock_400(31 downto 0) <= power_up_rst_clock_400(30 downto 0) & '0';
                testCount_400   <= 0;
                clock_400_rst   <= '1';
            else
                testCount_400   <= testCount_400 + 1;
                clock_400_rst   <= '0';
            end if;
        end if;
    end process;

    -------------------------------------------------------------------------------------------------------------

    rx_packets_to_capture       <= 32D"120";

    tx_packet_size              <= 14D"2154";
    time_between_bursts_ns      <= 32D"175";

    expected_number_of_loops    <= 32D"5";      -- 6 loops
    loop_tx                     <= '1';         -- enabled.

    expected_total_number_of_4k_axi     <= 32D"11";

    expected_number_beats_per_burst     <= 13D"34";
    expected_beats_per_packet           <= 32D"34";

    expected_packets_per_burst          <= 32D"1";

    expected_total_number_of_bursts     <= 32D"20";


    run_proc : process(clock_300)
    begin
        if rising_edge(clock_300) then
            if clock_300_rst = '1' then
                reset_tx        <= '1';
                start_tx        <= '0';    
            else
            
                if testCount_300 = 120 then
                    reset_tx <= '0';
                elsif testCount_300 = 150 then
                    start_tx <= '1';
                end if;
            

            end if;
        end if;
    end process;


    -------------------------------------------------------------------------------------------------------------

    dut : entity HBM_PktController_lib.HBM_PktController port map (
        clk_freerun                     => clock_100,
        -- shared memory interface clock (300 MHz)
        i_shared_clk                    => clock_300,
        i_shared_rst                    => clock_300_rst,
    
        o_reset_packet_player           => open,
        ------------------------------------------------------------------------------------
        -- Data from CMAC module after CDC in shared memory clock domain
        i_data_from_cmac                => zero_512,
        i_data_valid_from_cmac          => '0',
    
        ------------------------------------------------------------------------------------
        -- config and status registers interface
        -- rx
        i_rx_packet_size                => (others => '0'),
        i_rx_soft_reset                 => '0',
        i_enable_capture                => '0',
    
        i_lfaa_bank1_addr               => zero_dword,
        i_lfaa_bank2_addr               => zero_dword,
        i_lfaa_bank3_addr               => zero_dword,
        i_lfaa_bank4_addr               => zero_dword,
        update_start_addr               => '0',
        i_rx_bank_enable                => x"0",
    
        o_1st_4GB_rx_addr               => open,
        o_2nd_4GB_rx_addr               => open,
        o_3rd_4GB_rx_addr               => open,
        o_4th_4GB_rx_addr               => open,
    
        o_capture_done                  => open,
        o_num_packets_received          => open,
        i_rx_packets_to_capture         => rx_packets_to_capture,
        i_rx_flush_to_hbm               => '0',
    
        -- tx
        i_tx_packet_size                    => tx_packet_size,
        i_start_tx                          => start_tx,
        i_reset_tx                          => reset_tx,
    
        i_loop_tx                           => loop_tx,
        i_expected_total_number_of_4k_axi   => expected_total_number_of_4k_axi,
        i_expected_number_beats_per_burst   => expected_number_beats_per_burst,
        i_expected_beats_per_packet         => expected_beats_per_packet,
        i_expected_packets_per_burst        => expected_packets_per_burst,
        i_expected_total_number_of_bursts   => expected_total_number_of_bursts,
        i_expected_number_of_loops          => expected_number_of_loops,
        i_time_between_bursts_ns            => time_between_bursts_ns,
    
        i_readaddr                          => zero_dword,
        i_update_readaddr                   => '0',
    
        o_tx_addr                           => open,
        o_tx_boundary_across_num            => open,
        o_axi_rvalid_but_fifo_full          => open,
        
        o_tx_complete                       => open,
        o_tx_packets_to_mac                 => open,
        o_tx_packet_count                   => open,
    ------------------------------------------------------------------------------------
        -- debug ports
        o_rd_fsm_debug                      => open,
        o_output_fsm_debug                  => open,
        o_input_fsm_debug                   => open,
            
    ------------------------------------------------------------------------------------
        -- Data output, to the packetizer
        -- Add the packetizer records here
        o_packetiser_data_in_wr             => open,
        o_packetiser_data                   => open,
        o_packetiser_bytes_to_transmit      => open,
        i_packetiser_data_to_player_rdy     => '1',
    
        -----------------------------------------------------------------------
        i_schedule_action                   => x"00",
        -----------------------------------------------------------------------
    
        -----------------------------------------------------------------------
        --first 4GB section of AXI
        --aw bus
        m01_axi_awvalid   => open,
        m01_axi_awready   => '0',
        m01_axi_awaddr    => open,
        m01_axi_awlen     => open,
        --w bus
        m01_axi_wvalid    => open,
        m01_axi_wdata     => open,
        m01_axi_wstrb     => open,
        m01_axi_wlast     => open,
        m01_axi_wready    => '0',
    
        -- ar bus - read address
        m01_axi_arvalid   => open,
        m01_axi_arready   => '1',
        m01_axi_araddr    => open,
        m01_axi_arlen     => open,
        -- r bus - read data
        m01_axi_rvalid    => '1',
        m01_axi_rready    => open,
        m01_axi_rdata     => (others => '0'),
        m01_axi_rlast     => '0',
        m01_axi_rresp     => (others => '0'),
    
        --second 4GB section of AXI
        --aw bus
        m02_axi_awvalid   => open,
        m02_axi_awready   => '0',
        m02_axi_awaddr    => open,
        m02_axi_awlen     => open,
        --w bus
        m02_axi_wvalid    => open,
        m02_axi_wdata     => open,
        m02_axi_wstrb     => open,
        m02_axi_wlast     => open,
        m02_axi_wready    => '0',
    
        -- ar bus - read address
        m02_axi_arvalid   => open,
        m02_axi_arready   => '0',
        m02_axi_araddr    => open,
        m02_axi_arlen     => open,
        -- r bus - read data
        m02_axi_rvalid    => '0',
        m02_axi_rready    => open,
        m02_axi_rdata     => (others => '0'),
        m02_axi_rlast     => '0',
        m02_axi_rresp     => (others => '0'),
    
        --third 4GB section of AXI
        --aw bus
        m03_axi_awvalid   => open,
        m03_axi_awready   => '0',
        m03_axi_awaddr    => open,
        m03_axi_awlen     => open,
        --w bus
        m03_axi_wvalid    => open,
        m03_axi_wdata     => open,
        m03_axi_wstrb     => open,
        m03_axi_wlast     => open,
        m03_axi_wready    => '0',
    
        -- ar bus - read address
        m03_axi_arvalid   => open,
        m03_axi_arready   => '0',
        m03_axi_araddr    => open,
        m03_axi_arlen     => open,
        -- r bus - read data
        m03_axi_rvalid    => '0',
        m03_axi_rready    => open,
        m03_axi_rdata     => (others => '0'),
        m03_axi_rlast     => '0',
        m03_axi_rresp     => (others => '0'),
    
        --fourth 4GB section of AXI
        --aw bus
        m04_axi_awvalid   => open,
        m04_axi_awready   => '0',
        m04_axi_awaddr    => open,
        m04_axi_awlen     => open,
        --w bus
        m04_axi_wvalid    => open,
        m04_axi_wdata     => open,
        m04_axi_wstrb     => open,
        m04_axi_wlast     => open,
        m04_axi_wready    => '1',
    
        -- ar bus - read address
        m04_axi_arvalid   => open,
        m04_axi_arready   => '0',
        m04_axi_araddr    => open,
        m04_axi_arlen     => open,
        -- r bus - read data
        m04_axi_rvalid    => '0',
        m04_axi_rready    => open,
        m04_axi_rdata     => (others => '0'),
        m04_axi_rlast     => '0',
        m04_axi_rresp     => (others => '0')
    );


end Behavioral;

----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 07/13/2022 
-- Module Name: s_axi_packet_capture - Behavioral
--
-- Version 2
--
-- Description: 
--      This block is to capture Streaming packets from the CMAC on 322 MHz clock domain. 
--      Check if the received packet is the desired length.
--      There is an almost saw tooth write pattern based on the 64/66 gearboxing on the 322 MHz clock domain from the CMAC.
--      All packets are captured into a FIFO, to smooth this out.
--      At the same time, bytes received are counted and number of 64 byte writes are calculated.
--
--      CDC from 322 to 300.
--      Data is then streamed into the second banks of FIFOs for clock crossing
--      Reading out is continuous and shorter than the write cycle for a packet.
--      Packets that are shorter than the desired are dropped at this point,
--
--      Write to HBM as a single block, once data is lodged in the second bank of FIFOs.
--      WR signal will be continuously high and fall at end of packet.
--      Timestamp and other meta data will precede the packet.
-- 
--      HBM takes in LEndian formatted data, the S_AXI stream from the CMAC, shown below, already has this format.
--      Any other data sent, like a timestamp, needs to be appropriately byte swapped instead of doing this in software.
-- 
-- Behaviour of S_AXI from CMAC.
--      Due to the gearboxing in the CMAC, a packet sent at line speed will present on the S_AXI interface with tvalid toggling.
--      tlast indicates the packet has finished and you can reset logic around this if needed.
--      CMAC interface clocks in at ~322 MHz and with a width of 64 bytes has a data rate of ~164.8 Gbps, which explains the toggling.
-- 
--      Data on the bus is in the following form for an Ethernet Frame ...
--      dst_mac  <= CMAC_rx_axis_tdata(7 downto 0) & CMAC_rx_axis_tdata(15 downto 8) & CMAC_rx_axis_tdata(23 downto 16) & CMAC_rx_axis_tdata(31 downto 24) & CMAC_rx_axis_tdata(39 downto 32) & CMAC_rx_axis_tdata(47 downto 40);
--      src_mac  <= CMAC_rx_axis_tdata(55 downto 48) & CMAC_rx_axis_tdata(63 downto 56) & CMAC_rx_axis_tdata(71 downto 64) & CMAC_rx_axis_tdata(79 downto 72) & CMAC_rx_axis_tdata(87 downto 80) & CMAC_rx_axis_tdata(95 downto 88);
--      eth_type <= CMAC_rx_axis_tdata(103 downto 96) & CMAC_rx_axis_tdata(111 downto 104);
--
--      
--
----------------------------------------------------------------------------------


library IEEE, common_lib, signal_processing_common, ethernet_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ethernet_lib.ethernet_pkg.ALL;
USE common_lib.common_pkg.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity s_axi_packet_capture is
    Generic (
        g_DEBUG_ILA             : IN BOOLEAN := FALSE
    );
    Port ( 
        --------------------------------------------------------
        -- 100G 
        i_clk_100GE             : in std_logic;
        i_eth100G_locked        : in std_logic; -- pseudo reset
        
        i_clk_300               : in std_logic;
        i_clk_300_rst           : in std_logic;

        i_enable_capture        : in std_logic;
        i_rx_packet_size        : in std_logic_vector(13 downto 0);     -- Max size is 9000.
        i_rx_reset_capture      : in std_logic;
        i_reset_counter         : in std_logic;
        i_rx_packet_size_abs    : in std_logic;

        o_target_count          : out std_logic_vector(31 downto 0);
        o_nontarget_count       : out std_logic_vector(31 downto 0);
        o_LFAA_spead_count      : out std_logic_vector(31 downto 0);
        o_PSR_PST_count         : out std_logic_vector(31 downto 0);
        o_CODIF_2154_count      : out std_logic_vector(31 downto 0);
        
        i_rx_packets_to_capture : in std_logic_vector(31 downto 0);

        -- 100G RX S_AXI interface ~322 MHz
        i_rx_axis_tdata         : in std_logic_vector ( 511 downto 0 );
        i_rx_axis_tkeep         : in std_logic_vector ( 63 downto 0 );
        i_rx_axis_tlast         : in std_logic;
        o_rx_axis_tready        : out std_logic;
        i_rx_axis_tuser         : in std_logic_vector ( 79 downto 0 );
        i_rx_axis_tvalid        : in std_logic;
        
        -- Data to HBM writer - 300 MHz
        o_data_to_hbm           : out std_logic_vector(511 downto 0);
        o_data_to_hbm_wr        : out std_logic
    
    );
end s_axi_packet_capture;

architecture RTL of s_axi_packet_capture is

COMPONENT ila_0
    PORT (
        clk     : IN STD_LOGIC;
        probe0  : IN STD_LOGIC_VECTOR(191 DOWNTO 0)
    );       
END COMPONENT;

constant CODIF_2154_length      : std_logic_vector (13 downto 0) := 14D"2154";
constant PST_PSR_length         : std_logic_vector (13 downto 0) := 14D"6330";
constant SPEAD_LFAA_length      : std_logic_vector (13 downto 0) := 14D"8292";

signal rx_axis_tdata_int        : std_logic_vector ( 511 downto 0 );
signal rx_axis_tkeep_int        : std_logic_vector ( 63 downto 0 );
signal rx_axis_tlast_int        : std_logic;
signal rx_axis_tuser_int        : std_logic_vector ( 79 downto 0 );
signal rx_axis_tvalid_int       : std_logic;

signal rx_axis_tdata_int_d1     : std_logic_vector ( 511 downto 0 );
signal rx_axis_tkeep_int_d1     : std_logic_vector ( 63 downto 0 );
signal rx_axis_tlast_int_d1     : std_logic;
signal rx_axis_tuser_int_d1     : std_logic_vector ( 79 downto 0 );
signal rx_axis_tvalid_int_d1    : std_logic;

signal rx_axis_tready_int       : std_logic;

signal inc_packet_byte_count    : std_logic_vector (13 downto 0) := "00" & x"040"; 
signal packet_byte_count        : std_logic_vector (13 downto 0);

signal words_to_send_to_hbm     : unsigned(7 downto 0);

signal data_to_hbm_wr_int       : std_logic;

signal rx_packet_size           : std_logic_vector(13 downto 0);

signal cmac_rx_reset_capture        : std_logic;
signal cmac_rx_packet_size          : std_logic_vector(13 downto 0);
signal cmac_rx_packet_size_abs      : std_logic;
signal cmac_rx_enable_capture       : std_logic;

type hbm_streaming_statemachine is (IDLE, PREP, DATA, FINISH);
signal hbm_streaming_sm : hbm_streaming_statemachine;

type packed_to_cdc_statemachine is (IDLE, CHECK, DATA, FINISH);
signal packed_to_cdc_sm : packed_to_cdc_statemachine;

signal packed_sm_rd_count           : unsigned(7 downto 0);
signal enable_cdc_wr                : std_logic;

signal cmac_reset                   : std_logic;
signal cmac_reset_combined          : std_logic;

signal ila_data_in_r                : std_logic_vector(127 downto 0);
signal ila_data_out_r               : std_logic_vector(127 downto 0);

signal ptp_seconds                  : std_logic_vector(47 downto 0);
signal ptp_sub_sec                  : std_logic_vector(31 downto 0);

signal capture_timestamp            : std_logic;
signal timestamp                    : std_logic_vector(79 downto 0);

constant META_DATA_FIFO_WIDTH       : integer := 104;

signal meta_data_fifo_wr            : std_logic;
signal meta_data_fifo_rd            : std_logic;
signal meta_data_fifo_empty         : std_logic;
signal meta_data_fifo_data          : std_logic_vector((META_DATA_FIFO_WIDTH-1) downto 0);
signal meta_data_fifo_q             : std_logic_vector((META_DATA_FIFO_WIDTH-1) downto 0);

signal pack_meta_data_fifo_wr       : std_logic;
signal pack_meta_data_fifo_rd       : std_logic;
signal pack_meta_data_fifo_empty    : std_logic;
signal pack_meta_data_fifo_data     : std_logic_vector((META_DATA_FIFO_WIDTH-1) downto 0);
signal pack_meta_data_fifo_q        : std_logic_vector((META_DATA_FIFO_WIDTH-1) downto 0);

signal meta_out                     : std_logic_vector(511 downto 0) := zero_512;
signal data_to_hbm                  : std_logic_vector(511 downto 0);
signal data_to_hbm_wr               : std_logic;

signal enable_bunching_wr           : std_logic := '0';
signal enable_bunching_cnt          : unsigned(7 downto 0) := x"FF";

------------------------------------------------------------------------------
-- packet stats

signal b0, b1, b2, b3                                   : unsigned(13 downto 0);
signal b0_cached, b1_cached, b2_cached, b3_cached       : unsigned(13 downto 0);        

signal stat_byte_count                                  : unsigned(13 downto 0);
signal stat_byte_count_cache                            : unsigned(13 downto 0);
signal stat_byte_count_working                          : unsigned(13 downto 0);
signal byte_count_last_packet                           : unsigned(13 downto 0);

signal read_64b_last_packet                             : unsigned(7 downto 0);

signal b_quad                                           : std_logic_vector(3 downto 0);
signal stat_packet_length_final                         : std_logic_vector(13 downto 0);

signal stat_done                                        : std_logic;
signal expected_length_detect                           : std_logic;
signal unexpected_length_detect                         : std_logic;
signal CODIF_2154_length_detect                         : std_logic;
signal PST_PSR_length_detect                            : std_logic;
signal SPEAD_LFAA_length_detect                         : std_logic;

signal target_count_int                                 : std_logic_vector(31 downto 0);

constant STAT_REGISTERS                                 : integer := 5;

signal stats_count                                      : t_slv_32_arr(0 to (STAT_REGISTERS-1));
signal stats_increment                                  : t_slv_3_arr(0 to (STAT_REGISTERS-1));
signal stats_to_host_data_out                           : t_slv_32_arr(0 to (STAT_REGISTERS-1));

signal packet_length_final                              : std_logic_vector(15 downto 0);

signal read_carry                                       : unsigned(0 downto 0);

------------------------------------------------------------------------------
-- injest FIFO
constant C_INJEST_FIFO_WIDTH                            : integer := 512;

signal packup_fifo_data                                 : std_logic_vector((C_INJEST_FIFO_WIDTH-1) downto 0);
signal packup_fifo_q                                    : std_logic_vector((C_INJEST_FIFO_WIDTH-1) downto 0);
signal packup_fifo_wr                                   : std_logic;
signal packup_fifo_rd                                   : std_logic;
signal packup_fifo_empty                                : std_logic;

signal data_fifo_wr                                     : std_logic;
signal data_fifo_rd                                     : std_logic;
signal data_fifo_empty                                  : std_logic;
signal data_fifo_data                                   : std_logic_vector((C_INJEST_FIFO_WIDTH-1) downto 0);
signal data_fifo_q                                      : std_logic_vector((C_INJEST_FIFO_WIDTH-1) downto 0);

------------------------------------------------------------------------------

signal debug_count                                      : unsigned(7 downto 0) := (others=>'0');

begin

------------------------------------------------------------------------------
-- assume always ready if CMAC is locked.
o_rx_axis_tready        <= rx_axis_tready_int;
o_data_to_hbm           <= data_to_hbm;
o_data_to_hbm_wr        <= data_to_hbm_wr_int;

-- register AXI bus
s_axi_proc : process(i_clk_100GE)
begin
    if rising_edge(i_clk_100GE) then
    
        rx_axis_tready_int      <= i_eth100G_locked;
        
        rx_axis_tdata_int       <= i_rx_axis_tdata;
        rx_axis_tkeep_int       <= i_rx_axis_tkeep;
        rx_axis_tlast_int       <= i_rx_axis_tlast;
        rx_axis_tuser_int       <= i_rx_axis_tuser;
        rx_axis_tvalid_int      <= i_rx_axis_tvalid;
        
        rx_axis_tdata_int_d1    <= rx_axis_tdata_int;
        rx_axis_tkeep_int_d1    <= rx_axis_tkeep_int;
        rx_axis_tlast_int_d1    <= rx_axis_tlast_int;
        rx_axis_tuser_int_d1    <= rx_axis_tuser_int;
        rx_axis_tvalid_int_d1   <= rx_axis_tvalid_int;
        

    end if;
end process;

------------------------------------------------------------------------------
-- BUNCHING

-- clutching 
-- The software reset can be released with no regard to the incoming data stream.
-- This means the two FIFOs below, (depending on traffic), can capture a packet mid stream.
-- 9000 bytes or 141 writes on the interface is possible.
-- due to the gearboxing, this will circuit will wait upto 256 cycles or a tlast to enable this writing
-- after a reset.
-- this should allow the static and live capture cases to continue.
clutch_proc : process( i_clk_100GE )
begin
    if rising_edge(i_clk_100GE) then
        if (cmac_reset_combined = '1') then
            enable_bunching_wr      <= '0';
            enable_bunching_cnt     <= x"FF";
        else
            if (cmac_rx_enable_capture = '1') then
                if (enable_bunching_cnt = x"00") OR (rx_axis_tlast_int_d1 = '1') then
                    enable_bunching_wr      <= '1';
                else
                    enable_bunching_cnt     <= enable_bunching_cnt - 1;
                end if;
            end if;
        end if ;

    end if;
end process;

-- PACK up the incoming data and generate the timestamp and control metadata for later on.
packup_fifo_data    <= rx_axis_tdata_int_d1;
packup_fifo_wr      <= rx_axis_tvalid_int_d1 AND enable_bunching_wr;

-- FIFO to pack up data.
i_packup_fifo : entity signal_processing_common.xpm_sync_fifo_wrapper 
    generic map 
    (
        FIFO_MEMORY_TYPE    => "uram",
        READ_MODE           => "fwft",
        FIFO_DEPTH          => 1024,        -- this should use 8 urams, 64w x 4096d config.
        DATA_WIDTH          => C_INJEST_FIFO_WIDTH
    )
    port map ( 
        fifo_reset          => cmac_reset_combined,    
        fifo_clk            => i_clk_100GE,   
        -- RD    
        fifo_rd             => packup_fifo_rd,
        fifo_q              => packup_fifo_q,
        fifo_empty          => packup_fifo_empty,
        -- WR        
        fifo_wr             => packup_fifo_wr,
        fifo_data           => packup_fifo_data 
    );

pack_meta_data_fifo_data(79 downto 0)       <= timestamp;
pack_meta_data_fifo_data(95 downto 80)      <= packet_length_final(15 downto 0);
pack_meta_data_fifo_data(103 downto 96)     <= std_logic_vector(read_64b_last_packet);

pack_meta_data_fifo_wr                      <= rx_axis_tlast_int_d1 AND enable_bunching_wr;

-- write timestamp if the packet is valid.
packup_meta_data_fifo : entity signal_processing_common.xpm_sync_fifo_wrapper
    Generic map (
        FIFO_MEMORY_TYPE    => "bram",
        READ_MODE           => "fwft",
        FIFO_DEPTH          => 16,
        DATA_WIDTH          => META_DATA_FIFO_WIDTH
    )
    Port Map ( 
        fifo_reset          => cmac_reset_combined,
        fifo_clk            => i_clk_100GE,
        -- WR        
        fifo_wr             => pack_meta_data_fifo_wr,
        fifo_data           => pack_meta_data_fifo_data,
        -- RD    
        fifo_rd             => pack_meta_data_fifo_rd,
        fifo_q              => pack_meta_data_fifo_q,
        fifo_empty          => pack_meta_data_fifo_empty
    );

------------------------------------------------------------------------------
-- EMPTY packed FIFO to CDC FIFO
-- Check byte size to enable the writing to the CDC FIFO.

fifo_to_fifo_proc : process(i_clk_100GE)
begin
    if rising_edge(i_clk_100GE) then
        if cmac_rx_reset_capture = '1' then
            packet_byte_count       <= "00" & x"000";
            packed_to_cdc_sm        <= IDLE;
            enable_cdc_wr           <= '0';
            pack_meta_data_fifo_rd  <= '0';
            packup_fifo_rd          <= '0';
            packed_sm_rd_count      <= x"00";
        else
            
            case packed_to_cdc_sm is
                WHEN IDLE =>
                    enable_cdc_wr           <= '0';
                    pack_meta_data_fifo_rd  <= '0';

                    if pack_meta_data_fifo_empty = '0' and (cmac_rx_enable_capture = '1') then
                        packed_to_cdc_sm        <= CHECK;
                    end if;

                WHEN CHECK =>
                    -- if packet size is greater or equal to desired and don't want absolute then capture.
                    if (pack_meta_data_fifo_q(93 downto 80) >= cmac_rx_packet_size) AND (cmac_rx_packet_size_abs = '0') then
                        enable_cdc_wr       <= '1';
                    -- if packet size is equal to desired and want absolute to capture.
                    elsif (pack_meta_data_fifo_q(93 downto 80) = cmac_rx_packet_size) AND (cmac_rx_packet_size_abs = '1') then
                        enable_cdc_wr       <= '1';
                    end if;
                    packed_sm_rd_count      <= unsigned(pack_meta_data_fifo_q(103 downto 96));
                    packed_to_cdc_sm        <= DATA;

                WHEN DATA =>
                    packed_sm_rd_count      <= packed_sm_rd_count - 1;
                    packup_fifo_rd          <= '1';
                    
                    if packed_sm_rd_count = 1 then
                        packed_to_cdc_sm        <= FINISH;
                        pack_meta_data_fifo_rd  <= '1';
                    end if;

                WHEN FINISH =>
                    packup_fifo_rd          <= '0';
                    pack_meta_data_fifo_rd  <= '0';
                    packed_to_cdc_sm        <= IDLE;

                WHEN OTHERS => 
                    packed_to_cdc_sm        <= IDLE;
            end case;
               
        end if;
    end if;
end process;

------------------------------------------------------------------------------
-- capture timestamp
write_rx_data_proc : process(i_clk_100GE)
begin
    if rising_edge(i_clk_100GE) then
        if cmac_rx_reset_capture = '1' then
            capture_timestamp   <= '1';
            timestamp           <= zero_word & zero_64;
        else
            -- timestamp is provided with the first write, otherwise bus provides zero.
            -- re-enable the capture of timestamp after last write from packet indicated.
            if rx_axis_tlast_int = '1' then
                capture_timestamp   <= '1';
            elsif rx_axis_tvalid_int = '1' then
                capture_timestamp   <= '0';
            end if;

            if rx_axis_tvalid_int = '1' and capture_timestamp = '1' then
                -- byte swap as per how it will be read from HBM.
                timestamp(79 downto 40) <= rx_axis_tuser_int(7 downto 0) & rx_axis_tuser_int(15 downto 8) & rx_axis_tuser_int(23 downto 16) & rx_axis_tuser_int(31 downto 24) & rx_axis_tuser_int(39 downto 32);
                timestamp(39 downto 0)  <= rx_axis_tuser_int(47 downto 40) & rx_axis_tuser_int(55 downto 48) & rx_axis_tuser_int(63 downto 56) & rx_axis_tuser_int(71 downto 64) & rx_axis_tuser_int(79 downto 72);
            end if;
        end if;
    end if;
end process;

------------------------------------------------------------------------------
-- CDC FIFOs

meta_data_fifo_data(79 downto 0)    <= pack_meta_data_fifo_q(79 downto 0);
meta_data_fifo_data(95 downto 80)   <= pack_meta_data_fifo_q(87 downto 80) & pack_meta_data_fifo_q(95 downto 88);   -- swap byte count for software alignment.
meta_data_fifo_data(103 downto 96)  <= pack_meta_data_fifo_q(103 downto 96);
meta_data_fifo_wr                   <= pack_meta_data_fifo_rd AND enable_cdc_wr;

-- write timestamp if the packet is valid.
meta_data_fifo : entity signal_processing_common.xpm_fifo_wrapper
    Generic map (
        FIFO_DEPTH      => 16,
        DATA_WIDTH      => META_DATA_FIFO_WIDTH
    )
    Port Map ( 
        -- WR clk reset
        fifo_reset      => cmac_reset_combined,
        -- WR        
        fifo_wr_clk     => i_clk_100GE,
        fifo_wr         => meta_data_fifo_wr,
        fifo_data       => meta_data_fifo_data,
        -- RD    
        fifo_rd_clk     => i_clk_300,
        fifo_rd         => meta_data_fifo_rd,
        fifo_q          => meta_data_fifo_q,
        fifo_empty      => meta_data_fifo_empty
    );
    
data_fifo_wr    <= packup_fifo_rd AND enable_cdc_wr;
data_fifo_data  <= packup_fifo_q;

packet_data_fifo : entity signal_processing_common.xpm_fifo_wrapper
    Generic map (
        FIFO_DEPTH      => 256,
        DATA_WIDTH      => C_INJEST_FIFO_WIDTH
    )
    Port Map ( 
        -- WR clk reset
        fifo_reset      => cmac_reset_combined,
        -- WR        
        fifo_wr_clk     => i_clk_100GE,
        fifo_wr         => data_fifo_wr,
        fifo_data       => data_fifo_data,
        -- RD    
        fifo_rd_clk     => i_clk_300,
        fifo_rd         => data_fifo_rd,
        fifo_q          => data_fifo_q,
        fifo_empty      => data_fifo_empty
    );

    
------------------------------------------------------------------------------
packet_config_cdc : entity signal_processing_common.sync_vector
    generic map (
        WIDTH => 17
    )
    Port Map ( 
        clock_a_rst             => i_clk_300_rst,
        Clock_a                 => i_clk_300,
        data_in(0)              => i_rx_reset_capture,
        data_in(14 downto 1)    => i_rx_packet_size,
        data_in(15)             => i_rx_packet_size_abs,
        data_in(16)             => i_enable_capture,
        
        Clock_b                 => i_clk_100GE,
        data_out(0)             => cmac_rx_reset_capture,
        data_out(14 downto 1)   => cmac_rx_packet_size,
        data_out(15)            => cmac_rx_packet_size_abs,
        data_out(16)            => cmac_rx_enable_capture
    );  

cmac_reset          <= NOT i_eth100G_locked;

cmac_reset_combined <= cmac_rx_reset_capture OR cmac_reset;

------------------------------------------------------------------------------
-- stream out data to HBM SM
-- trigger based on the page flip and cache that while streaming.
-- The STAMP state is there to lengthen the packet write sequence and slip in the timestamp at the end.

meta_out(META_DATA_FIFO_WIDTH-1 downto 0)  <= meta_data_fifo_q;


config_proc : process(i_clk_300)
begin
    if rising_edge(i_clk_300) then
        -- ARGS axi Reset
        if (i_clk_300_rst = '1') OR (i_rx_reset_capture = '1' ) then
            hbm_streaming_sm        <= IDLE;
            data_to_hbm_wr_int      <= '0';
            meta_data_fifo_rd       <= '0';
            data_fifo_rd            <= '0';
            words_to_send_to_hbm    <= x"00";
        else
                                   
            case hbm_streaming_sm is
                WHEN IDLE =>
                    meta_data_fifo_rd       <= '0';
                    data_to_hbm_wr_int      <= '0';
                    if meta_data_fifo_empty = '0' then
                        hbm_streaming_sm    <= PREP;
                    end if;

                WHEN PREP =>
                    words_to_send_to_hbm    <= unsigned(meta_data_fifo_q(103 downto 96));
                    hbm_streaming_sm        <= DATA;
                    data_to_hbm_wr_int      <= '1';
                    data_fifo_rd            <= '1';

                WHEN DATA =>
                    words_to_send_to_hbm    <= words_to_send_to_hbm - 1;
                    
                    if words_to_send_to_hbm = 2 then
                        hbm_streaming_sm    <= FINISH;
                        meta_data_fifo_rd   <= '1';
                    end if;

                WHEN FINISH =>
                    data_fifo_rd            <= '0';
                    meta_data_fifo_rd       <= '0';
                    hbm_streaming_sm        <= IDLE;

                WHEN OTHERS => 
                hbm_streaming_sm        <= IDLE;
            end case;
        end if;
        
        if (hbm_streaming_sm = PREP) then
            data_to_hbm <= meta_out;
        else
            data_to_hbm <= data_fifo_q;
        end if;
        
        data_to_hbm_wr  <= data_to_hbm_wr_int;
        
        if data_to_hbm_wr = '1' then
            debug_count <= debug_count + 1;
        else
            debug_count <= x"00";
        end if;

    end if;
end process;

------------------------------------------------------------------------------
-- calculate stats for various type of packets detected.
-- useful when we have multipacket traffic in the system.
-- used to create meta data for HBM packing.

b0(4 downto 0)              <= byte_en_to_unsigned_count(i_rx_axis_tkeep(15 downto 0));
b0(13 downto 5)             <= '0' & x"00"; 
b1(4 downto 0)              <= byte_en_to_unsigned_count(i_rx_axis_tkeep(31 downto 16));
b1(13 downto 5)             <= '0' & x"00"; 
b2(4 downto 0)              <= byte_en_to_unsigned_count(i_rx_axis_tkeep(47 downto 32));
b2(13 downto 5)             <= '0' & x"00"; 
b3(4 downto 0)              <= byte_en_to_unsigned_count(i_rx_axis_tkeep(63 downto 48));
b3(13 downto 5)             <= '0' & x"00"; 

b_quad                      <= i_rx_axis_tkeep(63) & i_rx_axis_tkeep(47) & i_rx_axis_tkeep(31) & i_rx_axis_tkeep(15);

packet_length_final         <= "00" & std_logic_vector(byte_count_last_packet);

        
-- BYTE calculation of incoming packet is done on early data in the pipeline so that the time stamp and the byte count
-- can be written at the same time as the final bytes for the packet to the FIFO caches.

inc_packet_calc : process (i_clk_100GE)
begin
    if rising_edge(i_clk_100GE) then
        if cmac_reset_combined = '1' then
            stat_byte_count             <= "00" & x"000";
            stat_done                   <= '0';
            stat_packet_length_final    <= "00" & x"000";
            stat_byte_count_working     <= "00" & x"000";
            stat_byte_count_cache       <= "00" & x"000";
            read_64b_last_packet        <= x"00";
            read_carry(0)               <= '0';
        else
        
            if i_rx_axis_tlast = '1' then
                stat_byte_count     <= "00" & x"000";
            elsif i_rx_axis_tvalid = '1' then
                stat_byte_count     <= stat_byte_count + 64;
            end if;
    
            if i_rx_axis_tlast = '1' then
                -- assume there needs to be an additional read
                read_carry(0) <= '1';
                -- cache the count so far.
                stat_byte_count_cache   <= stat_byte_count;

                if b_quad = "1111" then
                    stat_byte_count_working <= "00" & x"040";
                elsif b_quad = "0111" then
                    stat_byte_count_working <= b3 + 48;
                elsif b_quad = "0011" then
                    stat_byte_count_working <= b2 + 32;
                elsif b_quad = "0001" then
                    stat_byte_count_working <= b1 + 16;
                elsif b_quad = "0000" then
                    stat_byte_count_working <= b0;
                    -- this will null out the additional read.
                    if b0 = "00000" then
                        read_carry(0) <= '0';
                    end if;
                end if;             
            end if;

            byte_count_last_packet      <= stat_byte_count_cache + stat_byte_count_working;
            read_64b_last_packet        <= stat_byte_count_cache(13 downto 6) + read_carry;
 
            stat_done                   <= rx_axis_tlast_int_d1;
                                
            stat_packet_length_final    <= std_logic_vector(byte_count_last_packet);

            if stat_done = '1' then
                if stat_packet_length_final >= cmac_rx_packet_size then
                    expected_length_detect      <= '1';
                else
                    unexpected_length_detect    <= '1';
                end if;
            
                if stat_packet_length_final = CODIF_2154_length then
                    CODIF_2154_length_detect    <= '1';
                end if;
                
                if stat_packet_length_final = PST_PSR_length then
                    PST_PSR_length_detect       <= '1';
                end if;
                
                if stat_packet_length_final = SPEAD_LFAA_length then
                    SPEAD_LFAA_length_detect    <= '1';
                end if;
            else
                expected_length_detect          <= '0';
                unexpected_length_detect        <= '0';
                CODIF_2154_length_detect        <= '0';
                PST_PSR_length_detect           <= '0';
                SPEAD_LFAA_length_detect        <= '0';
            end if;
        end if;
    end if;
end process;

stats_increment(0) <= "00" & expected_length_detect;
stats_increment(1) <= "00" & unexpected_length_detect;
stats_increment(2) <= "00" & CODIF_2154_length_detect;
stats_increment(3) <= "00" & PST_PSR_length_detect;
stats_increment(4) <= "00" & SPEAD_LFAA_length_detect;


stats_accumulators: FOR i IN 0 TO (STAT_REGISTERS-1) GENERATE
    u_cnt_acc: ENTITY common_lib.common_accumulate
        GENERIC MAP (
            g_representation  => "UNSIGNED")
        PORT MAP (
            rst      => cmac_reset_combined,
            clk      => i_clk_100GE,
            clken    => '1',
            sload    => '0',
            in_val   => '1',
            in_dat   => stats_increment(i),
            out_dat  => stats_count(i)
        );
END GENERATE;

sync_stats_to_Host: FOR i IN 0 TO (STAT_REGISTERS-1) GENERATE

    STATS_DATA : entity signal_processing_common.sync_vector
        generic map (
            WIDTH => 32
        )
        Port Map ( 
            clock_a_rst => cmac_reset_combined,
            Clock_a     => i_clk_100GE,
            data_in     => stats_count(i),
            
            Clock_b     => i_clk_300,
            data_out    => stats_to_host_data_out(i)
        );  

END GENERATE;

target_count_int        <= stats_to_host_data_out(0);
o_target_count          <= target_count_int;

o_nontarget_count       <= stats_to_host_data_out(1);
o_CODIF_2154_count      <= stats_to_host_data_out(2);
o_PSR_PST_count         <= stats_to_host_data_out(3);
o_LFAA_spead_count      <= stats_to_host_data_out(4);

------------------------------------------------------------------------------
-- DEBUG LOGIC AND ILA
        
debug_packet_capture_in : IF g_DEBUG_ILA GENERATE

debug_ila_cmac_proc : process(i_clk_100GE)
begin
    if rising_edge(i_clk_100GE) then

        ptp_seconds <= rx_axis_tuser_int_d1(79 downto 32);
        ptp_sub_sec <= rx_axis_tuser_int_d1(31 downto 0);

    end if;
end process;


    cmac_in_ila : ila_0
    port map (
        clk                     => i_clk_100GE, 
        probe0(47 downto 0)     => rx_axis_tdata_int_d1(47 downto 0),
--        probe0(127 downto 48)   => rx_axis_tuser_int_d1,
        probe0(79 downto 48)    => ptp_sub_sec,
        probe0(127 downto 80)   => ptp_seconds,
        probe0(136 downto 128)  => (others => '0'),
        probe0(137)             => '0', 
        probe0(138)             => rx_axis_tlast_int_d1,
        probe0(139)             => rx_axis_tvalid_int_d1,
        probe0(153 downto 140)  => inc_packet_byte_count,
        probe0(191 downto 154)  => (others => '0')
    );
    

    hbm_out_ila : ila_0
    port map (
        clk                     => i_clk_300, 
        probe0(79 downto 0)     => meta_data_fifo_q,
        probe0(127 downto 80)   => data_to_hbm(47 downto 0),
        probe0(136 downto 128)  => (others => '0'),
        probe0(137)             => data_to_hbm_wr_int, 
        probe0(145 downto 138)  => std_logic_vector(words_to_send_to_hbm),
        --probe0(147 downto 146)  => (others => '0'),
        probe0(146)             => meta_data_fifo_empty,
        probe0(147)             => meta_data_fifo_rd,
        probe0(148)             => '0',
        probe0(149)             => '0',
        probe0(165 downto 150)  => target_count_int(15 downto 0),
        probe0(181 downto 166)  => i_rx_packets_to_capture(15 downto 0),
        probe0(191 downto 182)  => (others => '0')
    );

end generate;

end RTL;

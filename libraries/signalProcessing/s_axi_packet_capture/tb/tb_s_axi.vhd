----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: July 2022
-- Design Name: TB_s_axi
-- 
-- 
-- test bench written to be used in Vivado
-- not automated, and requires visual inspection to verify.
-- 
-- this is a static TB to test desired packet sizes and see if the byte detect will pick this up.
-- also to test if back to back packets, (ie tlast and the next cycle packet start) coming from the CMAC S_AXI will provoke a problem.
--
-- i_rx_axis_tuser time stamp data in 80 bit format coming from TimeSlave IP, this will be zero most of the time and provide the time stamp on first cycle of a packet.
--
library IEEE,technology_lib, PSR_Packetiser_lib, signal_processing_common, cmac_s_axi_lib, vd_lib, axi4_lib, common_lib, ethernet_lib;
library cnic_lib, vd_datagen_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ethernet_lib.ethernet_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;
library technology_lib, Timeslave_CMAC_lib;
USE technology_lib.tech_mac_100g_pkg.ALL;
USE axi4_lib.axi4_lite_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
USE common_lib.common_pkg.ALL;
USE Timeslave_CMAC_lib.timer_pkg.ALL;

entity tb_s_axi is
--  Port ( );
end tb_s_axi;

architecture Behavioral of tb_s_axi is

constant fname          : string := "";
-- assuming stim in base of repo for the moment.
constant init_fname     : string := "";
constant HBM_addr_width         : integer := 32;

signal HBM_axi_ar               : t_axi4_full_addr;                 -- read address bus : out t_axi4_full_addr (.valid, .addr(39:0), .len(7:0))
signal HBM_axi_arready          : std_logic;
signal HBM_axi_r                : t_axi4_full_data;                 -- r data bus : in t_axi4_full_data (.valid, .data(511:0), .last, .resp(1:0))
signal HBM_axi_rready           : std_logic;

signal init_mem     : std_logic     := '0';

signal clock_300 : std_logic := '0';    -- 3.33ns
signal clock_400 : std_logic := '0';    -- 2.50ns
signal clock_322 : std_logic := '0';    -- 3.11ns

signal testCount_400        : integer   := 0;
signal testCount_300        : integer   := 0;
signal testCount_322        : integer   := 0;

signal clock_400_rst        : std_logic := '1';
signal clock_300_rst        : std_logic := '1';
signal clock_322_rst        : std_logic := '1';

signal power_up_rst_clock_400   : std_logic_vector(31 downto 0) := c_ones_dword;
signal power_up_rst_clock_300   : std_logic_vector(31 downto 0) := c_ones_dword;
signal power_up_rst_clock_322   : std_logic_vector(31 downto 0) := c_ones_dword;

signal streaming_data           : std_logic_vector(511 downto 0);

signal ethernet_info            : ethernet_frame    := default_ethernet_frame;
signal ipv4_info                : IPv4_header       := default_ipv4_header;
signal udp_info                 : UDP_header        := default_UDP_header;

signal rx_packet_size           : std_logic_vector(13 downto 0);     -- Max size is 9000.
signal rx_reset_capture         : std_logic;
signal rx_reset_counter         : std_logic;

signal i_rx_axis_tdata          : STD_LOGIC_VECTOR ( 511 downto 0 );
signal i_rx_axis_tkeep          : STD_LOGIC_VECTOR ( 63 downto 0 );
signal i_rx_axis_tlast          : STD_LOGIC;
signal o_rx_axis_tready         : STD_LOGIC;
signal i_rx_axis_tuser          : STD_LOGIC_VECTOR ( 79 downto 0 );
signal i_rx_axis_tvalid         : STD_LOGIC;

signal o_tx_axis_tdata          : STD_LOGIC_VECTOR(511 downto 0);
signal o_tx_axis_tkeep          : STD_LOGIC_VECTOR(63 downto 0);
signal o_tx_axis_tvalid         : STD_LOGIC;
signal o_tx_axis_tlast          : STD_LOGIC;
signal o_tx_axis_tuser          : STD_LOGIC;
signal i_tx_axis_tready         : STD_LOGIC;
signal o_data_tx_sosi           : t_lbus_sosi;
signal i_data_tx_siso           : t_lbus_siso;

signal packet_player_rdy                    : std_logic;

-- HBM packets
signal hbm_packetiser_data_in_wr            : std_logic;
signal hbm_packetiser_data                  : std_logic_vector(511 downto 0);
signal hbm_packetiser_bytes_to_transmit     : std_logic_vector(13 downto 0);

-- first 100G interface packet player
signal packet_player_1_data_in_wr           : std_logic;
signal packet_player_1_data                 : std_logic_vector(511 downto 0);
signal packet_player_1_bytes_to_transmit    : std_logic_vector(13 downto 0);

-- VD ARGs interface
signal i_vd_lite_axi_mosi      : t_axi4_lite_mosi_arr(1 downto 0); 
signal o_vd_lite_axi_miso      : t_axi4_lite_miso_arr(1 downto 0);
signal i_vd_full_axi_mosi      : t_axi4_full_mosi_arr(1 downto 0);
signal o_vd_full_axi_miso      : t_axi4_full_miso_arr(1 downto 0);

signal debug                    : std_logic_vector(4 downto 0);

signal timer_time               : unsigned(31 downto 0);
signal timer_calc               : unsigned(31 downto 0);
signal timer_sec                : unsigned(47 downto 0);
signal ptp_time_emulation       : std_logic_vector(79 downto 0);
signal timer_pps                : std_logic;

signal real_time_1_vec          : std_logic_vector(3 downto 0);
signal real_time_2_vec          : std_logic_vector(3 downto 0);
signal real_time_3_vec          : std_logic_vector(3 downto 0);

signal real_time_ila            : t_slv_4_arr(14 downto 0);

signal rt_vec_int               : integer range 0 to 14;
signal rt_clk_div               : std_logic;

type timer_statemachine is (IDLE, PPS, SECOND, COMPLETE);
signal timer_sm : timer_statemachine;

signal i_timer_fields_in        : timer_fields_in;
signal o_timer_fields_out       : timer_fields_out;

signal tb_timer_fields_in       : timer_fields_in;
signal tb_timer_fields_out      : timer_fields_out;

signal schedule_action          : std_logic_vector(7 downto 0);

------------------------------------------------------------
-- CNIC NOISE GEN        
-- Indicates this module is currently writing data to the first 512 MBytes of the HBM
signal writing_buf0             : std_logic;
-- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
signal writing_buf1             : std_logic;
-- Pulse to start writing data (to whichever buffer we are currently up to)
signal start_noise_gen          : std_logic_vector(1 downto 0);
signal reset_noise_gen          : std_logic_vector(1 downto 0);

-- ar bus - read address
signal vd_axi_arvalid           : std_logic;
signal vd_axi_arready           : std_logic;
signal vd_axi_araddr            : std_logic_vector(31 downto 0);
signal vd_axi_arlen             : std_logic_vector(7 downto 0);
-- r bus - read data
signal vd_axi_rvalid            : std_logic;
signal vd_axi_rready            : std_logic;
signal vd_axi_rdata             : std_logic_vector(511 downto 0);
signal vd_axi_rlast             : std_logic;
signal vd_axi_rresp             : std_logic_vector(1 downto 0);

signal i_vdgen_full_axi_mosi    : t_axi4_full_mosi;
signal o_vdgen_full_axi_miso    : t_axi4_full_miso;
-- ARGs lite interface
-- Registers for top level control and monitoring.
signal i_vdgen_lite_axi_mosi    : t_axi4_lite_mosi; 
signal o_vdgen_lite_axi_miso    : t_axi4_lite_miso;

-- ar bus - read address
signal vd_hbm_2_axi_arvalid     : std_logic;
signal vd_hbm_2_axi_arready     : std_logic;
signal vd_hbm_2_axi_araddr      : std_logic_vector(31 downto 0);
signal vd_hbm_2_axi_arlen       : std_logic_vector(7 downto 0);
-- r bus - read data
signal vd_hbm_2_axi_rvalid      : std_logic;
signal vd_hbm_2_axi_rready      : std_logic;
signal vd_hbm_2_axi_rdata       : std_logic_vector(511 downto 0);
signal vd_hbm_2_axi_rlast       : std_logic;
signal vd_hbm_2_axi_rresp       : std_logic_vector(1 downto 0);

signal tally                    : std_logic_vector(31 downto 0);
signal reset_accum              : std_logic;
signal reset_combo              : std_logic;

begin

clock_300 <= not clock_300 after 1.67 ns;
clock_322 <= not clock_322 after 1.56 ns;
clock_400 <= not clock_400 after 1.25 ns;

--i_bytes_to_transmit <= std_logic_vector(to_unsigned(bytes_to_transmit,14));

-------------------------------------------------------------------------------------------------------------
-- powerup resets for SIM
test_runner_proc_clk300: process(clock_300)
begin
    if rising_edge(clock_300) then
        -- power up reset logic
        if power_up_rst_clock_300(31) = '1' then
            power_up_rst_clock_300(31 downto 0) <= power_up_rst_clock_300(30 downto 0) & '0';
            clock_300_rst   <= '1';
            testCount_300   <= 0;
        else
            clock_300_rst   <= '0';
            testCount_300   <= testCount_300 + 1;
        end if;
    end if;
end process;

test_runner_proc_clk400: process(clock_400)
begin
    if rising_edge(clock_400) then
        -- power up reset logic
        if power_up_rst_clock_400(31) = '1' then
            power_up_rst_clock_400(31 downto 0) <= power_up_rst_clock_400(30 downto 0) & '0';
            testCount_400   <= 0;
            clock_400_rst   <= '1';
        else
            testCount_400   <= testCount_400 + 1;
            clock_400_rst   <= '0';
        end if;
    end if;
end process;

test_runner_proc_clk322: process(clock_322)
begin
    if rising_edge(clock_322) then
        -- power up reset logic
        if power_up_rst_clock_322(31) = '1' then
            power_up_rst_clock_322(31 downto 0) <= power_up_rst_clock_322(30 downto 0) & '0';
            testCount_322   <= 0;
            clock_322_rst   <= '1';
        else
            testCount_322   <= testCount_322 + 1;
            clock_322_rst   <= '0';
        end if;
    end if;
end process;


-------------------------------------------------------------------------------------------------------------
run_proc : process(clock_322)
begin
    if rising_edge(clock_322) then
        if clock_322_rst = '1' then
            i_rx_axis_tvalid    <= '0';
            streaming_data      <= zero_512;
            i_rx_axis_tkeep     <= x"0000000000000000";
            i_rx_axis_tvalid    <= '0';
            i_rx_axis_tlast     <= '0';
            rx_reset_capture    <= '1';
            i_rx_axis_tuser     <= zero_word & zero_dword & zero_dword;
            
        else
            reset_accum                     <= '0';
            
                
            if testCount_322 = 80 then
                i_rx_axis_tuser                <= zero_word & one_dword & zero_dword;
                streaming_data(127 downto 0)   <=  ethernet_info.dst_mac & 
                                                ethernet_info.src_mac & 
                                                ethernet_info.eth_type & 
                                                ipv4_info.version & 
                                                ipv4_info.header_length & 
                                                ipv4_info.type_of_service; 
                streaming_data(255 downto 128) <=  ipv4_info.total_length & 
                                                ipv4_info.id & 
                                                ipv4_info.ip_flags & 
                                                ipv4_info.fragment_off & 
                                                ipv4_info.TTL & 
                                                ipv4_info.protocol & 
                                                ipv4_info.header_chk_sum & 
                                                ipv4_info.src_addr & 
                                                ipv4_info.dst_addr(31 downto 16);
                streaming_data(383 downto 256) <=  ipv4_info.dst_addr(15 downto 0) & 
                                                udp_info.src_port & 
                                                udp_info.dst_port & 
                                                udp_info.length & 
                                                udp_info.checksum & 
                                                x"AAAA" & 
                                                x"AAAAAAAA";
                streaming_data(511 downto 384) <=  x"AAAAAAAA" & 
                                                x"AAAAAAAA" & 
                                                x"AAAAAAAA" & 
                                                x"AAAAAAAA";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
                
            elsif testCount_322 = 81 then
                i_rx_axis_tuser                <= zero_word & zero_dword & zero_dword;
                streaming_data(127 downto 0)    <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                streaming_data(255 downto 128)  <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                streaming_data(383 downto 256)  <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                streaming_data(511 downto 384)  <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
            
            elsif testCount_322 = 82 then
                streaming_data(127 downto 0)    <= x"55555555" & x"66666666" & x"77777777" & x"88888888";
                streaming_data(255 downto 128)  <= x"55555555" & x"66666666" & x"77777777" & x"88888888";
                streaming_data(383 downto 256)  <= x"55555555" & x"66666666" & x"77777777" & x"88888888";
                streaming_data(511 downto 384)  <= x"55555555" & x"66666666" & x"77777777" & x"88888888";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
               
            elsif testCount_322 >= 83 AND testCount_322 < 113 then
                streaming_data(127 downto 0)    <= x"99999999" & x"AAAAAAAA" & x"BBBBBBBB" & x"CCCCCCCC";
                streaming_data(255 downto 128)  <= x"99999999" & x"AAAAAAAA" & x"BBBBBBBB" & x"CCCCCCCC";
                streaming_data(383 downto 256)  <= x"99999999" & x"AAAAAAAA" & x"BBBBBBBB" & x"CCCCCCCC";
                streaming_data(511 downto 384)  <= x"99999999" & x"AAAAAAAA" & x"BBBBBBBB" & x"CCCCCCCC";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
                
            elsif testCount_322 = 113 then
                streaming_data(127 downto 0)    <= x"DDDDDDDD" & x"EEEEEEEE" & x"FFFFFFFF" & x"00000000";
                streaming_data(255 downto 128)  <= x"DDDDDDDD" & x"EEEEEEEE" & x"FFFFFFFF" & x"00000000";
                streaming_data(383 downto 256)  <= x"DDDDDDDD" & x"EEEEEEEE" & x"FFFFFFFF" & x"00000000";
                streaming_data(511 downto 384)  <= zero_128;
                i_rx_axis_tkeep                 <= x"000003FFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
                i_rx_axis_tlast                 <= '1'; 
            -- PTP injection
            elsif testCount_322 = 121 or testCount_322 = 70 then
                i_rx_axis_tuser                <= one_word & one_dword & zero_dword;
                streaming_data(127 downto 0)   <=  ethernet_info.dst_mac & 
                                                ethernet_info.src_mac & 
                                                ethernet_info.eth_type & 
                                                ipv4_info.version & 
                                                ipv4_info.header_length & 
                                                ipv4_info.type_of_service; 
                streaming_data(255 downto 128) <=  ipv4_info.total_length & 
                                                ipv4_info.id & 
                                                ipv4_info.ip_flags & 
                                                ipv4_info.fragment_off & 
                                                ipv4_info.TTL & 
                                                ipv4_info.protocol & 
                                                ipv4_info.header_chk_sum & 
                                                ipv4_info.src_addr & 
                                                ipv4_info.dst_addr(31 downto 16);
                streaming_data(383 downto 256) <=  ipv4_info.dst_addr(15 downto 0) & 
                                                udp_info.src_port & 
                                                udp_info.dst_port & 
                                                udp_info.length & 
                                                udp_info.checksum & 
                                                x"AAAA" & 
                                                x"AAAAAAAA";
                streaming_data(511 downto 384) <=  x"AAAAAAAA" & 
                                                x"AAAAAAAA" & 
                                                x"AAAAAAAA" & 
                                                x"AAAAAAAA";
                i_rx_axis_tkeep                 <= x"00FFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';  
                i_rx_axis_tlast                 <= '1';
                
            elsif testCount_322 = 300 then
                streaming_data(127 downto 0)   <=  ethernet_info.dst_mac & 
                                                ethernet_info.src_mac & 
                                                ethernet_info.eth_type & 
                                                ipv4_info.version & 
                                                ipv4_info.header_length & 
                                                ipv4_info.type_of_service; 
                streaming_data(255 downto 128) <=  ipv4_info.total_length & 
                                                ipv4_info.id & 
                                                ipv4_info.ip_flags & 
                                                ipv4_info.fragment_off & 
                                                ipv4_info.TTL & 
                                                ipv4_info.protocol & 
                                                ipv4_info.header_chk_sum & 
                                                ipv4_info.src_addr & 
                                                ipv4_info.dst_addr(31 downto 16);
                streaming_data(383 downto 256) <=  ipv4_info.dst_addr(15 downto 0) & 
                                                udp_info.src_port & 
                                                udp_info.dst_port & 
                                                udp_info.length & 
                                                udp_info.checksum & 
                                                x"AAAA" & 
                                                x"AAAAAAAA";
                streaming_data(511 downto 384) <=  x"AAAAAAAA" & 
                                                x"AAAAAAAA" & 
                                                x"AAAAAAAA" & 
                                                x"AAAAAAAA";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
                
                reset_accum                     <= '1';
                
            elsif testCount_322 = 301 then
                streaming_data(127 downto 0)    <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                streaming_data(255 downto 128)  <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                streaming_data(383 downto 256)  <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                streaming_data(511 downto 384)  <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
            
            elsif testCount_322 = 302 then
                streaming_data(127 downto 0)    <= x"55555555" & x"66666666" & x"77777777" & x"88888888";
                streaming_data(255 downto 128)  <= x"55555555" & x"66666666" & x"77777777" & x"88888888";
                streaming_data(383 downto 256)  <= x"55555555" & x"66666666" & x"77777777" & x"88888888";
                streaming_data(511 downto 384)  <= x"55555555" & x"66666666" & x"77777777" & x"88888888";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
               
            elsif testCount_322 = 303 then
                streaming_data(127 downto 0)    <= x"99999999" & x"AAAAAAAA" & x"BBBBBBBB" & x"CCCCCCCC";
                streaming_data(255 downto 128)  <= x"99999999" & x"AAAAAAAA" & x"BBBBBBBB" & x"CCCCCCCC";
                streaming_data(383 downto 256)  <= x"99999999" & x"AAAAAAAA" & x"BBBBBBBB" & x"CCCCCCCC";
                streaming_data(511 downto 384)  <= x"99999999" & x"AAAAAAAA" & x"BBBBBBBB" & x"CCCCCCCC";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
                i_rx_axis_tlast                 <= '1';
                
            elsif testCount_322 = 304 then
                streaming_data(127 downto 0)    <= x"DDDDDDDD" & x"EEEEEEEE" & x"FFFFFFFF" & x"00000000";
                streaming_data(255 downto 128)  <= x"DDDDDDDD" & x"EEEEEEEE" & x"FFFFFFFF" & x"00000000";
                streaming_data(383 downto 256)  <= x"DDDDDDDD" & x"EEEEEEEE" & x"FFFFFFFF" & x"00000000";
                streaming_data(511 downto 384)  <= zero_128;
                i_rx_axis_tkeep                 <= x"0000000000000000";
                i_rx_axis_tvalid                <= '0';
                i_rx_axis_tlast                 <= '0';                 
            else
                streaming_data                  <= zero_512;
                i_rx_axis_tkeep                 <= x"0000000000000000";
                i_rx_axis_tvalid                <= '0';
                i_rx_axis_tlast                 <= '0';
            end if;

            if testCount_322 = 38 then
                rx_reset_capture    <= '1';
                rx_packet_size      <= "00" & x"86A";      -- 2154    

            elsif testCount_322 = 220 then
                rx_reset_capture    <= '1';
                rx_packet_size      <= "00" & x"100";      -- 256
            else
                rx_reset_capture    <= '0';
            end if;
        end if;

    end if;
end process;

i_rx_axis_tdata        <= streaming_data;

rx_reset_counter        <= '0';

-------------------------------------------------------------------------------------------------------------

DUT : entity cmac_s_axi_lib.s_axi_packet_capture 
    Port map ( 
        --------------------------------------------------------
        -- 100G 
        i_clk_100GE             => clock_322,
        i_eth100G_locked        => '1',
        
        i_clk_300               => clock_300,
        i_clk_300_rst           => clock_300_rst,
        
        i_enable_capture        => '0',
        i_rx_packet_size        => rx_packet_size,
        i_rx_reset_capture      => rx_reset_capture,
        i_reset_counter         => rx_reset_counter,
        i_rx_packet_size_abs    => '0',
        
        o_target_count          => open,
        o_nontarget_count       => open,
        
        i_rx_packets_to_capture => x"00000080",

        -- 100G RX S_AXI interface ~322 MHz
        i_rx_axis_tdata         => i_rx_axis_tdata,
        i_rx_axis_tkeep         => i_rx_axis_tkeep,
        i_rx_axis_tlast         => i_rx_axis_tlast,
        o_rx_axis_tready        => o_rx_axis_tready,
        i_rx_axis_tuser         => i_rx_axis_tuser,
        i_rx_axis_tvalid        => i_rx_axis_tvalid,
        
        -- Data to HBM writer - 300 MHz
        o_data_to_hbm           => open,
        o_data_to_hbm_wr        => open
    
    );

-------------------------------------------------------------------------------------------------------------

DUT_2 : entity vd_lib.virtual_digitiser
    port map ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                   => clock_300,
        i_rst                   => clock_300_rst,

        i_local_reset           => '0',

        -- data to packet_player
        o_bytes_to_transmit     => packet_player_1_bytes_to_transmit,
        o_data_to_player        => packet_player_1_data,
        o_data_to_player_wr     => packet_player_1_data_in_wr,
        i_data_to_player_rdy    => packet_player_rdy,

        -- Timing signals
        o_timer_fields_in       => i_timer_fields_in,
        i_timer_fields_out      => o_timer_fields_out,
        
        -- mux select for data path
        o_vd_enable             => open,

        -- PTP scheduler vector
        i_schedule_action       => schedule_action,
        
        -- debug vector
        i_debug                 => debug,

        ------------------------------------------------------------
        -- CNIC NOISE GEN        
        -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
        i_writing_buf0(0)       => writing_buf0,
        i_writing_buf0(1)       => '0',
        -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
        i_writing_buf1(0)       => writing_buf1,
        i_writing_buf1(1)       => '0',
        -- Pulse to start writing data (to whichever buffer we are currently up to)
        o_start_noise_gen       => start_noise_gen,

        o_reset_noise_gen       => reset_noise_gen,

        -- ar bus - read address
        vd_axi_arvalid          => vd_axi_arvalid,
        vd_axi_arready          => vd_axi_arready,
        vd_axi_araddr           => vd_axi_araddr,
        vd_axi_arlen            => vd_axi_arlen,
        -- r bus - read data
        vd_axi_rvalid           => vd_axi_rvalid,
        vd_axi_rready           => vd_axi_rready,
        vd_axi_rdata            => vd_axi_rdata,
        vd_axi_rlast            => vd_axi_rlast,
        vd_axi_rresp            => vd_axi_rresp,
        
        -- ar bus - read address
        vd_hbm_2_axi_arvalid    => vd_hbm_2_axi_arvalid,
        vd_hbm_2_axi_arready    => vd_hbm_2_axi_arready,
        vd_hbm_2_axi_araddr     => vd_hbm_2_axi_araddr,
        vd_hbm_2_axi_arlen      => vd_hbm_2_axi_arlen,
        -- r bus - read data
        vd_hbm_2_axi_rvalid     => vd_hbm_2_axi_rvalid,
        vd_hbm_2_axi_rready     => vd_hbm_2_axi_rready,
        vd_hbm_2_axi_rdata      => vd_hbm_2_axi_rdata,
        vd_hbm_2_axi_rlast      => vd_hbm_2_axi_rlast,
        vd_hbm_2_axi_rresp      => vd_hbm_2_axi_rresp,

        -- VD ARGs interface
        i_vd_lite_axi_mosi      => i_vd_lite_axi_mosi,
        o_vd_lite_axi_miso      => o_vd_lite_axi_miso,
        i_vd_full_axi_mosi      => i_vd_full_axi_mosi,
        o_vd_full_axi_miso      => o_vd_full_axi_miso

    ); 
    
--     i_vd_data_grn : entity vd_datagen_lib.dataGen_top
--     generic map (
--         -- Number of data streams to generate as a multiple of 128
--         -- Min = 1, max = 8, use less to reduce build times.
--         g_N128_SOURCE_INSTANCES     => 8 
--     )
--     port map (
--         clk                         => clk,
--         reset                       => reset,
--         -- ARGs full interface
--         -- Addresses two ultraRAMs for storage of delay polynomials 
--         -- other signal generation controls
--         i_vdgen_lite_axi_mosi       => i_vd_gen_lite_axi_mosi,
--         o_vdgen_lite_axi_miso       => o_vd_gen_lite_axi_miso,
--         -- ARGs lite interface
--         -- Registers for top level control and monitoring.
--         i_vdgen_full_axi_mosi       => i_vd_gen_full_axi_mosi,
--         o_vdgen_full_axi_miso       => o_vd_gen_full_axi_miso,
--         ----------------------------------------------------
--         -- Control signals to the rest of CNIC
--         -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
--         o_writing_buf0              => writing_buf0,
--         -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
--         o_writing_buf1              => writing_buf1,
--         -- Pulse to start writing data (to whichever buffer we are currently up to)
--         i_start                     => start_vd_gen,
--         ----------------------------------------------------
--         -- HBM interface, Uses 1 Gbyte of HBM. 
--         -- aw bus
--         m01_axi_awvalid             => vd_m03_axi_awvalid,
--         m01_axi_awready             => m03_axi_awready,
--         m01_axi_awaddr              => vd_m03_axi_awaddr,
--         m01_axi_awlen               => vd_m03_axi_awlen,
--         -- w bus - write data
--         m01_axi_wvalid              => vd_m03_axi_wvalid,
--         m01_axi_wready              => m03_axi_wready,
--         m01_axi_wdata               => vd_m03_axi_wdata,
--         m01_axi_wlast               => vd_m03_axi_wlast,
--         -- b bus - write response
--         m01_axi_bvalid              => m03_axi_bvalid,
--         m01_axi_bresp               => m03_axi_bresp
--     );



    HBM_interface : entity cnic_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => HBM_addr_width, 
        AXI_ID_WIDTH => 1, 
        AXI_DATA_WIDTH => 512, 
        READ_QUEUE_SIZE => 16, 
        MIN_LAG => 60,    
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 43526,             -- natural := 12345;
        LATENCY_LOW_PROBABILITY => 60, -- natural := 95;  -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 60 -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
    ) Port map (
        i_clk          => clock_300,
        i_rst_n        => not clock_300_rst, 
        -- WR
        axi_awaddr     => (others => '0'),
        axi_awid       => "0", 
        axi_awlen      => (others => '0'),
        axi_awsize     => "110",
        axi_awburst    => "01", 
        axi_awlock     => "00",  
        axi_awcache    => "0011", 
        axi_awprot     => "000",
        axi_awqos      => "0000", 
        axi_awregion   => "0000",
        axi_awvalid    => '0', 
        axi_awready    => open, 
        axi_wdata      => (others => '0'), 
        axi_wstrb      => (others => '0'),
        axi_wlast      => '0',
        axi_wvalid     => '0',
        axi_wready     => open,
        axi_bresp      => open,
        axi_bvalid     => open,
        axi_bready     => '1', 
        axi_bid        => open,
        -- RD 
        axi_araddr     => HBM_axi_ar.addr(HBM_addr_width-1 downto 0),
        axi_arlen      => HBM_axi_ar.len,
        axi_arsize     => "110",   -- 6 = 64 bytes per beat = 512 bit wide bus. -- out std_logic_vector(2 downto 0);
        axi_arburst    => "01",    -- "01" = incrementing address for each beat in the burst. -- out std_logic_vector(1 downto 0);
        axi_arlock     => "00",
        axi_arcache    => "0011",
        axi_arprot     => "000",
        axi_arvalid    => HBM_axi_ar.valid,
        axi_arready    => HBM_axi_arready,
        axi_arqos      => "0000",
        axi_arid       => "0",
	    axi_arregion   => "0000",
        axi_rdata      => HBM_axi_r.data,
        axi_rresp      => HBM_axi_r.resp,
        axi_rlast      => HBM_axi_r.last,
        axi_rvalid     => HBM_axi_r.valid,
        axi_rready     => HBM_axi_rready,

        -- control dump to disk.
        i_write_to_disk         => '0',
        i_write_to_disk_addr    => 0,
        i_write_to_disk_size    => 0,
        i_fname                 => fname,
        -- Initialisation of the memory
        -- The memory is loaded with the contents of the file i_init_fname in 
        -- any clock cycle where i_init_mem is high.
        i_init_mem              => init_mem,
        i_init_fname            => init_fname
    );
    
-------------------------------------------------------------------------------------------------------------


    i_tx_axis_tready <= NOT clock_322_rst;

DUT_3 : entity PSR_Packetiser_lib.packet_player 
    generic map (
        LBUS_TO_CMAC_INUSE      => FALSE,      -- FUTURE WORK to IMPLEMENT AXI
        PLAYER_CDC_FIFO_DEPTH   => 512        
        -- FIFO is 512 Wide, 9KB packets = 73728 bits, 512 * 256 = 131072, 256 depth allows ~1.88 9K packets, we are target packets sizes smaller than this.
    )
    port map ( 
        i_clk                   => clock_300, 
        i_clk_reset             => clock_300_rst,
    
        i_cmac_clk              => clock_322,
        i_cmac_clk_rst          => clock_322_rst,
        
        i_bytes_to_transmit     => packet_player_1_bytes_to_transmit,
        i_data_to_player        => packet_player_1_data,
        i_data_to_player_wr     => packet_player_1_data_in_wr,
        o_data_to_player_rdy    => packet_player_rdy,
        
        o_cmac_ready            => open,
               
        -- streaming AXI to CMAC
        o_tx_axis_tdata         => o_tx_axis_tdata,
        o_tx_axis_tkeep         => o_tx_axis_tkeep,
        o_tx_axis_tvalid        => o_tx_axis_tvalid,
        o_tx_axis_tlast         => o_tx_axis_tlast,
        o_tx_axis_tuser         => o_tx_axis_tuser,
        i_tx_axis_tready        => i_tx_axis_tready,
    
        -- LBUS to CMAC
        o_data_to_transmit      => o_data_tx_sosi,
        i_data_to_transmit_ctl  => i_data_tx_siso
    );

-------------------------------------------------------------------------------------------------------------
-- real_time_ila setup with figures from ILA

real_time_ila(0) <= 4D"3";
real_time_ila(1) <= 4D"10";
real_time_ila(2) <= 4D"10";
real_time_ila(3) <= 4D"10";
real_time_ila(4) <= 4D"10";
real_time_ila(5) <= 4D"6";
real_time_ila(6) <= 4D"10";
real_time_ila(7) <= 4D"10";
real_time_ila(8) <= 4D"10";
real_time_ila(9) <= 4D"10";
real_time_ila(10) <= 4D"7";
real_time_ila(11) <= 4D"10";
real_time_ila(12) <= 4D"10";
real_time_ila(13) <= 4D"10";
real_time_ila(14) <= 4D"10";

run_300_proc : process(clock_300)
begin
    if rising_edge(clock_300) then
        if clock_300_rst = '1' then
            timer_calc(31 downto 0)     <= 32D"999980000";
            timer_time(31 downto 0)     <= 32D"999980000";
            timer_sec(47 downto 0)      <= x"0000" & x"FFFFFFFF";
            real_time_1_vec             <= x"3";
            real_time_2_vec             <= x"3";
            real_time_3_vec             <= x"4";
            schedule_action             <= x"00";
            tb_timer_fields_in.timer_on <= '0';
            debug                       <= ( others => '0' );
            rt_vec_int                  <= 0;
            rt_clk_div                  <= '0';
            timer_pps                   <= '0';
            timer_sm                    <= PPS;
        else
            -- fake 3.33ns count
            real_time_1_vec     <= real_time_3_vec;
            real_time_2_vec     <= real_time_1_vec;
            real_time_3_vec     <= real_time_2_vec;
            
            rt_clk_div  <= NOT rt_clk_div;
            
            -- clock updated every two cycles.
            if rt_clk_div = '1' then
                if rt_vec_int = 14 then
                    rt_vec_int <= 0;
                else
                    rt_vec_int <= rt_vec_int + 1;
                end if;
                       
                timer_calc <= timer_time + unsigned(real_time_ila(rt_vec_int));       
                       
            end if;
                   
            if timer_calc > 999999999 then
                timer_time <= timer_calc - 1000000000;
            else
                timer_time <= timer_calc;               
            end if;

                
            case timer_sm is
                when IDLE =>
                    if timer_time < 100 then
                        timer_sm    <= PPS;
                    end if;
                    
                when PPS =>
                    if timer_time > 999999900 then
                        timer_pps   <= '1';
                        timer_sm    <= SECOND;
                    end if;
                    
                when SECOND =>
                    if timer_time > 999999920 then
                        timer_sec   <= timer_sec + 1;
                        timer_sm    <= COMPLETE;
                    end if;
                
                when COMPLETE => 
                    if timer_time > 999999950 then
                        timer_pps   <= '0';
                        timer_sm    <= IDLE;
                    end if;
            
            end case;            
            
                       
            if testcount_300 = 10 then
                debug(2)            <= '1';     -- packetiser_enable
                debug(3)            <= '1';     -- VD_enable
                debug(4)            <= '1';     -- PTP enable
            end if;

            if testcount_300 = 2998 then
                schedule_action     <= x"02";   -- start PTP TX
            end if;

            tb_timer_fields_in.timer_goal_in_ns     <= 32D"9900";
            tb_timer_fields_in.timer_on             <= '1';
            
        end if;
    end if;
end process;

ptp_time_emulation  <= std_logic_vector(timer_sec) & std_logic_vector(timer_time);
    


DUT_4 : entity Timeslave_CMAC_lib.sub_second_timer 
    port map ( 
        i_clk               => clock_300,
        i_reset             => clock_300_rst,
        
        i_ptp_time          => ptp_time_emulation,
        i_pps               => timer_pps,

        i_timer_fields_in   => tb_timer_fields_in,
        o_timer_fields_out  => tb_timer_fields_out
        
    );

-------------------------------------------------------------------------------------------------------------

--DUT_5 : entity ethernet_lib.cmac_100g_bw_mon
--    Port map ( 
--        i_clk               => clock_300,
--        i_clk_reset         => clock_300_rst,
    
--        -- Data in from the 100GE MAC
--        i_100GE_clk         => clock_322,
--        i_100GE_rst         => clock_322_rst,

--        i_axis_tdata        => i_rx_axis_tdata,
--        i_axis_tkeep        => i_rx_axis_tkeep,
--        i_axis_tlast        => i_rx_axis_tlast,                  
--        i_axis_tuser        => i_rx_axis_tuser,
--        i_axis_tvalid       => i_rx_axis_tvalid,

--        o_bw_sec            => open,
--        o_bw_min            => open

--    );

-------------------------------------------------------------------------------------------------------------

reset_combo     <= reset_accum OR clock_322_rst;

--DUT_6_accum : entity Timeslave_CMAC_lib.CMAC_stats_accum
--        Port map ( 
--            i_clk           => clock_322,
--            i_reset         => reset_combo,

--            i_cmac_signal   => streaming_data(373 downto 371),
--            o_tally         => tally
--        );





















-------------------------------------------------------------------------------------------------------------



end Behavioral;

# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Verify CNIC Virtual Digitiser Data Generation is consistent."""
import json
import os
import sys
from datetime import datetime
from time import sleep

import matplotlib.pyplot as plt
import numpy as np
import pytest
from crc import Calculator, Crc8
from ska_low_cbf_integration.sps_analyser import MAX_NUMBER_SPS_CHANNELS, SPSAnalyser
from ska_low_cbf_sw_cnic import (
    NULL_SOURCE,
    TIME_STR_FORMAT,
    Source,
    StreamConfig,
    VDChannelConfig,
    frequency_from_id,
)
from ska_low_cbf_sw_cnic.icl.vd_datagen import SOURCES_PER_STREAM

from .test_cnic import (
    SPS_HEADER_SIZE_VERSION,
    debug_register_dump,
    rx_wait_success,
    this_test_filename,
)


@pytest.mark.parametrize("start_mode", ("ptp", "immediate"))
@pytest.mark.parametrize(
    "stations",
    [
        pytest.param({345, 350, 352, 355, 431, 434}, id="576streams"),
        pytest.param(set(range(18)), id="1728streams"),
    ],
)
@pytest.mark.parametrize("direction", ("forward", "reverse"))
def test_vd_consistency(cnics, stations, direction, start_mode):
    """Verify Virtual Digitiser data is consistent."""
    sps_packet_version = 3
    freq_channels = list(range(100, 196))
    spead_header_length = SPS_HEADER_SIZE_VERSION[sps_packet_version]
    packet_length = 8234 + spead_header_length

    if direction == "forward":
        vd_cnic = cnics[1]
        rx_cnic = cnics[0]
    if direction == "reverse":
        vd_cnic = cnics[0]
        rx_cnic = cnics[1]

    if not bool(vd_cnic.system.datagen_1_capable):
        pytest.skip("No Virtual Digitiser Capability")

    print("")
    print("VD CNIC:")
    print(f"\t{vd_cnic.fw_version.value}")
    print(f'\t{vd_cnic.info["platform"]["static_region"]["vbnv"]}')
    print("Rx CNIC:")
    print(f"\t{rx_cnic.fw_version.value}")
    print(f'\t{rx_cnic.info["platform"]["static_region"]["vbnv"]}')
    rx_filename = this_test_filename("test_results", "pcap")
    log_filename = ".".join(rx_filename.split(".")[:-1] + ["log"])
    print(f"Rx filename: '{rx_filename}'")
    print(f"Log filename: '{log_filename}'")

    for cnic in cnics:
        cnic.reset()

    streams = [
        StreamConfig(
            spead_stream=VDChannelConfig(
                scan=1234,
                beam=1,
                frequency=freq,
                substation=1,
                subarray=1,
                station=stn,
            ),
            sources=[
                Source(
                    tone=False,
                    channel_frequency=frequency_from_id(freq) / 1e9,
                    scale=4000,
                    seed=freq,
                    delay_polynomial=[10] + [0] * 5,
                )
            ]
            + [NULL_SOURCE] * 3
            + [
                Source(
                    tone=False,
                    channel_frequency=0.05,
                    scale=4000,
                    seed=stn,
                    delay_polynomial=[10] + [0] * 5,
                )
            ]
            + [NULL_SOURCE] * 3,
        )
        for freq in freq_channels
        for stn in stations
    ]
    n_streams = len(streams)
    print(f"Channel configuration created with {len(streams)} entries")
    rx_cnic.receive_pcap(rx_filename, packet_length, n_packets=n_streams * 5)

    vd_cnic.configure_vd(
        streams,
        sps_packet_version=sps_packet_version,
        ska_time=0,
    )

    if start_mode == "ptp":
        start_time_unix = vd_cnic.timeslave.unix_timestamp + 4
        start_time = datetime.fromtimestamp(start_time_unix)
        start_time_str = start_time.strftime(TIME_STR_FORMAT)
        print(f"Starting Virtual Digitiser at {start_time_str}")
        vd_cnic.vd.use_ptp_to_begin = True
        vd_cnic.timeslave.tx_start_time = start_time_str
        vd_cnic.enable_vd = True
        vd_cnic.vd_datagen.enable_vd = True
        vd_cnic.vd_datagen_2.enable_vd = True

    if start_mode == "immediate":
        # dummy sleep why not
        sleep(4)

        # Tango lands do this with delay subscription:
        #      self.fpga.configure_next_delay_polynomials(
        #             self._next_polys, datagen_use_time, data_start_sec, data_start_ns
        #         )
        # - we could do the same if I knew what numbers to inject...

        vd_cnic.vd_datagen.buf0_valid = True
        if n_streams > int(vd_cnic.vd_datagen.vd_gen_no_of_chans) * SOURCES_PER_STREAM:
            if not bool(vd_cnic.system.datagen_2_capable):
                pytest.fail("No Virtual Digitiser 2 Capability!")
            vd_cnic.vd_datagen_2.buf0_valid = True

        # reset DataGenerator back to T=0
        vd_cnic.vd.reset_vd_data_gen_logic = 1
        vd_cnic.vd.reset_vd_data_gen_logic = 0

        vd_cnic.enable_vd = True

    if start_mode == "ptp":
        # allowing 1 extra second for VD to do its business
        while vd_cnic.timeslave.unix_timestamp < (start_time_unix + 1):
            sleep(1)

    if not rx_wait_success(rx_cnic):
        debug_register_dump(vd_cnic)

    print("Stopping Virtual Digitiser")
    cnic.reset()

    consistent, analyser = check_consistent(
        freq_channels, log_filename, rx_filename, stations
    )
    assert consistent, "SPS data mismatch between stations"
    assert stations == analyser.stations, "Stations not as configured"
    assert freq_channels == analyser.coarse_channels, "Channels not as configured"
    assert n_streams == analyser.channels, "Number of streams not as configured"


def check_consistent(freq_channels, log_filename, rx_filename, stations):
    # SPS Data Analysis
    with open(log_filename, "w", encoding="utf-8") as logfile:
        sys.stdout = logfile  # SPSAnalyser prints too much
        try:
            analyser = SPSAnalyser(
                json.dumps(
                    {
                        "coarse_channels": freq_channels,
                        "stations": list(stations),  # set is not JSON-able
                    }
                )
            )
            analyser.extract_sps_data(rx_filename)
        except Exception as exc:
            print(exc)  # will go to log
            print(exc, file=sys.__stderr__)
            analyser = None
        finally:
            sys.stdout = sys.__stdout__
        assert analyser is not None, "SPSAnalyser could not extract SPS data"
        print("The minimum timestamp is", min(analyser.timestamps), file=logfile)

        def chan_idx(station, channel):
            return analyser.channel_lookup.index(
                MAX_NUMBER_SPS_CHANNELS * (station - 1) + channel
            )

        match = {}
        stations = list(stations)
        for station in stations[1:]:
            for channel in freq_channels:
                # only compare pol. 0, because that's the one that should match
                # for each station
                match[(station, channel)] = np.all(
                    analyser.samples[chan_idx(stations[0], channel), 0, :]
                    == analyser.samples[chan_idx(station, channel), 0, :]
                )
        print(
            f"Does the data for all stations match station {stations[0]}?", file=logfile
        )
        if all(match.values()):
            print("Yes", file=logfile)
        else:
            print("No", file=logfile)
            print("", file=logfile)
            print("Mismatches at:", file=logfile)
            for k, v in match.items():
                if not v:
                    print(f"Station {k[0]:3d} Channel {k[1]:3d}", file=logfile)
            print("", file=logfile)

        plot_base_name = os.path.join(
            "test_results", f"{len(stations)}_stations_{len(freq_channels)}_channels"
        )
        plot_sps_data(
            analyser,
            n_pol=0,
            filename=f"{plot_base_name}_x.png",
        )
        plot_sps_data(
            analyser,
            n_pol=2,
            filename=f"{plot_base_name}_y.png",
        )
        return all(match.values()), analyser


def plot_sps_data(
    analyser: SPSAnalyser, n_plots: int = 3, n_pol: int = 0, filename="plot.png"
) -> None:
    """
    Plot SPS Data (Hashed via CRC8).

    :param analyser: SPSAnalyser
    :param n_plots: number of time steps to plot
    :param n_pol: 0 = X pol. real, 2 = Y pol. real
    :param filename: where to save file
    """
    crc_calculator = Calculator(Crc8.CCITT)

    def chan_idx(station, channel):
        return analyser.channel_lookup.index(
            MAX_NUMBER_SPS_CHANNELS * (station - 1) + channel
        )

    for n in range(n_plots):
        plt.subplot(n_plots, 1, n + 1)
        plt.suptitle(f"Hashed {'X' if n_pol < 2 else 'Y'}-Polarisation Values")
        plt.title(f"Time Step {n}")
        t_samples = analyser.samples[:, n_pol, n * 2048 : (n + 1) * 2048]
        plot_data = np.zeros(
            (len(analyser.stations), len(analyser.coarse_channels)), dtype=np.uint8
        )
        for n_station, station in enumerate(analyser.stations):
            for n_channel, channel in enumerate(analyser.coarse_channels):
                # hash a few bytes of data to get a unique-ish value to plot
                stn_chan_samples = t_samples[chan_idx(station, channel)][:8].tobytes()
                crc = crc_calculator.checksum(stn_chan_samples)
                plot_data[n_station, n_channel] = crc
        plt.imshow(plot_data, cmap="magma")
    plt.ylabel("Station")
    plt.xlabel("Channel")
    plt.tight_layout()
    plt.savefig(filename, dpi=450)
    plt.close()

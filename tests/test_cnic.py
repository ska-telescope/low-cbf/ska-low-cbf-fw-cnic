# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Verify CNIC operation."""
import datetime
import os
import time
from enum import Enum
from time import sleep

import numpy as np
import pytest
from dpkt.pcap import UniversalReader
from ska_low_cbf_integration.compare_pcap import same_packets
from ska_low_cbf_sw_cnic.util.pcap import count_packets_in_pcap

# Pick 3 or 5 GB file, depending on type of card under test
# (at present we do not test U50 & U55C together)
# I cannot figure out how to share this with conftest.py ...
BIG_PCAP_NAME = "five_gb.pcap"
target_alveo = os.environ.get("TARGET_ALVEO", "no-target-alveo").lower()
# if "u50" in target_alveo:
BIG_PCAP_NAME = "three_gb.pcap"
DUPLEX_PCAP_NAME = "three_gb.pcap"

SPS_HEADER_SIZE_VERSION = {2: 72, 3: 56}
"""SPS SPEAD packet header size by protocol version"""


# TODO - refactor integration code to expose a shared "this_test_filename" function?
def this_test_filename(base_path: str = ".", extension: str = "log", suffix: str = ""):
    """
    Create a per-CI-test-scenario filename.

    "<short commit hash> <timestamp> <test name>[suffix].<extension>"
    """
    current_test = os.environ.get("PYTEST_CURRENT_TEST", "no-pytest-test")
    current_test = current_test.removesuffix("(call)").strip()
    commit = os.environ.get("CI_COMMIT_SHORT_SHA", "")
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    filename = sanitise_filename(
        f"{commit}{timestamp}{current_test}{suffix}.{extension}"
    )
    return os.path.join(base_path, filename)


def sanitise_filename(raw: str, substitute="-") -> str:
    """
    Make a string safe(r) to use as a filename.

    :param raw: Desired filename, potentially containing invalid characters.
    :param substitute: Substituted in place of bad characters.
    """
    allowed_extras = [" ", "_", "-", ":", ".", "[", "]"]
    sanitised = ""
    for char in raw:
        if char.isalnum() or char in allowed_extras:
            sanitised += char
        else:
            sanitised += substitute
    return sanitised


def pcap_data_rate(filename) -> float:
    """
    Calculate average data rate of a PCAP file.

    :returns: Gbps
    """
    with open(filename, "rb") as file:
        reader = UniversalReader(file)
        first_ts = None
        last_ts = None
        total_bytes = 0
        for n, (ts, packet) in enumerate(reader):
            if n == 0:
                first_ts = ts
            else:
                last_ts = ts
            total_bytes += len(packet)
    return float(((total_bytes * 8) / (last_ts - first_ts)) / 1_000_000_000)


@pytest.mark.parametrize("direction", ("forward", "reverse"))
@pytest.mark.parametrize("tx_rate", [50, 100, "pcap"])
@pytest.mark.parametrize("in_filename", ["tests/10_multi_length.pcap", BIG_PCAP_NAME])
def test_simplex(cnics, in_filename, tx_rate, direction):
    """Verify simplex operation."""
    for cnic in cnics:
        cnic.hbm_pktcontroller.duplex = False

    if direction == "forward":
        rx_cnic = cnics[0]
        tx_cnic = cnics[1]
    if direction == "reverse":
        rx_cnic = cnics[1]
        tx_cnic = cnics[0]

    print(
        f'{tx_cnic.info["platform"]["static_region"]["vbnv"]}',
        "->",
        f'{rx_cnic.info["platform"]["static_region"]["vbnv"]}',
    )

    n_packets = count_packets_in_pcap(in_filename)
    rx_filename = this_test_filename("test_results", "pcap")
    rx_cnic.receive_pcap(rx_filename, 128, n_packets=n_packets)
    tx_cnic.transmit_pcap(in_filename, rate=tx_rate)
    while not rx_cnic.finished_receive:
        sleep(2)

    assert rx_cnic.hbm_pktcontroller.rx_packet_count == n_packets
    assert tx_cnic.hbm_pktcontroller.tx_packet_count == n_packets
    assert same_packets(in_filename, rx_filename)

    # check data rate if numeric rate specified and large file in use
    if n_packets > 10_000 and isinstance(tx_rate, (int, float)):
        measured_rate = pcap_data_rate(rx_filename)
        print(f"Measured data rate: {measured_rate:.3f} Gbps")
        rate_ratio = measured_rate / tx_rate  # 1 == perfectly as requested
        assert 0.9 <= rate_ratio <= 1.1

    if tx_rate == "pcap":
        t_offsets = timestamps(in_filename) - timestamps(rx_filename)

        # Make sure that all packets arrived at about the same time as specified in the
        # source PCAP file.
        # Allow 100 ns variation (note: using 128 bit floats, how accurate are they?)
        np.testing.assert_allclose(t_offsets, 0, atol=100e-9)


def timestamps(filename) -> np.ndarray:
    """Get all timestamps from a PCAP file, relative to the first packet."""
    with open(filename, "rb") as in_file:
        reader = UniversalReader(in_file)
        timestamps = [timestamp for timestamp, _ in reader]
        relative_timestamps = [timestamp - timestamps[0] for timestamp in timestamps]
        return np.array(relative_timestamps, dtype=np.float128)


@pytest.mark.parametrize("duplex_mode", (1, 2, 3))
def test_duplex(cnics, duplex_mode):
    """
    Verify duplex operation.

    Both CNICs will be set up as duplex and will send/receive to/from each other
    simultaneously.
    """
    # duplex mode 1 gives 2x 2GB to each of Tx, Rx
    # duplex mode 2 & 3 give only 1x 2GB buffer to one side (or the other),
    # so there's no point using a bigger file
    in_filename = DUPLEX_PCAP_NAME if duplex_mode == 1 else "tests/10_multi_length.pcap"
    n_packets = count_packets_in_pcap(in_filename)
    results_filenames = []
    for cnic in cnics:
        cnic.reset()
        cnic.hbm_pktcontroller.duplex = duplex_mode

    for n, cnic in enumerate(cnics):
        rx_filename = this_test_filename("test_results", "pcap", n)
        results_filenames.append(rx_filename)
        cnic.receive_pcap(rx_filename, 128, n_packets=n_packets)
        print(f'CNIC {n} Platform: {cnic.info["platform"]["static_region"]["vbnv"]}')
        print(f"CNIC {n} Rx: {rx_filename}")
        print(f"CNIC {n} cnic.enable_vd: {cnic.enable_vd.value}")
        print(f"CNIC {n} cnic.vd.enable_vd: {cnic.vd.enable_vd.value}")
        print(f"CNIC {n} cnic.vd_datagen.enable_vd: {cnic.vd_datagen.enable_vd.value}")
        print(
            f"CNIC {n} cnic.hbm_pktcontroller.legacy_rate_sel: {cnic.hbm_pktcontroller.legacy_rate_sel.value}"
        )
        print(
            f"CNIC {n} cnic.hbm_pktcontroller.debug_tx_current_hbm_rd_addr: {cnic.hbm_pktcontroller.debug_tx_current_hbm_rd_addr.value}"
        )
        print(
            f"CNIC {n} cnic.hbm_pktcontroller.debug_tx_current_hbm_rd_buffer: {cnic.hbm_pktcontroller.debug_tx_current_hbm_rd_buffer.value}"
        )

    for cnic in cnics:
        print(f"CNIC preparing to Tx: {in_filename}")
        cnic.prepare_transmit(in_filename)
    while not all([cnic.ready_to_transmit.value for cnic in cnics]):
        sleep(10)
        ready = [cnic.ready_to_transmit.value for cnic in cnics]
        print(f"Ready to Tx? {ready}")
    print("Beginning Tx")
    for cnic in cnics:
        cnic.begin_transmit()

    while not all([cnic.finished_receive.value for cnic in cnics]):
        sleep(10)
        finished = [cnic.finished_receive.value for cnic in cnics]
        print(f"Finished Rx? {finished}")
        for n, cnic in enumerate(cnics):
            print(f"CNIC {n}")
            print(
                "HBM Packet Controller",
                "Rx:",
                int(cnic.hbm_pktcontroller.rx_packet_count),
                end=" ",
            )
            print("Tx:", int(cnic.hbm_pktcontroller.tx_packet_count))
            print(
                "System Ethernet 100G ",
                "Rx:",
                int(cnic.system.eth100g_rx_total_packets),
                end=" ",
            )
            print("Tx:", int(cnic.system.eth100g_tx_total_packets))
            print(f"CNIC {n} cnic.enable_vd: {cnic.enable_vd.value}")
            print(f"CNIC {n} cnic.vd.enable_vd: {cnic.vd.enable_vd.value}")
            print(
                f"CNIC {n} cnic.vd_datagen.enable_vd: {cnic.vd_datagen.enable_vd.value}"
            )
            print(
                f"CNIC {n} cnic.hbm_pktcontroller.legacy_rate_sel: {cnic.hbm_pktcontroller.legacy_rate_sel.value}"
            )
            print(
                f"CNIC {n} cnic.hbm_pktcontroller.debug_tx_current_hbm_rd_addr: {cnic.hbm_pktcontroller.debug_tx_current_hbm_rd_addr.value}"
            )
            print(
                f"CNIC {n} cnic.hbm_pktcontroller.debug_tx_current_hbm_rd_buffer: {cnic.hbm_pktcontroller.debug_tx_current_hbm_rd_buffer.value}"
            )
            print("---")

    for n, cnic in enumerate(cnics):
        print(f"Checking counts for CNIC {n}")
        assert cnic.hbm_pktcontroller.rx_packet_count == n_packets
        assert cnic.hbm_pktcontroller.tx_packet_count == n_packets

    for rx_filename in results_filenames:
        print(f"Checking Rx file {rx_filename}")
        assert same_packets(in_filename, rx_filename)


class FilterMode(Enum):
    """Packet Size Filter Mode."""

    MINIMUM = 0
    EXACT = 1


class TestPacketSizeFilterLoops:
    """Check packet size filter & looping."""

    def _run_simplex_with_filter(
        self, cnics, filter_size, expected_rx, filter_mode, loops=1
    ):
        for cnic in cnics:
            cnic.hbm_pktcontroller.duplex = False
        rx_cnic = cnics[0]
        tx_cnic = cnics[1]
        in_filename = "tests/10_multi_length.pcap"

        n_packets = count_packets_in_pcap(in_filename) * loops
        rx_filename = this_test_filename("test_results", "pcap")

        if filter_mode == FilterMode.EXACT:
            rx_cnic.hbm_pktcontroller.rx_packet_size_abs = 1
        elif filter_mode == FilterMode.MINIMUM:
            rx_cnic.hbm_pktcontroller.rx_packet_size_abs = 0
        else:
            raise NotImplementedError(f"Unknown filter mode {filter_mode}")

        rx_cnic.receive_pcap(rx_filename, packet_size=filter_size, n_packets=n_packets)

        tx_cnic.transmit_pcap(in_filename, n_loops=loops)
        while not tx_cnic.hbm_pktcontroller.tx_complete:
            sleep(1)
        # Need to manually abort Rx, as we asked for more packets as a way of being sure
        # that the filter is actually excluding some.
        rx_cnic.stop_receive()
        while not rx_cnic.finished_receive:
            sleep(2)
        # restore default filter mode. TODO - drive this in CnicFpga.receive_pcap
        rx_cnic.hbm_pktcontroller.rx_packet_size_abs = 0

        assert rx_cnic.hbm_pktcontroller.rx_packet_count == expected_rx

    def test_filter_minimum(self, cnics):
        """Test Rx packet size filter in minimum size mode."""
        # >=782 bytes -> 7 packets per loop
        self._run_simplex_with_filter(
            cnics,
            filter_size=782,
            expected_rx=77,
            filter_mode=FilterMode.MINIMUM,
            loops=11,
        )

    def test_filter_exact(self, cnics):
        """Test Rx packet size filter in exact size mode."""
        # ==782 bytes -> 4 packets per loop
        self._run_simplex_with_filter(
            cnics,
            filter_size=782,
            expected_rx=40,
            filter_mode=FilterMode.EXACT,
            loops=10,
        )


def rx_wait_success(cnic, timeout_s: int = 60) -> bool:
    """Wait for Receive to finish, return success."""
    t_stop_wait = time.time() + timeout_s
    while not cnic.finished_receive:
        sleep(2)
        if time.time() > t_stop_wait:
            break
    if time.time() > t_stop_wait:
        # we failed
        return False
    return True


def debug_register_dump(cnic):
    """Print a bunch of register values for debugging purposes."""
    for attr in cnic.vd.user_attributes:
        if "configuration" in attr:
            continue
        print("VD", attr, getattr(cnic.vd, attr).value)
    for attr in cnic.vd_datagen.user_attributes:
        if "configuration" in attr:
            continue
        print("datagen", attr, getattr(cnic.vd_datagen, attr).value)
    for attr in cnic.vd_datagen_2.user_attributes:
        if "configuration" in attr:
            continue
        print("datagen_2", attr, getattr(cnic.vd_datagen_2, attr).value)

### C-NIC firmware

## Release Notes
Latest updates can be viewed in [CHANGELOG](https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-cnic/-/blob/main/CHANGELOG.md?ref_type=heads)


## Documentation
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-fw-cnic/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-fw-cnic/en/latest/?badge=latest)

The documentation for this project can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-cbf-fw-cnic documentation](https://developer.skatelescope.org/projects/ska-low-cbf-fw-cnic/en/latest/index.html "SKA Developer Portal: ska-low-cbf-fw-cnic documentation")


## Cloning the repo


To clone the repo including populatating submodule repo's:

    git clone --recurse-submodules --remote-submodules  

for a branch

    git clone --recurse-submodules --remote-submodules -b <branch_name> 


You can also issue the following to checkout and populate the submodule repo's

    git submodule init
    git submodule update

## Build Tools

Vivado/Vitis 2022.2

## Targets

Target ALVEO Devices - 
    * U50   - xilinx_u50_gen3x4_xdma_2
    * U50LV - xilinx_u50lv_gen3x4_xdma_2
    * U55C  - xilinx_u55c_gen3x16_xdma_2 (0.1.4 last version)
            - xilinx_u55c_gen3x16_xdma_3
    * U280  - xilinx_u280_gen3x16_xdma_1

1 x 100 GbE port is supported for capture and playout. In the case of the U55C it is the furthest from the PCIe connector.

On dual 100 GbE cards, second port can be used for PTP sync optionally.

A capability register (system.firmware_capability) has been added that will report what is in the current build as this can change per ALVEO.
Firmware capabilities as defined below 
    Bits 3 -> 0      = buffers available in simplex tx, literal number
    Bits 7 -> 4      = buffers available in simplex rx, literal number
    Bits 11 -> 8     = buffers available in duplex tx, literal number
    Bits 15 -> 12    = buffers available in duplex rx, literal number
    Bits 16          = 100G / PTP A
    Bits 17          = 100G / PTP B
    Bits 18          = VD
    Bits 19          = VD 2
    Bits 20 -> 21    = reserved.
    Bits 22          = Loopback for 100G Port A.
    Bits 31 -> 23    = reserved."


## Getting started on a developer machine
Clone this repository onto your machine.
cd into the dir then run the following to setup dependancies

git submodule update --init --recursive

You will be able to work locally with setup and then commit to repo for compilation and packages for others.

RunMe.sh will create project.

## CI Build Notes
Note CI configuration imported from


Version number is taken from the `release` file in the project root directory.
This file must contain three numbers separated by dots, e.g. `1.2.3`.
See also [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Command sequence to update for a patch version example:
* make bump-patch-release
* make git-create-tag
* make git-push-tag

Reference this;
https://developer.skao.int/en/latest/tools/software-package-release-procedure.html#release-management

## CI Tests

Some tests are performed here using pytest for the U55C hardware only.
See CI job `cnic-sw-u55c-test`, and the pytest code in the [tests](tests) directory.

Further tests are performed by triggering the downstream
[Low CBF Integration Testing](https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-integration)
pipeline - see CI job `psi-low-test`.

## Known Issues
* SPEAD V2 packets don't produce a timestamp header of 0x1800, this is to match the partial implementation of the TPM, and will 0x1600 as per V1.
* CNIC-VD before 0.1.12 did not reset the buffer ready flag between uses, assumed the CNIC was not being reused. This may cause the 2.2ms bursts to combine. Only impacts non-initial configurations when the card is not reset.
* CNIC TX may pause between loops at high transmission rates.

## Tag Notes
Tagged builds are kept both in the package registry and the CAR.

----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- 
-- 
-- 
-- registers_tb.txt is generated from the runme_generate_registers_tb.sh in the same dir as this TB
-- relies on ARGs being run first.
-- Test case in VD_datagen.data region is 3 stations x 1 channel, 2 sine waves 1 DC signal.
-- 
----------------------------------------------------------------------------------
library IEEE, signal_processing_common, technology_lib, ethernet_lib;
library LFAADecode_lib, common_lib, cnic_lib, PSR_Packetiser_lib;
library axi4_lib;
library xpm;
use xpm.vcomponents.all;
use IEEE.STD_LOGIC_1164.ALL;
use axi4_lib.axi4_stream_pkg.ALL;
USE axi4_lib.axi4_lite_pkg.ALL;
use axi4_lib.axi4_full_pkg.all;

use std.textio.all;
use IEEE.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
use common_lib.common_pkg.all;
use ethernet_lib.ethernet_pkg.ALL;
USE technology_lib.tech_mac_100g_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;

entity tb_cnic is
    generic (
        -- Location of the test case; All the other filenames in generics here are in this directory
        g_TEST_CASE : string := "../../../../../../../";
        -- text file with SPS packets
        g_SPS_DATA_FILENAME : string := "sps_axi_tb_input.txt";
        -- Register initialisation
        g_REGISTER_INIT_FILENAME : string := "registers_tb.txt";
        -- File to log the output data to (the 100GE axi interface)
        g_SDP_FILENAME : string := "";
        -- initialisation of corner turn 1 HBM
        g_LOAD_CT1_HBM : boolean := False;
        g_CT1_INIT_FILENAME : string := "";
        -- initialisation of corner turn 2 HBM
        g_LOAD_CT2_HBM_CORR1 : boolean := True;
        g_CT2_HBM_CORR1_FILENAME : string := "ct2_init.txt";
        g_LOAD_CT2_HBM_CORR2 : boolean := False;
        g_CT2_HBM_CORR2_FILENAME : string := "";
        --
        --
        -- Text file to use to check against the visibility data going to the HBM from the correlator.
        g_VIS_CHECK_FILE : string := "LTA_vis_check.txt";
        -- Text file to use to check the meta data going to the HBM from the correlator
        g_META_CHECK_FILE : string := "LTA_TCI_FD_check.txt";
        -- Number of bytes to dump from the filterbank output
        -- Default 8 Mbytes; 
        -- Needs to be at least = 
        --  ceil(virtual_channels/4) * (512 bytes) * (3456 fine channels) * (2 timegroups (of 32 times each))
        g_CT2_HBM_DUMP_SIZE : integer := 8388608;  
        g_CT2_HBM_DUMP_ADDR : integer := 0; -- Address to start the memory dump at.
        g_CT2_HBM_DUMP_FNAME : string := "ct2_hbm_dump.txt"
    );
end tb_cnic;

architecture Behavioral of tb_cnic is

    constant c_HBM_init     : string := "";


    signal load_HBM         : std_logic := '0';
    signal clk_freerun      : std_logic := '0';
    signal ap_clk           : std_logic := '0';
    signal clk100           : std_logic := '0';
    signal ap_rst_n         : std_logic := '0';
    signal mc_lite_mosi     : t_axi4_lite_mosi;
    signal mc_lite_miso     : t_axi4_lite_miso;
    signal axi4_lite_miso_dummy : t_axi4_lite_miso;

    signal axi4_full_miso_dummy : t_axi4_full_miso;

    -- The shared memory in the shell is 128Kbytes;
    -- i.e. 32k x 4 byte words. 
    type memType is array(32767 downto 0) of integer;
    shared variable sharedMem : memType;
    
    function strcmp(a, b : string) return boolean is
        alias a_val : string(1 to a'length) is a;
        alias b_val : string(1 to b'length) is b;
        variable a_char, b_char : character;
    begin
        if a'length /= b'length then
            return false;
        elsif a = b then
            return true;
        else
            return false;
        end if;
    end;
    

    COMPONENT axi_bram_RegisterSharedMem
    PORT (
        s_axi_aclk : IN STD_LOGIC;
        s_axi_aresetn : IN STD_LOGIC;
        s_axi_awaddr : IN STD_LOGIC_VECTOR(16 DOWNTO 0);
        s_axi_awlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_awsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_awlock : IN STD_LOGIC;
        s_axi_awcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_awvalid : IN STD_LOGIC;
        s_axi_awready : OUT STD_LOGIC;
        s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_wlast : IN STD_LOGIC;
        s_axi_wvalid : IN STD_LOGIC;
        s_axi_wready : OUT STD_LOGIC;
        s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_bvalid : OUT STD_LOGIC;
        s_axi_bready : IN STD_LOGIC;
        s_axi_araddr : IN STD_LOGIC_VECTOR(16 DOWNTO 0);
        s_axi_arlen : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        s_axi_arsize : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arburst : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_arlock : IN STD_LOGIC;
        s_axi_arcache : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        s_axi_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        s_axi_arvalid : IN STD_LOGIC;
        s_axi_arready : OUT STD_LOGIC;
        s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
        s_axi_rlast : OUT STD_LOGIC;
        s_axi_rvalid : OUT STD_LOGIC;
        s_axi_rready : IN STD_LOGIC;
        bram_rst_a : OUT STD_LOGIC;
        bram_clk_a : OUT STD_LOGIC;
        bram_en_a : OUT STD_LOGIC;
        bram_we_a : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a : OUT STD_LOGIC_VECTOR(16 DOWNTO 0);
        bram_wrdata_a : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a : IN STD_LOGIC_VECTOR(31 DOWNTO 0));
    END COMPONENT;
    
    --signal s_axi_aclk :  STD_LOGIC;
    --signal s_axi_aresetn :  STD_LOGIC;
    signal m00_awaddr :  STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m00_awlen :  STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal m00_awsize :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m00_awburst :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m00_awlock :  STD_LOGIC;
    signal m00_awcache :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m00_awprot :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m00_awvalid :  STD_LOGIC;
    signal m00_awready :  STD_LOGIC;
    signal m00_wdata :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m00_wstrb :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m00_wlast :  STD_LOGIC;
    signal m00_wvalid :  STD_LOGIC;
    signal m00_wready :  STD_LOGIC;
    signal m00_bresp :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m00_bvalid :  STD_LOGIC;
    signal m00_bready :  STD_LOGIC;
    signal m00_araddr :  STD_LOGIC_VECTOR(63 DOWNTO 0);
    signal m00_arlen :  STD_LOGIC_VECTOR(7 DOWNTO 0);
    signal m00_arsize : STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m00_arburst : STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m00_arlock :  STD_LOGIC;
    signal m00_arcache :  STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m00_arprot :  STD_LOGIC_VECTOR(2 DOWNTO 0);
    signal m00_arvalid :  STD_LOGIC;
    signal m00_arready :  STD_LOGIC;
    signal m00_rdata :  STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m00_rresp :  STD_LOGIC_VECTOR(1 DOWNTO 0);
    signal m00_rlast :  STD_LOGIC;
    signal m00_rvalid :  STD_LOGIC;
    signal m00_rready :  STD_LOGIC;
    
    signal m00_clk : std_logic;
    signal m00_we : std_logic_vector(3 downto 0);
    signal m00_addr : std_logic_vector(16 downto 0);
    signal m00_wrData : std_logic_vector(31 downto 0);
    signal m00_rdData : std_logic_vector(31 downto 0);
    
    constant g_HBM_INTERFACES : integer := 4;
    signal HBM_axi_awvalid  : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awready  : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awaddr   : t_slv_64_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awid     : t_slv_1_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awlen    : t_slv_8_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awsize   : t_slv_3_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awburst  : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awlock   : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awcache  : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awprot   : t_slv_3_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awqos    : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_awregion : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_wvalid   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_wready   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_wdata    : t_slv_512_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_wstrb    : t_slv_64_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_wlast    : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_bvalid   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_bready   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_bresp    : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_bid      : t_slv_1_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arvalid  : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arready  : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_araddr   : t_slv_64_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arid     : t_slv_1_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arlen    : t_slv_8_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arsize   : t_slv_3_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arburst  : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arlock   : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arcache  : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arprot   : t_slv_3_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arqos    : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_arregion : t_slv_4_arr(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_rvalid   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_rready   : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_rdata    : t_slv_512_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(M01_AXI_DATA_WIDTH-1 downto 0);
    signal HBM_axi_rlast    : std_logic_vector(g_HBM_INTERFACES-1 downto 0);
    signal HBM_axi_rid      : t_slv_1_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(M01_AXI_ID_WIDTH - 1 downto 0);
    signal HBM_axi_rresp    : t_slv_2_arr(g_HBM_INTERFACES-1 downto 0); -- std_logic_vector(1 downto 0);   
    
    signal eth100_rx_sosi : t_lbus_sosi;
    signal eth100_tx_sosi : t_lbus_sosi;
    signal eth100_tx_siso : t_lbus_siso;
    signal setupDone : std_logic;
    signal eth100G_clk : std_logic := '0';

    signal PTP_time_CMAC_clk        : std_logic_vector(79 downto 0);
    signal PTP_pps_CMAC_clk         : std_logic;
    
    signal PTP_time_CMAC_clk_b      : std_logic_vector(79 downto 0);
    signal PTP_pps_CMAC_clk_b       : std_logic;
        
    signal PTP_time_ARGs_clk        : t_slv_80_arr(0 to (2-1));
    signal PTP_pps_ARGs_clk         : std_logic_vector((2-1) downto 0);
    
    signal m00_bram_we : STD_LOGIC_VECTOR(3 DOWNTO 0);
    signal m00_bram_en : STD_LOGIC;
    signal m00_bram_addr : STD_LOGIC_VECTOR(16 DOWNTO 0);
    signal m00_bram_addr_word : std_logic_vector(14 downto 0);
    signal m00_bram_wrData : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m00_bram_rdData : STD_LOGIC_VECTOR(31 DOWNTO 0);
    signal m00_bram_clk : std_logic;
    signal validMemRstActive : std_logic; 

    
    signal m02_arFIFO_dout, m02_arFIFO_din : std_logic_vector(63 downto 0);
    signal m02_arFIFO_empty, m02_arFIFO_rdEn, m02_arFIFO_wrEn : std_logic;
    signal m02_arFIFO_wrDataCount : std_logic_vector(5 downto 0);
    signal M02_READ_QUEUE_SIZE, MIN_LAG : integer;
    signal m02_arlen_delayed : std_logic_vector(7 downto 0);
    signal m02_arsize_delayed : std_logic_vector(2 downto 0);
    signal m02_arburst_delayed : std_logic_vector(1 downto 0);
    signal m02_arcache_delayed : std_logic_vector(3 downto 0);
    signal m02_arprot_delayed : std_logic_vector(2 downto 0);
    signal m02_arqos_delayed : std_logic_vector(3 downto 0);
    signal m02_arregion_delayed : std_logic_vector(3 downto 0);
    
    signal m02_araddr_delayed : std_logic_vector(19 downto 0);
    signal m02_reqTime : std_logic_vector(31 downto 0);
    signal m02_arvalid_delayed, m02_arready_delayed : std_logic;
    
    
    signal wr_addr_x410E0, rd_addr_x410E0 : std_logic := '0'; 
    signal wrdata_x410E0, rddata_x410E0 : std_logic := '0';

    signal timer_time               : unsigned(31 downto 0);
    signal timer_sec                : unsigned(47 downto 0);
    signal ptp_time_emulation       : std_logic_vector(79 downto 0);

    signal real_time_1_vec          : std_logic_vector(3 downto 0);
    signal real_time_2_vec          : std_logic_vector(3 downto 0);
    signal real_time_3_vec          : std_logic_vector(3 downto 0);

    signal clock_300 : std_logic := '0';    -- 3.33ns
    signal clock_400 : std_logic := '0';    -- 2.50ns
    signal clock_322 : std_logic := '0';    -- 3.11ns

    signal testCount_400        : integer   := 0;
    signal testCount_300        : integer   := 0;
    signal testCount_322        : integer   := 0;

    signal clock_400_rst        : std_logic := '1';
    signal clock_300_rst        : std_logic := '1';
    signal clock_322_rst        : std_logic := '1';

    signal power_up_rst_clock_400   : std_logic_vector(31 downto 0) := c_ones_dword;
    signal power_up_rst_clock_300   : std_logic_vector(31 downto 0) := c_ones_dword;
    signal power_up_rst_clock_322   : std_logic_vector(31 downto 0) := c_ones_dword;

    signal i_rx_axis_tdata          : STD_LOGIC_VECTOR ( 511 downto 0 );
    signal i_rx_axis_tkeep          : STD_LOGIC_VECTOR ( 63 downto 0 );
    signal i_rx_axis_tlast          : STD_LOGIC;
    signal i_rx_axis_tuser          : STD_LOGIC_VECTOR ( 79 downto 0 );
    signal i_rx_axis_tvalid         : STD_LOGIC;
    signal o_rx_axis_tready         : STD_LOGIC;

    signal ethernet_info            : ethernet_frame    := default_ethernet_frame;
    signal ipv4_info                : IPv4_header       := default_ipv4_header;
    signal udp_info                 : UDP_header        := default_UDP_header;
    
    -- Do an axi-lite read of a single 32-bit register.
    PROCEDURE axi_lite_rd(SIGNAL mm_clk   : IN STD_LOGIC;
                          SIGNAL axi_miso : IN t_axi4_lite_miso;
                          SIGNAL axi_mosi : OUT t_axi4_lite_mosi;
                          register_addr   : NATURAL;  -- 4-byte word address
                          variable rd_data  : out std_logic_vector(31 downto 0)) is

        VARIABLE stdio             : line;
        VARIABLE result            : STD_LOGIC_VECTOR(31 DOWNTO 0);
        variable wvalidInt         : std_logic;
        variable awvalidInt        : std_logic;
        
    BEGIN

        -- Start transaction
        WAIT UNTIL rising_edge(mm_clk);
            -- Setup read address
            axi_mosi.arvalid <= '1';
            axi_mosi.araddr <= std_logic_vector(to_unsigned(register_addr*4, 32));
            axi_mosi.rready <= '1';

        read_address_wait: LOOP
            WAIT UNTIL rising_edge(mm_clk);
            IF axi_miso.arready = '1' THEN
               axi_mosi.arvalid <= '0';
               axi_mosi.araddr <= (OTHERS => '0');
            END IF;

            IF axi_miso.rvalid = '1' THEN
               EXIT;
            END IF;
        END LOOP;

        
        rd_data := axi_miso.rdata(31 downto 0);
        -- Read response
        IF axi_miso.rresp = "01" THEN
            write(stdio, string'("exclusive access error "));
            writeline(output, stdio);
        ELSIF axi_miso.rresp = "10" THEN
            write(stdio, string'("slave error "));
            writeline(output, stdio);
        ELSIF axi_miso.rresp = "11" THEN
           write(stdio, string'("address decode error "));
           writeline(output, stdio);
        END IF;


        
        WAIT UNTIL rising_edge(mm_clk);
        axi_mosi.rready <= '0';
    end procedure;
        
begin
        
    ap_clk      <= not ap_clk after 1.666 ns;       -- 300 MHz 
    eth100G_clk <= not eth100G_clk after 1.553 ns;  -- 322 MHz
    clk_freerun <= not clk_freerun after 5 ns;      -- 100 MHz

    clock_300   <= ap_clk;
    
    test_runner_proc_clk300: process(clock_300)
    begin
        if rising_edge(clock_300) then
            -- power up reset logic
            if power_up_rst_clock_300(31) = '1' then
                power_up_rst_clock_300(31 downto 0) <= power_up_rst_clock_300(30 downto 0) & '0';
                clock_300_rst   <= '1';
                testCount_300   <= 0;
            else
                clock_300_rst   <= '0' OR (NOT SetupDone);
                testCount_300   <= testCount_300 + 1;
            end if;
        end if;
    end process;

    test_runner_proc_clk322: process(eth100G_clk)
    begin
        if rising_edge(eth100G_clk) then
            -- power up reset logic
            if power_up_rst_clock_322(31) = '1' then
                power_up_rst_clock_322(31 downto 0) <= power_up_rst_clock_322(30 downto 0) & '0';
                testCount_322   <= 0;
                clock_322_rst   <= '1';
            else
                if testCount_322 = 185 then
                    testCount_322 <= 70;
                else
                    testCount_322 <= testCount_322 + 1;
                end if;
                clock_322_rst   <= '0' OR (NOT SetupDone);
            end if;
        end if;
    end process;

    process
        file RegCmdfile: TEXT;
        variable RegLine_in : Line;
        variable RegGood : boolean;
        variable cmd_str : string(1 to 2);
        variable regAddr : std_logic_vector(31 downto 0);
        variable regSize : std_logic_vector(31 downto 0);
        variable regData : std_logic_vector(31 downto 0);
        variable readResult : std_logic_vector(31 downto 0);
    begin        
        SetupDone <= '0';
        ap_rst_n <= '1';
        
        FILE_OPEN(RegCmdfile, g_TEST_CASE & g_REGISTER_INIT_FILENAME ,READ_MODE);
        
        for i in 1 to 10 loop
            WAIT UNTIL RISING_EDGE(ap_clk);
        end loop;
        ap_rst_n <= '0';
        for i in 1 to 10 loop
             WAIT UNTIL RISING_EDGE(ap_clk);
        end loop;
        ap_rst_n <= '1';
        
        for i in 1 to 100 loop
             WAIT UNTIL RISING_EDGE(ap_clk);
        end loop;
        
        -- For some reason the first transaction doesn't work; this is just a dummy transaction
        -- Arguments are       clk,    miso      ,    mosi     , 4-byte word Addr, write ?, data)
        axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 0,    true, x"00000000");
        
        -- Addresses in the axi lite control module
        -- ADDR_AP_CTRL         = 6'h00,
        -- ADDR_DMA_SRC_0       = 6'h10,
        -- ADDR_DMA_DEST_0      = 6'h14,
        -- ADDR_DMA_SHARED_0    = 6'h18,
        -- ADDR_DMA_SHARED_1    = 6'h1C,
        -- ADDR_DMA_SIZE        = 6'h20,
        --
        axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 6, true, x"DEF20000");  -- Full address of the shared memory; arbitrary so long as it is 128K aligned.
        axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 7, true, x"56789ABC");  -- High 32 bits of the  address of the shared memory; arbitrary.
        
        
        -- Pseudo code :
        --
        --  Repeat while there are commands in the command file:
        --    - Read command from the file (either read or write, together with the ARGs register address)
        --        - Possible commands : [read address length]   <-- Does a register read.
        --                              [write address length]  <-- Does a register write
        --    - If this is a write, then read the write data from the file, and copy into a shared variable used by the memory.
        --    - trigger the kernel to do the register read/write.
        --  Trigger sending of the 100G test data.
        --
        
        while (not endfile(RegCmdfile)) loop 
            readline(RegCmdfile, RegLine_in);
            
            assert False report "Processing register command " & RegLine_in.all severity note;
            
            -- line_in should have a command; process it.
            read(RegLine_in,cmd_str,RegGood);   -- cmd should be either "rd" or "wr"
            hread(RegLine_in,regAddr,regGood);  -- then the register address
            hread(RegLine_in,regSize,regGood);  -- then the number of words
            
            if strcmp(cmd_str,"wr") then
            
                -- The command is a write, so get the write data into the shared memory variable "sharedMem"
                -- Divide by 4 to get the number of words, since the size is in bytes.
                for i in 0 to (to_integer(unsigned(regSize(31 downto 2)))-1) loop
                    readline(regCmdFile, regLine_in);
                    hread(regLine_in,regData,regGood);
                    sharedMem(i) := to_integer(signed(regData));
                end loop;
                
                -- put in the source address. For writes, the source address is the shared memory, which is mapped to word address 0x8000 (=byte address 0x20000) in the ARGS address space.
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 4, true, x"00020000"); 
                -- Put in the destination address in the ARGS memory space.
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 5, true, regAddr);
                -- Size = number of bytes to transfer. 
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 8, true, regSize);

            elsif strcmp(cmd_str,"rd") then
                -- Put in the source address
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 4, true, regAddr);      -- source Address
                -- Destination Address - the shared memory. 
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 5, true, x"00020000");  
                axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 8, true, regSize);      -- size of the transaction in bytes
                

                -- JVA add in here,            elsif strcmp(cmd_str,"wt") then

            else
                assert False report "Bad data in register command file" severity error;
            end if;
   
            -- trigger the command
            axi_lite_transaction(ap_clk, mc_lite_miso, mc_lite_mosi, 0, true, x"00000001");
        
            -- Poll until the command is done
            readResult := (others => '0');
            while (readResult(1) = '0') loop
                axi_lite_rd(ap_clk, mc_lite_miso, mc_lite_mosi, 0, readResult);
                for i in 1 to 10 loop
                    WAIT UNTIL RISING_EDGE(ap_clk);
                end loop;
            end loop;
            
        end loop;

        if validMemRstActive = '1' then
            wait until validMemRstActive = '0';
        end if;
        
        SetupDone <= '1';
        wait;
    end process;
    
    
    m00_bram_addr_word <= m00_bram_addr(16 downto 2);
    
    process(m00_bram_clk)
    begin
        if rising_edge(m00_bram_clk) then
            m00_bram_rdData <= std_logic_vector(to_signed(sharedMem(to_integer(unsigned(m00_bram_addr_word))),32)); 
            
            if m00_bram_we(0) = '1' and m00_bram_en = '1' then
                sharedMem(to_integer(unsigned(m00_bram_addr_word))) := to_integer(signed(m00_bram_wrData));
            end if;
            
            assert (m00_bram_we(3 downto 0) /= "0000" or m00_bram_we(3 downto 0) /= "1111") report "Byte wide write enables should never occur to shared memory" severity error;
            
        end if;
    end process;
    
    -----------------------------------------------------------------------------------
    pseudo_ptp_time : process(clock_300)
    begin
        if rising_edge(clock_300) then
            if clock_300_rst = '1' then
                timer_time(31 downto 0)     <= 32D"999990000";
                timer_sec(47 downto 0)      <= 48D"0";
                real_time_1_vec             <= x"3";
                real_time_2_vec             <= x"3";
                real_time_3_vec             <= x"4";
            else
                -- fake 3.33ns count
                real_time_1_vec     <= real_time_3_vec;
                real_time_2_vec     <= real_time_1_vec;
                real_time_3_vec     <= real_time_2_vec;

                timer_time          <= timer_time + unsigned(real_time_3_vec);
                if timer_time > 999999999 then
                    timer_time      <= timer_time - 1000000000;
                    timer_sec       <= timer_sec + 1;
                end if;
                                       
            end if;
        end if;
    end process;

    ptp_time_emulation      <= std_logic_vector(timer_sec) & std_logic_vector(timer_time);

    PTP_time_ARGs_clk(0)    <= ptp_time_emulation;
    PTP_time_ARGs_clk(1)    <= ptp_time_emulation;

    -------------------------------------------------------------------------------------------------------------
    -- spam traffic
    traffic_proc : process(eth100G_clk)
    begin
    if rising_edge(eth100G_clk) then
        if clock_322_rst = '1' then
            i_rx_axis_tdata      <= zero_512;
            i_rx_axis_tkeep     <= x"0000000000000000";
            i_rx_axis_tvalid    <= '0';
            i_rx_axis_tlast     <= '0';
            i_rx_axis_tuser     <= zero_word & zero_dword & zero_dword;
            
        else
            
            if testCount_322 = 80 then
                i_rx_axis_tuser                <= zero_word & one_dword & zero_dword;
                i_rx_axis_tdata(127 downto 0)   <=  ethernet_info.dst_mac & 
                                                ethernet_info.src_mac & 
                                                ethernet_info.eth_type & 
                                                ipv4_info.version & 
                                                ipv4_info.header_length & 
                                                ipv4_info.type_of_service; 
                i_rx_axis_tdata(255 downto 128) <=  ipv4_info.total_length & 
                                                ipv4_info.id & 
                                                ipv4_info.ip_flags & 
                                                ipv4_info.fragment_off & 
                                                ipv4_info.TTL & 
                                                ipv4_info.protocol & 
                                                ipv4_info.header_chk_sum & 
                                                ipv4_info.src_addr & 
                                                ipv4_info.dst_addr(31 downto 16);
                i_rx_axis_tdata(383 downto 256) <=  ipv4_info.dst_addr(15 downto 0) & 
                                                udp_info.src_port & 
                                                udp_info.dst_port & 
                                                udp_info.length & 
                                                udp_info.checksum & 
                                                x"AAAA" & 
                                                x"AAAAAAAA";
                i_rx_axis_tdata(511 downto 384) <=  x"AAAAAAAA" & 
                                                x"AAAAAAAA" & 
                                                x"AAAAAAAA" & 
                                                x"AAAAAAAA";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
                
            elsif testCount_322 >= 81 AND testCount_322 < 178 then
                i_rx_axis_tuser                <= zero_word & zero_dword & zero_dword;
                i_rx_axis_tdata(127 downto 0)    <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                i_rx_axis_tdata(255 downto 128)  <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                i_rx_axis_tdata(383 downto 256)  <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                i_rx_axis_tdata(511 downto 384)  <= x"11111111" & x"22222222" & x"33333333" & x"44444444";
                i_rx_axis_tkeep                 <= x"FFFFFFFFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
            elsif testCount_322 = 179 then
                i_rx_axis_tdata(127 downto 0)    <= x"DDDDDDDD" & x"EEEEEEEE" & x"FFFFFFFF" & x"00000000";
                i_rx_axis_tdata(255 downto 128)  <= x"DDDDDDDD" & x"EEEEEEEE" & x"FFFFFFFF" & x"00000000";
                i_rx_axis_tdata(383 downto 256)  <= x"DDDDDDDD" & x"EEEEEEEE" & x"FFFFFFFF" & x"00000000";
                i_rx_axis_tdata(511 downto 384)  <= zero_128;
                i_rx_axis_tkeep                 <= x"000003FFFFFFFFFF";
                i_rx_axis_tvalid                <= '1';
                i_rx_axis_tlast                 <= '1'; 
            elsif testCount_322 = 180 then
                i_rx_axis_tdata      <= zero_512;
                i_rx_axis_tkeep     <= x"0000000000000000";
                i_rx_axis_tvalid    <= '0';
                i_rx_axis_tlast     <= '0';    
            end if;
        end if;
    end if;
    end process;
    -----------------------------------------------------------------------------------
    -- axi BRAM controller to interface to the shared memory for register reads and writes.
    
    registerSharedMem : axi_bram_RegisterSharedMem
    PORT MAP (
        s_axi_aclk => ap_clk,
        s_axi_aresetn => ap_rst_n,
        s_axi_awaddr => m00_awaddr(16 downto 0),
        s_axi_awlen => m00_awlen,
        s_axi_awsize => m00_awsize,
        s_axi_awburst => m00_awburst,
        s_axi_awlock => '0',
        s_axi_awcache => m00_awcache,
        s_axi_awprot => m00_awprot,
        s_axi_awvalid => m00_awvalid,
        s_axi_awready => m00_awready,
        s_axi_wdata => m00_wdata,
        s_axi_wstrb => m00_wstrb,
        s_axi_wlast => m00_wlast,
        s_axi_wvalid => m00_wvalid,
        s_axi_wready => m00_wready,
        s_axi_bresp => m00_bresp,
        s_axi_bvalid => m00_bvalid,
        s_axi_bready => m00_bready,
        s_axi_araddr => m00_araddr(16 downto 0),
        s_axi_arlen => m00_arlen,
        s_axi_arsize => m00_arsize,
        s_axi_arburst => m00_arburst,
        s_axi_arlock => '0',
        s_axi_arcache => m00_arcache,
        s_axi_arprot => m00_arprot,
        s_axi_arvalid => m00_arvalid,
        s_axi_arready => m00_arready,
        s_axi_rdata => m00_rdata,
        s_axi_rresp => m00_rresp,
        s_axi_rlast => m00_rlast,
        s_axi_rvalid => m00_rvalid,
        s_axi_rready => m00_rready,
        bram_rst_a => open,   -- OUT STD_LOGIC;
        bram_clk_a => m00_bram_clk, -- OUT STD_LOGIC;
        bram_en_a  => m00_bram_en, -- OUT STD_LOGIC;
        bram_we_a  => m00_bram_we, -- OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
        bram_addr_a => m00_bram_addr, -- OUT STD_LOGIC_VECTOR(16 DOWNTO 0);
        bram_wrdata_a => m00_bram_wrData, -- OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        bram_rddata_a => m00_bram_rdData  -- IN STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
    
    -------------------------------------------------------------------------------------------
    -- 100 GE data input
    -- eth100_rx_sosi
    -- TYPE t_lbus_sosi IS RECORD  -- Source Out and Sink In
    --   data       : STD_LOGIC_VECTOR(511 DOWNTO 0);                -- Data bus
    --   valid      : STD_LOGIC_VECTOR(3 DOWNTO 0);    -- Data segment enable
    --   eop        : STD_LOGIC_VECTOR(3 DOWNTO 0);    -- End of packet
    --   sop        : STD_LOGIC_VECTOR(3 DOWNTO 0);    -- Start of packet
    --   error      : STD_LOGIC_VECTOR(3 DOWNTO 0);    -- Error flag, indicates data has an error
    --   empty      : t_empty_arr(3 DOWNTO 0);         -- Number of bytes empty in the segment  (four 4bit entries)
    -- END RECORD;
    process
        file cmdfile: TEXT;
        variable line_in : Line;
        variable good : boolean;
        variable LFAArepeats : std_logic_vector(15 downto 0);
        variable LFAAData  : std_logic_vector(511 downto 0);
        variable LFAAvalid : std_logic_vector(3 downto 0);
        variable LFAAeop   : std_logic_vector(3 downto 0);
        variable LFAAerror : std_logic_vector(3 downto 0);
        variable LFAAempty0 : std_logic_vector(3 downto 0);
        variable LFAAempty1 : std_logic_vector(3 downto 0);
        variable LFAAempty2 : std_logic_vector(3 downto 0);
        variable LFAAempty3 : std_logic_vector(3 downto 0);
        variable LFAAsop    : std_logic_vector(3 downto 0);
    begin
        
        eth100_rx_sosi.data <= (others => '0');  -- 512 bits
        eth100_rx_sosi.valid <= "0000";          -- 4 bits
        eth100_rx_sosi.eop <= "0000";  
        eth100_rx_sosi.sop <= "0000";
        eth100_rx_sosi.error <= "0000";
        eth100_rx_sosi.empty(0) <= "0000";
        eth100_rx_sosi.empty(1) <= "0000";
        eth100_rx_sosi.empty(2) <= "0000";
        eth100_rx_sosi.empty(3) <= "0000";
        
--        FILE_OPEN(cmdfile,cmd_file_name,READ_MODE);
--        wait until SetupDone = '1';
        
--        wait until rising_edge(eth100G_clk);
        
--        while (not endfile(cmdfile)) loop 
--            readline(cmdfile, line_in);
--            hread(line_in,LFAArepeats,good);
--            hread(line_in,LFAAData,good);
--            hread(line_in,LFAAvalid,good);
--            hread(line_in,LFAAeop,good);
--            hread(line_in,LFAAerror,good);
--            hread(line_in,LFAAempty0,good);
--            hread(line_in,LFAAempty1,good);
--            hread(line_in,LFAAempty2,good);
--            hread(line_in,LFAAempty3,good);
--            hread(line_in,LFAAsop,good);
            
--            eth100_rx_sosi.data <= LFAAData;  -- 512 bits
--            eth100_rx_sosi.valid <= LFAAValid;          -- 4 bits
--            eth100_rx_sosi.eop <= LFAAeop;
--            eth100_rx_sosi.sop <= LFAAsop;
--            eth100_rx_sosi.error <= LFAAerror;
--            eth100_rx_sosi.empty(0) <= LFAAempty0;
--            eth100_rx_sosi.empty(1) <= LFAAempty1;
--            eth100_rx_sosi.empty(2) <= LFAAempty2;
--            eth100_rx_sosi.empty(3) <= LFAAempty3;
            
--            wait until rising_edge(eth100G_clk);
--            while LFAArepeats /= "0000000000000000" loop
--                LFAArepeats := std_logic_vector(unsigned(LFAArepeats) - 1);
--                wait until rising_edge(eth100G_clk);
--            end loop;
--        end loop;
        
--        LFAADone <= '1';
        wait;
    end process;
    
    eth100_tx_siso.ready <= '1';
    eth100_tx_siso.underflow <= '0';
    eth100_tx_siso.overflow <= '0';
    
    -- Capture data packets in eth100_tx_sosi to a file.
    -- fields in eth100_tx_sosi are
    --   .data(511:0)
    --   .valid(3:0)
    --   .eop(3:0)
    --   .sop(3:0)
    --   .empty(3:0)(3:0)
    
    lbusRX : entity cnic_lib.lbus_packet_receive
    Generic map (
        log_file_name => "lbus_out.txt"
    )
    Port map ( 
        clk      => eth100G_clk, -- in  std_logic;     -- clock
        i_rst    => '0', -- in  std_logic;     -- reset input
        i_din    => eth100_tx_sosi.data, -- in  std_logic_vector(511 downto 0);  -- actual data out.
        i_valid  => eth100_tx_sosi.valid, -- in  std_logic_vector(3 downto 0);     -- data out valid (high for duration of the packet)
        i_eop    => eth100_tx_sosi.eop,   -- in  std_logic_vector(3 downto 0);
        i_sop    => eth100_tx_sosi.sop,   -- in  std_logic_vector(3 downto 0);
        i_empty0 => eth100_tx_sosi.empty(0), --  in std_logic_vector(3 downto 0);
        i_empty1 => eth100_tx_sosi.empty(1), -- in std_logic_vector(3 downto 0);
        i_empty2 => eth100_tx_sosi.empty(2), -- in std_logic_vector(3 downto 0);
        i_empty3 => eth100_tx_sosi.empty(3)  -- in std_logic_vector(3 downto 0)
    );
    
    
    dut : entity cnic_lib.cnic_core
    generic map (
        g_DEBUG_ILA                     => FALSE,
        g_HBM_bank_size                 => "4095MB",
        g_ALVEO_U55                     => TRUE,
        g_ALVEO_U50                     => FALSE,
        g_PTP_ENABLE                    => TRUE,
        g_128_SAMPLE_BLOCKS             => 8,       -- set to a subset to allow for Simulation, usually 8191 in firmware, write bursts every 8. Is a -1 number.
        g_N128_SOURCE_INSTANCES         => 1,       -- VD 1 generators
        g_VD_2_instances                => 1,       -- VD 2 generators
        g_CMAC_INSTANCES                => 2
    ) port map (
        
        ap_clk          => ap_clk, --  in std_logic;
        ap_rst_n        => ap_rst_n, -- in std_logic;
        
        -----------------------------------------------------------------------
        -- Ports used for simulation only.
        --
        -- Received data from 100GE
        i_eth100_rx_sosi => eth100_rx_sosi, -- in t_lbus_sosi;
        -- Data to be transmitted on 100GE
        o_eth100_tx_sosi => eth100_tx_sosi, -- out t_lbus_sosi;
        i_eth100_tx_siso => eth100_tx_siso, --  in t_lbus_siso;
        i_clk_100GE      => eth100G_clk,     -- in std_logic;        
        -- reset of the valid memory is in progress.
        o_validMemRstActive => validMemRstActive, -- out std_logic;  
        --  Note: A minimum subset of AXI4 memory mapped signals are declared.  AXI
        --  signals omitted from these interfaces are automatically inferred with the
        -- optimal values for Xilinx SDx systems.  This allows Xilinx AXI4 Interconnects
        -- within the system to be optimized by removing logic for AXI4 protocol
        -- features that are not necessary. When adapting AXI4 masters within the RTL
        -- kernel that have signals not declared below, it is suitable to add the
        -- signals to the declarations below to connect them to the AXI4 Master.
        --
        -- List of omitted signals - effect
        -- -------------------------------
        --  ID - Transaction ID are used for multithreading and out of order transactions.  This increases complexity. This saves logic and increases Fmax in the system when ommited.
        -- SIZE - Default value is log2(data width in bytes). Needed for subsize bursts. This saves logic and increases Fmax in the system when ommited.
        -- BURST - Default value (0b01) is incremental.  Wrap and fixed bursts are not recommended. This saves logic and increases Fmax in the system when ommited.
        -- LOCK - Not supported in AXI4
        -- CACHE - Default value (0b0011) allows modifiable transactions. No benefit to changing this.
        -- PROT - Has no effect in SDx systems.
        -- QOS - Has no effect in SDx systems.
        -- REGION - Has no effect in SDx systems.
        -- USER - Has no effect in SDx systems.
        --  RESP - Not useful in most SDx systems.
        --------------------------------------------------------------------------------------
        --  AXI4-Lite slave interface
        s_axi_control_awvalid => mc_lite_mosi.awvalid, --  in std_logic;
        s_axi_control_awready => mc_lite_miso.awready, --  out std_logic;
        s_axi_control_awaddr  => mc_lite_mosi.awaddr(6 downto 0), -- in std_logic_vector(C_S_AXI_CONTROL_ADDR_WIDTH-1 downto 0);
        s_axi_control_wvalid  => mc_lite_mosi.wvalid, -- in std_logic;
        s_axi_control_wready  => mc_lite_miso.wready, -- out std_logic;
        s_axi_control_wdata   => mc_lite_mosi.wdata(31 downto 0), -- in std_logic_vector(C_S_AXI_CONTROL_DATA_WIDTH-1 downto 0);
        s_axi_control_wstrb   => mc_lite_mosi.wstrb(3 downto 0), -- in std_logic_vector(C_S_AXI_CONTROL_DATA_WIDTH/8-1 downto 0);
        s_axi_control_arvalid => mc_lite_mosi.arvalid, -- in std_logic;
        s_axi_control_arready => mc_lite_miso.arready, -- out std_logic;
        s_axi_control_araddr  => mc_lite_mosi.araddr(6 downto 0), -- in std_logic_vector(C_S_AXI_CONTROL_ADDR_WIDTH-1 downto 0);
        s_axi_control_rvalid  => mc_lite_miso.rvalid,  -- out std_logic;
        s_axi_control_rready  => mc_lite_mosi.rready, -- in std_logic;
        s_axi_control_rdata   => mc_lite_miso.rdata(31 downto 0), -- out std_logic_vector(C_S_AXI_CONTROL_DATA_WIDTH-1 downto 0);
        s_axi_control_rresp   => mc_lite_miso.rresp(1 downto 0), -- out std_logic_vector(1 downto 0);
        s_axi_control_bvalid  => mc_lite_miso.bvalid, -- out std_logic;
        s_axi_control_bready  => mc_lite_mosi.bready, -- in std_logic;
        s_axi_control_bresp   => mc_lite_miso.bresp(1 downto 0), -- out std_logic_vector(1 downto 0);
  
        -- AXI4 master interface for accessing registers : m00_axi
        m00_axi_awvalid => m00_awvalid, -- out std_logic;
        m00_axi_awready => m00_awready, -- in std_logic;
        m00_axi_awaddr  => m00_awaddr,  -- out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        m00_axi_awid    => open, --s_axi_awid,    -- out std_logic_vector(C_M_AXI_ID_WIDTH - 1 downto 0);
        m00_axi_awlen   => m00_awlen,   -- out std_logic_vector(7 downto 0);
        m00_axi_awsize  => m00_awsize,  -- out std_logic_vector(2 downto 0);
        m00_axi_awburst => m00_awburst, -- out std_logic_vector(1 downto 0);
        m00_axi_awlock  => open, -- s_axi_awlock,  -- out std_logic_vector(1 downto 0);
        m00_axi_awcache => m00_awcache, -- out std_logic_vector(3 downto 0);
        m00_axi_awprot  => m00_awprot,  -- out std_logic_vector(2 downto 0);
        m00_axi_awqos   => open, -- s_axi_awqos,   -- out std_logic_vector(3 downto 0);
        m00_axi_awregion => open, -- s_axi_awregion, -- out std_logic_vector(3 downto 0);
        m00_axi_wvalid   => m00_wvalid,   -- out std_logic;
        m00_axi_wready   => m00_wready,   -- in std_logic;
        m00_axi_wdata    => m00_wdata,    -- out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        m00_axi_wstrb    => m00_wstrb,    -- out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
        m00_axi_wlast    => m00_wlast,    -- out std_logic;
        m00_axi_bvalid   => m00_bvalid,   -- in std_logic;
        m00_axi_bready   => m00_bready,   -- out std_logic;
        m00_axi_bresp    => m00_bresp,    -- in std_logic_vector(1 downto 0);
        m00_axi_bid      => "0", -- s_axi_bid,      -- in std_logic_vector(C_M_AXI_ID_WIDTH - 1 downto 0);
        m00_axi_arvalid  => m00_arvalid,  -- out std_logic;
        m00_axi_arready  => m00_arready,  -- in std_logic;
        m00_axi_araddr   => m00_araddr,   -- out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        m00_axi_arid     => open, -- s_axi_arid,     -- out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        m00_axi_arlen    => m00_arlen,    -- out std_logic_vector(7 downto 0);
        m00_axi_arsize   => m00_arsize,   -- out std_logic_vector(2 downto 0);
        m00_axi_arburst  => m00_arburst,  -- out std_logic_vector(1 downto 0);
        m00_axi_arlock   => open, -- s_axi_arlock,   -- out std_logic_vector(1 downto 0);
        m00_axi_arcache  => m00_arcache,  -- out std_logic_vector(3 downto 0);
        m00_axi_arprot   => m00_arprot,   -- out std_logic_Vector(2 downto 0);
        m00_axi_arqos    => open, -- s_axi_arqos,    -- out std_logic_vector(3 downto 0);
        m00_axi_arregion => open, -- s_axi_arregion, -- out std_logic_vector(3 downto 0);
        m00_axi_rvalid   => m00_rvalid,   -- in std_logic;
        m00_axi_rready   => m00_rready,   -- out std_logic;
        m00_axi_rdata    => m00_rdata,    -- in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        m00_axi_rlast    => m00_rlast,    -- in std_logic;
        m00_axi_rid      => "0", -- s_axi_rid,      -- in std_logic_vector(C_M_AXI_ID_WIDTH - 1 downto 0);
        m00_axi_rresp    => m00_rresp,    -- in std_logic_vector(1 downto 0);

        ---------------------------------------------------------------------------------------
        -- HBM interfaces for TX and RX.
        m01_axi_awvalid     => HBM_axi_awvalid(0),
        m01_axi_awready     => HBM_axi_awready(0),
        m01_axi_awaddr      => HBM_axi_awaddr(0),
        m01_axi_awid        => HBM_axi_awid(0),
        m01_axi_awlen       => HBM_axi_awlen(0),
        m01_axi_awsize      => HBM_axi_awsize(0),
        m01_axi_awburst     => HBM_axi_awburst(0),
        m01_axi_awlock      => HBM_axi_awlock(0),
        m01_axi_awcache     => HBM_axi_awcache(0),
        m01_axi_awprot      => HBM_axi_awprot(0),
        m01_axi_awqos       => HBM_axi_awqos(0),
        m01_axi_awregion    => HBM_axi_awregion(0),
        m01_axi_wvalid      => HBM_axi_wvalid(0),
        m01_axi_wready      => HBM_axi_wready(0),
        m01_axi_wdata       => HBM_axi_wdata(0),
        m01_axi_wstrb       => HBM_axi_wstrb(0),
        m01_axi_wlast       => HBM_axi_wlast(0),
        m01_axi_bvalid      => HBM_axi_bvalid(0),
        m01_axi_bready      => HBM_axi_bready(0),
        m01_axi_bresp       => HBM_axi_bresp(0),
        m01_axi_bid         => HBM_axi_bid(0),
        m01_axi_arvalid     => HBM_axi_arvalid(0),
        m01_axi_arready     => HBM_axi_arready(0),
        m01_axi_araddr      => HBM_axi_araddr(0),
        m01_axi_arid        => HBM_axi_arid(0),
        m01_axi_arlen       => HBM_axi_arlen(0),
        m01_axi_arsize      => HBM_axi_arsize(0),
        m01_axi_arburst     => HBM_axi_arburst(0),
        m01_axi_arlock      => HBM_axi_arlock(0),
        m01_axi_arcache     => HBM_axi_arcache(0),
        m01_axi_arprot      => HBM_axi_arprot(0),
        m01_axi_arqos       => HBM_axi_arqos(0),
        m01_axi_arregion    => HBM_axi_arregion(0),
        m01_axi_rvalid      => HBM_axi_rvalid(0),
        m01_axi_rready      => HBM_axi_rready(0),
        m01_axi_rdata       => HBM_axi_rdata(0),
        m01_axi_rlast       => HBM_axi_rlast(0),
        m01_axi_rid         => HBM_axi_rid(0),
        m01_axi_rresp       => HBM_axi_rresp(0),

        m02_axi_awvalid     => HBM_axi_awvalid(1),
        m02_axi_awready     => HBM_axi_awready(1),
        m02_axi_awaddr      => HBM_axi_awaddr(1),
        m02_axi_awid        => HBM_axi_awid(1),
        m02_axi_awlen       => HBM_axi_awlen(1),
        m02_axi_awsize      => HBM_axi_awsize(1),
        m02_axi_awburst     => HBM_axi_awburst(1),
        m02_axi_awlock      => HBM_axi_awlock(1),
        m02_axi_awcache     => HBM_axi_awcache(1),
        m02_axi_awprot      => HBM_axi_awprot(1),
        m02_axi_awqos       => HBM_axi_awqos(1),
        m02_axi_awregion    => HBM_axi_awregion(1),
        m02_axi_wvalid      => HBM_axi_wvalid(1),
        m02_axi_wready      => HBM_axi_wready(1),
        m02_axi_wdata       => HBM_axi_wdata(1),
        m02_axi_wstrb       => HBM_axi_wstrb(1),
        m02_axi_wlast       => HBM_axi_wlast(1),
        m02_axi_bvalid      => HBM_axi_bvalid(1),
        m02_axi_bready      => HBM_axi_bready(1),
        m02_axi_bresp       => HBM_axi_bresp(1),
        m02_axi_bid         => HBM_axi_bid(1),
        m02_axi_arvalid     => HBM_axi_arvalid(1),
        m02_axi_arready     => HBM_axi_arready(1),
        m02_axi_araddr      => HBM_axi_araddr(1),
        m02_axi_arid        => HBM_axi_arid(1),
        m02_axi_arlen       => HBM_axi_arlen(1),
        m02_axi_arsize      => HBM_axi_arsize(1),
        m02_axi_arburst     => HBM_axi_arburst(1),
        m02_axi_arlock      => HBM_axi_arlock(1),
        m02_axi_arcache     => HBM_axi_arcache(1),
        m02_axi_arprot      => HBM_axi_arprot(1),
        m02_axi_arqos       => HBM_axi_arqos(1),
        m02_axi_arregion    => HBM_axi_arregion(1),
        m02_axi_rvalid      => HBM_axi_rvalid(1),
        m02_axi_rready      => HBM_axi_rready(1),
        m02_axi_rdata       => HBM_axi_rdata(1),
        m02_axi_rlast       => HBM_axi_rlast(1),
        m02_axi_rid         => HBM_axi_rid(1),
        m02_axi_rresp       => HBM_axi_rresp(1),

        m03_axi_awvalid     => HBM_axi_awvalid(2),
        m03_axi_awready     => HBM_axi_awready(2),
        m03_axi_awaddr      => HBM_axi_awaddr(2),
        m03_axi_awid        => HBM_axi_awid(2),
        m03_axi_awlen       => HBM_axi_awlen(2),
        m03_axi_awsize      => HBM_axi_awsize(2),
        m03_axi_awburst     => HBM_axi_awburst(2),
        m03_axi_awlock      => HBM_axi_awlock(2),
        m03_axi_awcache     => HBM_axi_awcache(2),
        m03_axi_awprot      => HBM_axi_awprot(2),
        m03_axi_awqos       => HBM_axi_awqos(2),
        m03_axi_awregion    => HBM_axi_awregion(2),
        m03_axi_wvalid      => HBM_axi_wvalid(2),
        m03_axi_wready      => HBM_axi_wready(2),
        m03_axi_wdata       => HBM_axi_wdata(2),
        m03_axi_wstrb       => HBM_axi_wstrb(2),
        m03_axi_wlast       => HBM_axi_wlast(2),
        m03_axi_bvalid      => HBM_axi_bvalid(2),
        m03_axi_bready      => HBM_axi_bready(2),
        m03_axi_bresp       => HBM_axi_bresp(2),
        m03_axi_bid         => HBM_axi_bid(2),
        m03_axi_arvalid     => HBM_axi_arvalid(2),
        m03_axi_arready     => HBM_axi_arready(2),
        m03_axi_araddr      => HBM_axi_araddr(2),
        m03_axi_arid        => HBM_axi_arid(2),
        m03_axi_arlen       => HBM_axi_arlen(2),
        m03_axi_arsize      => HBM_axi_arsize(2),
        m03_axi_arburst     => HBM_axi_arburst(2),
        m03_axi_arlock      => HBM_axi_arlock(2),
        m03_axi_arcache     => HBM_axi_arcache(2),
        m03_axi_arprot      => HBM_axi_arprot(2),
        m03_axi_arqos       => HBM_axi_arqos(2),
        m03_axi_arregion    => HBM_axi_arregion(2),
        m03_axi_rvalid      => HBM_axi_rvalid(2),
        m03_axi_rready      => HBM_axi_rready(2),
        m03_axi_rdata       => HBM_axi_rdata(2),
        m03_axi_rlast       => HBM_axi_rlast(2),
        m03_axi_rid         => HBM_axi_rid(2),
        m03_axi_rresp       => HBM_axi_rresp(2),

        m04_axi_awvalid     => HBM_axi_awvalid(3),
        m04_axi_awready     => HBM_axi_awready(3),
        m04_axi_awaddr      => HBM_axi_awaddr(3),
        m04_axi_awid        => HBM_axi_awid(3),
        m04_axi_awlen       => HBM_axi_awlen(3),
        m04_axi_awsize      => HBM_axi_awsize(3),
        m04_axi_awburst     => HBM_axi_awburst(3),
        m04_axi_awlock      => HBM_axi_awlock(3),
        m04_axi_awcache     => HBM_axi_awcache(3),
        m04_axi_awprot      => HBM_axi_awprot(3),
        m04_axi_awqos       => HBM_axi_awqos(3),
        m04_axi_awregion    => HBM_axi_awregion(3),
        m04_axi_wvalid      => HBM_axi_wvalid(3),
        m04_axi_wready      => HBM_axi_wready(3),
        m04_axi_wdata       => HBM_axi_wdata(3),
        m04_axi_wstrb       => HBM_axi_wstrb(3),
        m04_axi_wlast       => HBM_axi_wlast(3),
        m04_axi_bvalid      => HBM_axi_bvalid(3),
        m04_axi_bready      => HBM_axi_bready(3),
        m04_axi_bresp       => HBM_axi_bresp(3),
        m04_axi_bid         => HBM_axi_bid(3),
        m04_axi_arvalid     => HBM_axi_arvalid(3),
        m04_axi_arready     => HBM_axi_arready(3),
        m04_axi_araddr      => HBM_axi_araddr(3),
        m04_axi_arid        => HBM_axi_arid(3),
        m04_axi_arlen       => HBM_axi_arlen(3),
        m04_axi_arsize      => HBM_axi_arsize(3),
        m04_axi_arburst     => HBM_axi_arburst(3),
        m04_axi_arlock      => HBM_axi_arlock(3),
        m04_axi_arcache     => HBM_axi_arcache(3),
        m04_axi_arprot      => HBM_axi_arprot(3),
        m04_axi_arqos       => HBM_axi_arqos(3),
        m04_axi_arregion    => HBM_axi_arregion(3),
        m04_axi_rvalid      => HBM_axi_rvalid(3),
        m04_axi_rready      => HBM_axi_rready(3),
        m04_axi_rdata       => HBM_axi_rdata(3),
        m04_axi_rlast       => HBM_axi_rlast(3),
        m04_axi_rid         => HBM_axi_rid(3),
        m04_axi_rresp       => HBM_axi_rresp(3),

        ---------------------------------------------------------------------------------------
        -- Timeslave Ports
        o_CMAC_Lite_axi_mosi            => open,
        i_CMAC_Lite_axi_miso            => axi4_lite_miso_dummy,

        o_Timeslave_Full_axi_mosi       => open,
        i_Timeslave_Full_axi_miso       => axi4_full_miso_dummy,

        o_CMAC_b_Lite_axi_mosi          => open,
        i_CMAC_b_Lite_axi_miso          => axi4_lite_miso_dummy,

        o_Timeslave_b_Full_axi_mosi     => open,
        i_Timeslave_b_Full_axi_miso     => axi4_full_miso_dummy,

        o_eth100_reset_final            => open,
        i_eth100G_clk                   => eth100G_clk,
        i_eth100G_locked                => '1',

        i_eth100G_b_clk                 => eth100G_clk,
        i_eth100G_b_locked              => '1',


        -- PTP Data
        i_PTP_time_CMAC_clk             => PTP_time_CMAC_clk,
        i_PTP_pps_CMAC_clk              => PTP_pps_CMAC_clk,
        
        i_PTP_time_CMAC_clk_b           => PTP_time_CMAC_clk_b,
        i_PTP_pps_CMAC_clk_b            => PTP_pps_CMAC_clk_b,
            
        i_PTP_time_ARGs_clk             => PTP_time_ARGs_clk,
        i_PTP_pps_ARGs_clk              => PTP_pps_ARGs_clk,

        -- CMAC Data buses
        o_tx_axis_tdata                 => open,
        o_tx_axis_tkeep                 => open,
        o_tx_axis_tvalid                => open,
        o_tx_axis_tlast                 => open,
        o_tx_axis_tuser                 => open,
        i_tx_axis_tready                => '1',
            
        i_rx_axis_tdata                 => i_rx_axis_tdata,
        i_rx_axis_tkeep                 => i_rx_axis_tkeep,
        i_rx_axis_tlast                 => i_rx_axis_tlast,
        i_rx_axis_tuser                 => i_rx_axis_tuser,
        i_rx_axis_tvalid                => i_rx_axis_tvalid,
        o_rx_axis_tready                => open,
        
        -- 2nd CMAC instance
        o_tx_axis_tdata_b               => open,
        o_tx_axis_tkeep_b               => open,
        o_tx_axis_tvalid_b              => open,
        o_tx_axis_tlast_b               => open,
        o_tx_axis_tuser_b               => open,
        i_tx_axis_tready_b              => '1',

        -- Stats
        i_eth100G_rx_total_packets      => x"00000000",
        i_eth100G_rx_bad_fcs            => x"00000000",
        i_eth100G_rx_bad_code           => x"00000000",
        i_eth100G_tx_total_packets      => x"00000000",

        i_eth100G_b_rx_total_packets    => x"00000000",
        i_eth100G_b_rx_bad_fcs          => x"00000000",
        i_eth100G_b_rx_bad_code         => x"00000000",
        i_eth100G_b_tx_total_packets    => x"00000000",




        clk_freerun     => clk_freerun
    );

sim_hbm : FOR i in 0 to (g_HBM_INTERFACES - 1) GENERATE
    HBM_4G : entity cnic_lib.HBM_axi_tbModel
    generic map (
        AXI_ADDR_WIDTH => 32, -- : integer := 32;   -- Byte address width. This also defines the amount of data. Use the correct width for the HBM memory block, e.g. 28 bits for 256 MBytes.
        AXI_ID_WIDTH => 1, -- integer := 1;
        AXI_DATA_WIDTH => 512, -- integer := 256;  -- Must be a multiple of 32 bits.
        READ_QUEUE_SIZE => 16, --  integer := 16;
        MIN_LAG => 60,  -- integer := 80   
        INCLUDE_PROTOCOL_CHECKER => TRUE,
        RANDSEED => 43526, -- : natural := 12345;
        LATENCY_LOW_PROBABILITY => 95, --  natural := 95;   -- probability, as a percentage, that non-zero gaps between read beats will be small (i.e. < 3 clocks)
        LATENCY_ZERO_PROBABILITY => 80  -- natural := 80   -- probability, as a percentage, that the gap between read beats will be zero.
        --INIT_FILENAME =>"HBMFill.txt"

        
    ) Port map (
        i_clk           => ap_clk,
        i_rst_n         => ap_rst_n,
        axi_awaddr      => HBM_axi_awaddr(i)(31 downto 0),
        axi_awid        => HBM_axi_awid(i),
        axi_awlen       => HBM_axi_awlen(i),
        axi_awsize      => HBM_axi_awsize(i),
        axi_awburst     => HBM_axi_awburst(i),
        axi_awlock      => HBM_axi_awlock(i),
        axi_awcache     => HBM_axi_awcache(i),
        axi_awprot      => HBM_axi_awprot(i),
        axi_awqos       => HBM_axi_awqos(i),
        axi_awregion    => HBM_axi_awregion(i),
        axi_awvalid     => HBM_axi_awvalid(i),
        axi_awready     => HBM_axi_awready(i),
        axi_wdata       => HBM_axi_wdata(i),
        axi_wstrb       => HBM_axi_wstrb(i),
        axi_wlast       => HBM_axi_wlast(i),
        axi_wvalid      => HBM_axi_wvalid(i),
        axi_wready      => HBM_axi_wready(i),
        axi_bresp       => HBM_axi_bresp(i),
        axi_bvalid      => HBM_axi_bvalid(i),
        axi_bready      => HBM_axi_bready(i),
        axi_bid         => HBM_axi_bid(i),
        axi_araddr      => HBM_axi_araddr(i)(31 downto 0),
        axi_arlen       => HBM_axi_arlen(i),
        axi_arsize      => HBM_axi_arsize(i),
        axi_arburst     => HBM_axi_arburst(i),
        axi_arlock      => HBM_axi_arlock(i),
        axi_arcache     => HBM_axi_arcache(i),
        axi_arprot      => HBM_axi_arprot(i),
        axi_arvalid     => HBM_axi_arvalid(i),
        axi_arready     => HBM_axi_arready(i),
        axi_arqos       => HBM_axi_arqos(i),
        axi_arid        => HBM_axi_arid(i),
        axi_arregion    => HBM_axi_arregion(i),
        axi_rdata       => HBM_axi_rdata(i),
        axi_rresp       => HBM_axi_rresp(i),
        axi_rlast       => HBM_axi_rlast(i),
        axi_rvalid      => HBM_axi_rvalid(i),
        axi_rready      => HBM_axi_rready(i),

        i_write_to_disk         => '0',
        i_fname                 => "",
        i_write_to_disk_addr    => 0,
        i_write_to_disk_size    => 0,
        -- Initialisation of the memory
        i_init_mem              => load_HBM,
        i_init_fname            => c_HBM_init
    );
END GENERATE;
    
end Behavioral;
